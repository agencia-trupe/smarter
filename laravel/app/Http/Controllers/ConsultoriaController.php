<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ConsultoriaRecebidosRequest;

use App\Models\Contato;
use App\Models\Consultoria;
use App\Models\ConsultoriaRecebido;

class ConsultoriaController extends Controller
{
    public function index()
    {
        $consultoria  = Consultoria::first();
        $cabecalhoImg = \App\Models\Cabecalhos::first()->consultoria;

        return view('frontend.consultoria', compact('consultoria', 'cabecalhoImg'));
    }

    public function post(ConsultoriaRecebidosRequest $request, ConsultoriaRecebido $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('arquivo')) {
            $input['arquivo'] = ConsultoriaRecebido::upload_arquivo();
        } else {
            $input['arquivo'] = '';
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.consultoria', $input, function($message) use ($request, $contato)
            {
                $message->to('comercial@smarterbrasil.com.br', config('site.name'))
                        ->subject('[CONSULTORIA] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }
}
