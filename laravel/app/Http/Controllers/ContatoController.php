<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Setores;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();
        $cabecalhoImg = \App\Models\Cabecalhos::first()->contato;

        return view('frontend.contato', compact('contato', 'cabecalhoImg'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('arquivo')) {
            $input['arquivo'] = ContatoRecebido::upload_arquivo();
        } else {
            $input['arquivo'] = '';
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        $setores = Setores::first();
        $emailsEnvio = [
            'Gestão Comercial'                       => array_map(function($email) { return trim($email); }, explode(',', $setores->gestao_comercial)),
            'Financeiro'                             => array_map(function($email) { return trim($email); }, explode(',', $setores->financeiro)),
            'Apoio Administrativo e Comercial'       => array_map(function($email) { return trim($email); }, explode(',', $setores->apoio_administrativo_e_comercial)),
            'Assistência Técnica e Pós-Venda'        => array_map(function($email) { return trim($email); }, explode(',', $setores->assistencia_tecnica_e_pos_venda)),
            'Suporte Técnico de Soldagem e Inspeção' => array_map(function($email) { return trim($email); }, explode(',', $setores->suporte_tecnico_de_soldagem_e_inspecao)),
            'Marketing'                              => array_map(function($email) { return trim($email); }, explode(',', $setores->marketing))
        ];

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function($message) use ($request, $contato, $emailsEnvio)
            {
                $message->to($emailsEnvio[$request->get('setor')], config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }
}
