<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Empresa;
use App\Models\Escritorio;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa      = Empresa::first();
        $escritorios  = Escritorio::ordenados()->get();
        $cabecalhoImg = \App\Models\Cabecalhos::first()->empresa;

        return view('frontend.empresa', compact('empresa', 'escritorios', 'cabecalhoImg'));
    }
}
