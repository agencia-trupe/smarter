<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Evento;

class EventosController extends Controller
{
    private function build_calendar($year, $month, $events = []) {
        if (app()->getLocale() == 'pt') {
            $daysOfWeek = array('Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb');
        } else {
            $daysOfWeek = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        }

        // What is the first day of the month in question?
        $firstDayOfMonth = mktime(0,0,0,$month,1,$year);

        // How many days does this month contain?
        $numberDays = date('t',$firstDayOfMonth);

        // Retrieve some information about the first day of the
        // month in question.
        $dateComponents = getdate($firstDayOfMonth);

        // What is the name of the month in question?
        $monthName = $dateComponents['month'];

        // What is the index value (0-6) of the first day of the
        // month in question.
        $dayOfWeek = $dateComponents['wday'];

        // Create the table tag opener and day headers
        $calendar = "<table class='calendar'>";
        $calendar .= "<tr>";

        // Create the calendar headers
        foreach($daysOfWeek as $day) {
            $calendar .= "<th class='header'>$day</th>";
        }

        // Create the rest of the calendar

        // Initiate the day counter, starting with the 1st.
        $currentDay = 1;

        $calendar .= "</tr><tr>";

        // The variable $dayOfWeek is used to
        // ensure that the calendar
        // display consists of exactly 7 columns.
        if ($dayOfWeek > 0) {
            foreach(range(1, $dayOfWeek) as $i) {
                $calendar .= "<td></td>";
            }
        }

        $month = str_pad($month, 2, "0", STR_PAD_LEFT);

        while ($currentDay <= $numberDays) {
            // Seventh column (Saturday) reached. Start a new row.
            if ($dayOfWeek == 7) {
                $dayOfWeek = 0;
                $calendar .= "</tr><tr>";
            }

            $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);

            $date = $currentDayRel.'/'.$month.'/'.$year;

            if (array_key_exists($date, $events)) {
                $calendar .= "<td rel='$date' title='".$events[$date]->{'titulo_calendario_'.app()->getLocale()}."'><span class='marcador'>$currentDay</span><span class='evento'>".$events[$date]->{'titulo_calendario_'.app()->getLocale()}."</span></td>";
            } else {
                $calendar .= "<td rel='$date'><span class='marcador'>$currentDay</span></td>";
            }

            // Increment counters
            $currentDay++;
            $dayOfWeek++;
        }

        // Complete the row of the last week in month, if necessary
        if ($dayOfWeek != 7) {
            $remainingDays = 7 - $dayOfWeek;

            foreach(range(1, $remainingDays) as $i) {
                $calendar .= "<td></td>";
            }
        }

        $calendar .= "</tr>";
        $calendar .= "</table>";

        return $calendar;
    }

    protected $meses = [
        'pt' => [
            'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
        ],
        'en' => [
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        ]
    ];

    public function index(Request $request)
    {
        if ($request->get('ano') && $request->get('mes')) {
            $ano = (int)$request->get('ano');
            $mes = (int)$request->get('mes');
        } elseif ($request->get('ano') || $request->get('mes')) {
            return redirect()->route('news.eventos');
        } else {
            $ano = date('Y');
            $mes = date('m');
        }

        $navegacao = [
            'anterior' => route('news.eventos', [
                'ano' => $mes == 1 ? $ano - 1 : $ano,
                'mes' => $mes == 1 ? 12 : $mes - 1
            ]),
            'atual'    => $this->meses[app()->getLocale()][$mes - 1].' '.$ano,
            'proximo'  => route('news.eventos', [
                'ano' => $mes == 12 ? $ano + 1 : $ano,
                'mes' => $mes == 12 ? 1 : $mes + 1
            ])
        ];

        $listaEventos = Evento::orderBy('data', 'DESC')->orderBy('id', 'DESC')
            ->whereYear('data', '=', $ano)
            ->whereMonth('data', '=', $mes)
            ->get();

        $eventos = [];

        foreach ($listaEventos as $e) {
            $eventos[$e->data] = $e;
        }

        $calendario = $this->build_calendar($ano, $mes, $eventos);

        return view('frontend.news.eventos', compact('navegacao', 'calendario', 'listaEventos'));
    }
}
