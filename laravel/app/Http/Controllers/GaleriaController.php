<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Galeria;

class GaleriaController extends Controller
{
    public function index()
    {
        $galerias = Galeria::ordenados()->paginate(16, ['*'], 'pagina');

        return view('frontend.news.galeria.index', compact('galerias'));
    }

    public function show(Galeria $galeria)
    {
        return view('frontend.news.galeria.show', compact('galeria'));
    }
}
