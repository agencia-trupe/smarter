<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NewsletterRequest;
use App\Http\Controllers\Controller;

use App\Models\Chamada;
use App\Models\Banner;
use App\Models\Newsletter;

class HomeController extends Controller
{
    public function index()
    {
        $banners  = Banner::ordenados()->get();
        $chamadas = Chamada::ordenados()->get();

        return view('frontend.home', compact('banners', 'chamadas'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.newsletter.sucesso')
        ];

        return response()->json($response);
    }
}
