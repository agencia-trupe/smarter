<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\News;
use App\Models\Noticia;
use App\Models\Galeria;
use App\Models\Eventoscapa;

class NewsController extends Controller
{
    public function index()
    {
        $news        = News::first();
        $novidades   = Noticia::orderBy('data', 'DESC')->orderBy('id', 'DESC')->limit(5)->get();
        $galerias    = Galeria::ordenados()->limit(5)->get();
        $eventosCapa = Eventoscapa::first();

        return view('frontend.news.index', compact('news', 'novidades', 'galerias', 'eventosCapa'));
    }
}
