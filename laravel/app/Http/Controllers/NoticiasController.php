<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index()
    {
        $noticias = Noticia::orderBy('data', 'DESC')->orderBy('id', 'DESC');
        $banners  = $noticias->limit(5)->get();
        $noticias = $noticias->paginate(5, ['*'], 'pagina');

        return view('frontend.news.noticias.index', compact('noticias', 'banners'));
    }

    public function show(Noticia $noticia)
    {
        $data_post = \Carbon\Carbon::createFromFormat('d/m/Y', $noticia->data)->format('Y-m-d');

        $anterior = Noticia::where('data', '<', $data_post)
                          ->orWhere(function($query) use ($data_post, $noticia) {
                              $query->where('data', $data_post)
                                    ->where('id', '<', $noticia->id);
                          })
                          ->orderBy('data', 'DESC')
                          ->orderBy('id', 'DESC')
                          ->first();

        $proximo = Noticia::where('data', '>', $data_post)
                          ->orWhere(function($query) use ($data_post, $noticia) {
                              $query->where('data', $data_post)
                                    ->where('id', '>', $noticia->id);
                          })
                          ->orderBy('data', 'ASC')
                          ->orderBy('id', 'ASC')
                          ->first();

        return view('frontend.news.noticias.show', compact('noticia', 'anterior', 'proximo'));
    }
}
