<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ConsultoriaRecebidosRequest;

use App\Models\OndeComprar;
use App\Models\Ecommerce;
use App\Models\Distribuidor;

class OndeComprarController extends Controller
{
    public function index($estado = null)
    {
        $ondeComprar  = OndeComprar::first();
        $ecommerces   = Ecommerce::ordenados()->get();
        $cabecalhoImg = \App\Models\Cabecalhos::first()->onde_comprar;

        if ($estado && !in_array(strtolower($estado), array_map('strtolower', array_keys(\Tools::listaEstados())))) abort('404');

        $distribuidores = $estado == null
                            ? null
                            : Distribuidor::where('estado', $estado)->ordenados()->get();

        return view('frontend.onde-comprar', compact('ondeComprar', 'cabecalhoImg', 'ecommerces', 'distribuidores', 'estado'));
    }
}
