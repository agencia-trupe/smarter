<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AcessoriosConsumiveisEpisCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\AcessorioConsumivelEpiCategoria;

class AcessoriosConsumiveisEpisCategoriasController extends Controller
{
    public function index()
    {
        $categorias = AcessorioConsumivelEpiCategoria::ordenados()->get();

        return view('painel.acessorios-consumiveis-epis.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.acessorios-consumiveis-epis.categorias.create');
    }

    public function store(AcessoriosConsumiveisEpisCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = AcessorioConsumivelEpiCategoria::upload_capa();

            AcessorioConsumivelEpiCategoria::create($input);
            return redirect()->route('painel.acessorios-consumiveis-epis.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(AcessorioConsumivelEpiCategoria $categoria)
    {
        return view('painel.acessorios-consumiveis-epis.categorias.edit', compact('categoria'));
    }

    public function update(AcessoriosConsumiveisEpisCategoriasRequest $request, AcessorioConsumivelEpiCategoria $categoria)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = AcessorioConsumivelEpiCategoria::upload_capa();

            $categoria->update($input);
            return redirect()->route('painel.acessorios-consumiveis-epis.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(AcessorioConsumivelEpiCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.acessorios-consumiveis-epis.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
