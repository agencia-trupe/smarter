<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AcessoriosConsumiveisEpisRequest;
use App\Http\Controllers\Controller;

use App\Models\AcessorioConsumivelEpi;
use App\Models\AcessorioConsumivelEpiCategoria;

class AcessoriosConsumiveisEpisController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = AcessorioConsumivelEpiCategoria::ordenados()->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (AcessorioConsumivelEpiCategoria::find($filtro)) {
            $registros = AcessorioConsumivelEpi::where('acessorios_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = AcessorioConsumivelEpi::leftJoin('acessorios_consumiveis_epis_categorias as cat', 'cat.id', '=', 'acessorios_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('acessorios_consumiveis_epis.*')
                ->ordenados()->get();
        }

        return view('painel.acessorios-consumiveis-epis.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.acessorios-consumiveis-epis.create', compact('categorias'));
    }

    public function store(AcessoriosConsumiveisEpisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = AcessorioConsumivelEpi::upload_imagem();

            AcessorioConsumivelEpi::create($input);

            return redirect()->route('painel.acessorios-consumiveis-epis.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AcessorioConsumivelEpi $registro)
    {
        $categorias = $this->categorias;

        return view('painel.acessorios-consumiveis-epis.edit', compact('registro', 'categorias'));
    }

    public function update(AcessoriosConsumiveisEpisRequest $request, AcessorioConsumivelEpi $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = AcessorioConsumivelEpi::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.acessorios-consumiveis-epis.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AcessorioConsumivelEpi $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.acessorios-consumiveis-epis.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
