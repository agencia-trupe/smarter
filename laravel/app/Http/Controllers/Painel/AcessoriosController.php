<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AcessoriosRequest;
use App\Http\Controllers\Controller;

use App\Models\Acessorio;
use App\Models\Produto;

class AcessoriosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = Acessorio::produto($produto->id)->ordenados()->get();

        return view('painel.acessorios.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.acessorios.create', compact('produto'));
    }

    public function store(AcessoriosRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Acessorio::upload_imagem();

            Acessorio::create($input);

            return redirect()->route('painel.produtos.acessorios.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, Acessorio $registro)
    {
        return view('painel.acessorios.edit', compact('registro', 'produto'));
    }

    public function update(AcessoriosRequest $request, Produto $produto, Acessorio $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Acessorio::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.acessorios.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, Acessorio $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.acessorios.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
