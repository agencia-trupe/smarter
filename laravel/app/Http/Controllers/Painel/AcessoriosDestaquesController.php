<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AcessoriosDestaquesRequest;
use App\Http\Controllers\Controller;

use App\Models\AcessorioDestaque;
use App\Models\AcessorioConsumivelEPI;

class AcessoriosDestaquesController extends Controller
{
    public function index(AcessorioConsumivelEPI $acessorio)
    {
        $registros = AcessorioDestaque::acessorio($acessorio->id)->ordenados()->get();

        return view('painel.acessorios-destaques.index', compact('registros', 'acessorio'));
    }

    public function create(AcessorioConsumivelEPI $acessorio)
    {
        return view('painel.acessorios-destaques.create', compact('acessorio'));
    }

    public function store(AcessoriosDestaquesRequest $request, AcessorioConsumivelEPI $acessorio)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = AcessorioDestaque::upload_imagem();

            AcessorioDestaque::create($input);

            return redirect()->route('painel.acessorios-consumiveis-epis.destaques.index', $acessorio->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AcessorioConsumivelEPI $acessorio, AcessorioDestaque $registro)
    {
        return view('painel.acessorios-destaques.edit', compact('registro', 'acessorio'));
    }

    public function update(AcessoriosDestaquesRequest $request, AcessorioConsumivelEPI $acessorio, AcessorioDestaque $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = AcessorioDestaque::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.acessorios-consumiveis-epis.destaques.index', $acessorio->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AcessorioConsumivelEPI $acessorio, AcessorioDestaque $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.acessorios-consumiveis-epis.destaques.index', $acessorio->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
