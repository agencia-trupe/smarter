<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AcessoriosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\AcessorioConsumivelEpi;
use App\Models\AcessorioImagem;

use App\Helpers\CropImage;

class AcessoriosImagensController extends Controller
{
    public function index(AcessorioConsumivelEpi $registro)
    {
        $imagens = AcessorioImagem::acessorio($registro->id)->ordenados()->get();

        return view('painel.acessorios-consumiveis-epis.imagens.index', compact('imagens', 'registro'));
    }

    public function show(AcessorioConsumivelEpi $registro, AcessorioImagem $imagem)
    {
        return $imagem;
    }

    public function store(AcessorioConsumivelEpi $registro, AcessoriosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = AcessorioImagem::uploadImagem();
            $input['acessorios_id'] = $registro->id;

            $imagem = AcessorioImagem::create($input);

            $view = view('painel.acessorios-consumiveis-epis.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(AcessorioConsumivelEpi $registro, AcessorioImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.acessorios-consumiveis-epis.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(AcessorioConsumivelEpi $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.acessorios-consumiveis-epis.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
