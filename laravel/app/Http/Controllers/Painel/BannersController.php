<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BannersRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;

class BannersController extends Controller
{
    public $limiteRegistros = 5;

    public function index()
    {
        $registros = Banner::ordenados()->get();
        $limiteExcedido =  Banner::count() >= $this->limiteRegistros;

        return view('painel.banners.index', compact('registros', 'limiteExcedido'));
    }

    public function create()
    {
        return view('painel.banners.create');
    }

    public function store(BannersRequest $request)
    {
        try {

            if (Banner::count() >= $this->limiteRegistros) {
                throw new \Exception('Limite de registros excedido', 1);
            }

            $input = $request->all();

            if (isset($input['background'])) $input['background'] = Banner::upload_background();
            if (isset($input['background_mobile'])) $input['background_mobile'] = Banner::upload_background_mobile();

            if (isset($input['imagem'])) $input['imagem'] = Banner::upload_imagem();

            if (isset($input['icone'])) $input['icone'] = Banner::upload_icone();

            Banner::create($input);

            return redirect()->route('painel.banners.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Banner $registro)
    {
        return view('painel.banners.edit', compact('registro'));
    }

    public function update(BannersRequest $request, Banner $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['background'])) $input['background'] = Banner::upload_background();
            if (isset($input['background_mobile'])) $input['background_mobile'] = Banner::upload_background_mobile();

            if (isset($input['imagem'])) $input['imagem'] = Banner::upload_imagem();

            if (isset($input['icone'])) $input['icone'] = Banner::upload_icone();

            $registro->update($input);

            return redirect()->route('painel.banners.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Banner $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.banners.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
