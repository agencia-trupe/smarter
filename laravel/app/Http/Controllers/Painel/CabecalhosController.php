<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CabecalhosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cabecalhos;

class CabecalhosController extends Controller
{
    public function index()
    {
        $registro = Cabecalhos::first();

        return view('painel.cabecalhos.edit', compact('registro'));
    }

    public function update(CabecalhosRequest $request, Cabecalhos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['empresa'])) $input['empresa'] = Cabecalhos::upload_empresa();
            if (isset($input['tutoriais'])) $input['tutoriais'] = Cabecalhos::upload_tutoriais();
            if (isset($input['suporte'])) $input['suporte'] = Cabecalhos::upload_suporte();
            if (isset($input['onde_comprar'])) $input['onde_comprar'] = Cabecalhos::upload_onde_comprar();
            if (isset($input['consultoria'])) $input['consultoria'] = Cabecalhos::upload_consultoria();
            if (isset($input['contato'])) $input['contato'] = Cabecalhos::upload_contato();

            $registro->update($input);

            return redirect()->route('painel.cabecalhos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
