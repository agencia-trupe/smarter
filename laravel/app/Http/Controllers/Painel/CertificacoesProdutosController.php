<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CertificacoesProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\CertificacaoProduto;
use App\Models\Produto;

class CertificacoesProdutosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = CertificacaoProduto::produto($produto->id)->ordenados()->get();

        return view('painel.certificacoes-produtos.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.certificacoes-produtos.create', compact('produto'));
    }

    public function store(CertificacoesProdutosRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CertificacaoProduto::upload_imagem();

            CertificacaoProduto::create($input);

            return redirect()->route('painel.produtos.certificacoes.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, CertificacaoProduto $registro)
    {
        return view('painel.certificacoes-produtos.edit', compact('registro', 'produto'));
    }

    public function update(CertificacoesProdutosRequest $request, Produto $produto, CertificacaoProduto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CertificacaoProduto::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.certificacoes.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, CertificacaoProduto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.certificacoes.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
