<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConsultoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\Consultoria;

class ConsultoriaController extends Controller
{
    public function index()
    {
        $registro = Consultoria::first();

        return view('painel.consultoria.edit', compact('registro'));
    }

    public function update(ConsultoriaRequest $request, Consultoria $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_consultoria'])) $input['imagem_consultoria'] = Consultoria::upload_imagem_consultoria();
            if (isset($input['imagem_consultoria_mobile'])) $input['imagem_consultoria_mobile'] = Consultoria::upload_imagem_consultoria_mobile();
            if (isset($input['imagem_contato'])) $input['imagem_contato'] = Consultoria::upload_imagem_contato();

            $registro->update($input);

            return redirect()->route('painel.consultoria.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
