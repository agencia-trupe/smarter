<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ConsultoriaRecebido;

class ConsultoriaRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ConsultoriaRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.consultoria.recebidos.index', compact('contatosrecebidos'));
    }

    public function show(ConsultoriaRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.consultoria.recebidos.show', compact('contato'));
    }

    public function destroy(ConsultoriaRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.consultoria.recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ConsultoriaRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.consultoria.recebidos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
