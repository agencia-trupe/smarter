<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DestaquesRequest;
use App\Http\Controllers\Controller;

use App\Models\Destaque;
use App\Models\Produto;

class DestaquesController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = Destaque::produto($produto->id)->ordenados()->get();

        return view('painel.destaques.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.destaques.create', compact('produto'));
    }

    public function store(DestaquesRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Destaque::upload_imagem();

            Destaque::create($input);

            return redirect()->route('painel.produtos.destaques.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, Destaque $registro)
    {
        return view('painel.destaques.edit', compact('registro', 'produto'));
    }

    public function update(DestaquesRequest $request, Produto $produto, Destaque $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Destaque::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.destaques.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, Destaque $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.destaques.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
