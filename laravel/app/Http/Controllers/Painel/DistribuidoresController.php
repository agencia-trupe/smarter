<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DistribuidoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Distribuidor;

class DistribuidoresController extends Controller
{
    public function index()
    {
        $registros = Distribuidor::ordenados()->get();

        return view('painel.distribuidores.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.distribuidores.create');
    }

    public function store(DistribuidoresRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Distribuidor::upload_imagem();

            Distribuidor::create($input);

            return redirect()->route('painel.distribuidores.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Distribuidor $registro)
    {
        return view('painel.distribuidores.edit', compact('registro'));
    }

    public function update(DistribuidoresRequest $request, Distribuidor $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Distribuidor::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.distribuidores.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Distribuidor $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.distribuidores.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
