<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DownloadsRequest;
use App\Http\Controllers\Controller;

use App\Models\Download;
use App\Models\Produto;

class DownloadsController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = Download::produto($produto->id)->ordenados()->get();

        return view('painel.downloads.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.downloads.create', compact('produto'));
    }

    public function store(DownloadsRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) $input['arquivo'] = Download::upload_arquivo();

            Download::create($input);

            return redirect()->route('painel.produtos.downloads.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, Download $registro)
    {
        return view('painel.downloads.edit', compact('registro', 'produto'));
    }

    public function update(DownloadsRequest $request, Produto $produto, Download $registro)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) $input['arquivo'] = Download::upload_arquivo();

            $registro->update($input);

            return redirect()->route('painel.produtos.downloads.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, Download $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.downloads.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
