<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EcommercesRequest;
use App\Http\Controllers\Controller;

use App\Models\Ecommerce;

class EcommercesController extends Controller
{
    public function index()
    {
        $registros = Ecommerce::ordenados()->get();

        return view('painel.ecommerces.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.ecommerces.create');
    }

    public function store(EcommercesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Ecommerce::upload_marca();

            Ecommerce::create($input);

            return redirect()->route('painel.ecommerces.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Ecommerce $registro)
    {
        return view('painel.ecommerces.edit', compact('registro'));
    }

    public function update(EcommercesRequest $request, Ecommerce $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Ecommerce::upload_marca();

            $registro->update($input);

            return redirect()->route('painel.ecommerces.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Ecommerce $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.ecommerces.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
