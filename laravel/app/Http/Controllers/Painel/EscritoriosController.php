<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EscritoriosRequest;
use App\Http\Controllers\Controller;

use App\Models\Escritorio;

class EscritoriosController extends Controller
{
    public function index()
    {
        $registros = Escritorio::ordenados()->get();

        return view('painel.escritorios.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.escritorios.create');
    }

    public function store(EscritoriosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Escritorio::upload_imagem();

            Escritorio::create($input);

            return redirect()->route('painel.escritorios.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Escritorio $registro)
    {
        return view('painel.escritorios.edit', compact('registro'));
    }

    public function update(EscritoriosRequest $request, Escritorio $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Escritorio::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.escritorios.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Escritorio $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.escritorios.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
