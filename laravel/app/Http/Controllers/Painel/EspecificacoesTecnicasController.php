<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EspecificacoesTecnicasRequest;
use App\Http\Controllers\Controller;

use App\Models\Especificacao;
use App\Models\Produto;

class EspecificacoesTecnicasController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = Especificacao::produto($produto->id)->ordenados()->get();

        return view('painel.especificacoes-tecnicas.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        return view('painel.especificacoes-tecnicas.create', compact('produto'));
    }

    public function store(EspecificacoesTecnicasRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            Especificacao::create($input);

            return redirect()->route('painel.produtos.especificacoes-tecnicas.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, Especificacao $registro)
    {
        return view('painel.especificacoes-tecnicas.edit', compact('registro', 'produto'));
    }

    public function update(EspecificacoesTecnicasRequest $request, Produto $produto, Especificacao $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.produtos.especificacoes-tecnicas.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, Especificacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.especificacoes-tecnicas.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
