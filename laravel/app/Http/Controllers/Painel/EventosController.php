<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EventosRequest;
use App\Http\Controllers\Controller;

use App\Models\Evento;

use Carbon\Carbon;

class EventosController extends Controller
{
    public function index()
    {
        $registros = Evento::orderBy('data', 'DESC')->get();

        return view('painel.eventos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.eventos.create');
    }

    public function store(EventosRequest $request)
    {
        try {

            $input = $request->all();

            if (Evento::where('data', Carbon::createFromFormat('d/m/Y', $input['data'])->format('Y-m-d'))->count()) {
                throw new \Exception('Já existe um evento cadastrado nesta data.');
            }

            Evento::create($input);

            return redirect()->route('painel.eventos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function edit(Evento $registro)
    {
        return view('painel.eventos.edit', compact('registro'));
    }

    public function update(EventosRequest $request, Evento $registro)
    {
        try {

            $input = $request->all();

            if ($registro->data != $input['data'] && Evento::where('data', Carbon::createFromFormat('d/m/Y', $input['data'])->format('Y-m-d'))->count()) {
                throw new \Exception('Já existe um evento cadastrado nesta data.');
            }

            $registro->update($input);

            return redirect()->route('painel.eventos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function destroy(Evento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.eventos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
