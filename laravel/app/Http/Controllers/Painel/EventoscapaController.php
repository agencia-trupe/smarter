<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EventoscapaRequest;
use App\Http\Controllers\Controller;

use App\Models\Eventoscapa;

class EventoscapaController extends Controller
{
    public function index()
    {
        $registro = Eventoscapa::first();

        return view('painel.eventoscapa.edit', compact('registro'));
    }

    public function update(EventoscapaRequest $request, Eventoscapa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Eventoscapa::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.eventos.capa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
