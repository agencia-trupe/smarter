<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GaleriasRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;

class GaleriasController extends Controller
{
    public function index()
    {
        $registros = Galeria::ordenados()->get();

        return view('painel.galerias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.galerias.create');
    }

    public function store(GaleriasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Galeria::upload_capa();

            Galeria::create($input);

            return redirect()->route('painel.galerias.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Galeria $registro)
    {
        return view('painel.galerias.edit', compact('registro'));
    }

    public function update(GaleriasRequest $request, Galeria $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Galeria::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.galerias.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Galeria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.galerias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
