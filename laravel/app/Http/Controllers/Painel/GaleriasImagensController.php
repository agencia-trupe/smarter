<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GaleriasImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Galeria;
use App\Models\GaleriaImagem;

use App\Helpers\CropImage;

class GaleriasImagensController extends Controller
{
    public function index(Galeria $registro)
    {
        $imagens = GaleriaImagem::galeria($registro->id)->ordenados()->get();

        return view('painel.galerias.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Galeria $registro, GaleriaImagem $imagem)
    {
        return $imagem;
    }

    public function store(Galeria $registro, GaleriasImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = GaleriaImagem::uploadImagem();
            $input['galeria_id'] = $registro->id;

            $imagem = GaleriaImagem::create($input);

            $view = view('painel.galerias.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function update(GaleriasImagensRequest $request, Galeria $registro, GaleriaImagem $imagem)
    {
        try {

            $imagem->update([
                'legenda_pt' => $request->get('legenda_pt'),
                'legenda_en' => $request->get('legenda_en')
            ]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Galeria $registro, GaleriaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.galerias.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Galeria $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.galerias.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
