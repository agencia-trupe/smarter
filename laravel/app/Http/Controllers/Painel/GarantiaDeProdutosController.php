<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GarantiaDeProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\GarantiaDeProdutos;

class GarantiaDeProdutosController extends Controller
{
    public function index()
    {
        $registro = GarantiaDeProdutos::first();

        return view('painel.garantia-de-produtos.edit', compact('registro'));
    }

    public function update(GarantiaDeProdutosRequest $request, GarantiaDeProdutos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_abertura'])) $input['imagem_abertura'] = GarantiaDeProdutos::upload_imagem_abertura();
            if (isset($input['imagem_saiba_mais'])) $input['imagem_saiba_mais'] = GarantiaDeProdutos::upload_imagem_saiba_mais();

            $registro->update($input);

            return redirect()->route('painel.garantia-de-produtos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
