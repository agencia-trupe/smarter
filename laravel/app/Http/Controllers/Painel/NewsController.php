<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;

use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $registro = News::first();

        return view('painel.news.edit', compact('registro'));
    }

    public function update(NewsRequest $request, News $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.news.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
