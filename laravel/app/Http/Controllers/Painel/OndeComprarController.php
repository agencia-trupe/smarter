<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OndeComprarRequest;
use App\Http\Controllers\Controller;

use App\Models\OndeComprar;

class OndeComprarController extends Controller
{
    public function index()
    {
        $registro = OndeComprar::first();

        return view('painel.onde-comprar.edit', compact('registro'));
    }

    public function update(OndeComprarRequest $request, OndeComprar $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_televendas'])) $input['imagem_televendas'] = OndeComprar::upload_imagem_televendas();
            if (isset($input['imagem_distribuidores'])) $input['imagem_distribuidores'] = OndeComprar::upload_imagem_distribuidores();

            $registro->update($input);

            return redirect()->route('painel.onde-comprar.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
