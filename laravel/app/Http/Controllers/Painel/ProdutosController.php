<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoLinha;

class ProdutosController extends Controller
{
    private $linhas;
    private $categorias;

    public function __construct()
    {
        $this->linhas = ProdutoLinha::ordenados()->lists('titulo_pt', 'id');
        $this->categorias = array_slice(Produto::categoriasArray(), 0 , -1);
    }

    public function index(Request $request)
    {
        $linhas     = $this->linhas;
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');
        $categoria  = $request->query('categoria') ?: key($categorias);

        if (!array_key_exists($categoria, $categorias)) {
            return redirect()->route('painel.produtos.index');
        }

        if (ProdutoLinha::find($filtro)) {
            $registros = Produto::where('categoria', $categoria)->where('produtos_linha_id', $filtro)->ordenados()->get();
        } else {
            $registros = Produto::where('categoria', $categoria)
                ->leftJoin('produtos_linhas as cat', 'cat.id', '=', 'produtos_linha_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->get();
        }

        return view('painel.produtos.index', compact('categorias', 'categoria', 'linhas', 'registros', 'filtro'));
    }

    public function create()
    {
        $linhas = $this->linhas;
        $categorias = $this->categorias;

        return view('painel.produtos.create', compact('categorias', 'linhas'));
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();
            if (isset($input['tabela_pt'])) $input['tabela_pt'] = Produto::upload_tabela_pt();
            if (isset($input['tabela_en'])) $input['tabela_en'] = Produto::upload_tabela_en();
            if ($request->hasFile('manual')) $input['manual'] = Produto::upload_arquivo();

            Produto::create($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $registro)
    {
        $linhas = $this->linhas;
        $categorias = $this->categorias;

        return view('painel.produtos.edit', compact('registro', 'linhas', 'categorias'));
    }

    public function update(ProdutosRequest $request, Produto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();
            if (isset($input['tabela_pt'])) $input['tabela_pt'] = Produto::upload_tabela_pt();
            if (isset($input['tabela_en'])) $input['tabela_en'] = Produto::upload_tabela_en();
            if ($request->hasFile('manual')) $input['manual'] = Produto::upload_arquivo();

            $registro->update($input);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
