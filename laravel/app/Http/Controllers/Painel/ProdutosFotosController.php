<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosFotosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoFoto;

use App\Helpers\CropImage;

class ProdutosFotosController extends Controller
{
    public function index(Produto $registro)
    {
        $imagens = ProdutoFoto::produto($registro->id)->ordenados()->get();

        return view('painel.produtos.fotos.index', compact('imagens', 'registro'));
    }

    public function show(Produto $registro, ProdutoFoto $imagem)
    {
        return $imagem;
    }

    public function store(Produto $registro, ProdutosFotosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ProdutoFoto::uploadFoto();
            $input['produto_id'] = $registro->id;

            $imagem = ProdutoFoto::create($input);

            $view = view('painel.produtos.fotos.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Produto $registro, ProdutoFoto $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.produtos.fotos.index', $registro)
                             ->with('success', 'Foto excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Produto $registro)
    {
        try {

            $registro->fotos()->delete();
            return redirect()->route('painel.produtos.fotos.index', $registro)
                             ->with('success', 'Fotos excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
