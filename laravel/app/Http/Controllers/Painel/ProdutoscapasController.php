<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutoscapasRequest;
use App\Http\Controllers\Controller;

use App\Models\Produtoscapas;

class ProdutoscapasController extends Controller
{
    public function index()
    {
        $registro = Produtoscapas::first();

        return view('painel.produtoscapas.edit', compact('registro'));
    }

    public function update(ProdutoscapasRequest $request, Produtoscapas $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['soldagem_eletrodo'])) $input['soldagem_eletrodo'] = Produtoscapas::upload_soldagem_eletrodo();
            if (isset($input['soldagem_eletrodo_ativo'])) $input['soldagem_eletrodo_ativo'] = Produtoscapas::upload_soldagem_eletrodo_ativo();
            if (isset($input['soldagem_tig'])) $input['soldagem_tig'] = Produtoscapas::upload_soldagem_tig();
            if (isset($input['soldagem_tig_ativo'])) $input['soldagem_tig_ativo'] = Produtoscapas::upload_soldagem_tig_ativo();
            if (isset($input['soldagem_migmag'])) $input['soldagem_migmag'] = Produtoscapas::upload_soldagem_migmag();
            if (isset($input['soldagem_migmag_ativo'])) $input['soldagem_migmag_ativo'] = Produtoscapas::upload_soldagem_migmag_ativo();
            if (isset($input['corte_plasma'])) $input['corte_plasma'] = Produtoscapas::upload_corte_plasma();
            if (isset($input['corte_plasma_ativo'])) $input['corte_plasma_ativo'] = Produtoscapas::upload_corte_plasma_ativo();
            if (isset($input['acessorios'])) $input['acessorios'] = Produtoscapas::upload_acessorios();
            if (isset($input['acessorios_ativo'])) $input['acessorios_ativo'] = Produtoscapas::upload_acessorios_ativo();

            $registro->update($input);

            return redirect()->route('painel.produtos.capas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
