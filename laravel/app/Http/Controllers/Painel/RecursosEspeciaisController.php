<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RecursosEspeciaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Recurso;
use App\Models\Produto;

class RecursosEspeciaisController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = Recurso::produto($produto->id)->ordenados()->get();

        return view('painel.recursos-especiais.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.recursos-especiais.create', compact('produto'));
    }

    public function store(RecursosEspeciaisRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Recurso::upload_imagem();

            Recurso::create($input);

            return redirect()->route('painel.produtos.recursos-especiais.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, Recurso $registro)
    {
        return view('painel.recursos-especiais.edit', compact('registro', 'produto'));
    }

    public function update(RecursosEspeciaisRequest $request, Produto $produto, Recurso $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Recurso::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.produtos.recursos-especiais.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, Recurso $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.recursos-especiais.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
