<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SetoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Setores;

class SetoresController extends Controller
{
    public function index()
    {
        $registro = Setores::first();

        return view('painel.setores.edit', compact('registro'));
    }

    public function update(SetoresRequest $request, Setores $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.setores.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
