<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SolicitacaoGarantia;

class SolicitacoesGarantiasController extends Controller
{
    public function index()
    {
        $contatosrecebidos = SolicitacaoGarantia::orderBy('created_at', 'DESC')->get();

        return view('painel.solicitacoes-de-garantias.index', compact('contatosrecebidos'));
    }

    public function show(SolicitacaoGarantia $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.solicitacoes-de-garantias.show', compact('contato'));
    }

    public function destroy(SolicitacaoGarantia $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.solicitacoes-de-garantias.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(SolicitacaoGarantia $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.solicitacoes-de-garantias.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
