<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SolicitacaoManual;

class SolicitacoesManuaisController extends Controller
{
    public function index()
    {
        $contatosrecebidos = SolicitacaoManual::orderBy('created_at', 'DESC')->get();

        return view('painel.solicitacoes-de-manuais.index', compact('contatosrecebidos'));
    }

    public function show(SolicitacaoManual $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.solicitacoes-de-manuais.show', compact('contato'));
    }

    public function destroy(SolicitacaoManual $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.solicitacoes-de-manuais.index')->with('success', 'Solicitação excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir solicitação: '.$e->getMessage()]);

        }
    }

    public function toggle(SolicitacaoManual $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.solicitacoes-de-manuais.index')->with('success', 'Solicitação alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar solicitação: '.$e->getMessage()]);

        }
    }
}
