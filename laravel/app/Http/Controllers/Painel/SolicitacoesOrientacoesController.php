<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SolicitacaoOrientacao;

class SolicitacoesOrientacoesController extends Controller
{
    public function index()
    {
        $contatosrecebidos = SolicitacaoOrientacao::orderBy('created_at', 'DESC')->get();

        return view('painel.solicitacoes-de-orientacoes.index', compact('contatosrecebidos'));
    }

    public function show(SolicitacaoOrientacao $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.solicitacoes-de-orientacoes.show', compact('contato'));
    }

    public function destroy(SolicitacaoOrientacao $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.solicitacoes-de-orientacoes.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(SolicitacaoOrientacao $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.solicitacoes-de-orientacoes.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
