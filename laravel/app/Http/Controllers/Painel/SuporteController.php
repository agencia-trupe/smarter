<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SuporteRequest;
use App\Http\Controllers\Controller;

use App\Models\Suporte;

class SuporteController extends Controller
{
    public function index()
    {
        $registro = Suporte::first();

        return view('painel.suporte.edit', compact('registro'));
    }

    public function update(SuporteRequest $request, Suporte $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['orientacoes_tecnicas_capa'])) $input['orientacoes_tecnicas_capa'] = Suporte::upload_orientacoes_tecnicas_capa();
            if (isset($input['garantia_de_produtos_capa'])) $input['garantia_de_produtos_capa'] = Suporte::upload_garantia_de_produtos_capa();
            if (isset($input['manuais_de_produtos_capa'])) $input['manuais_de_produtos_capa'] = Suporte::upload_manuais_de_produtos_capa();

            $registro->update($input);

            return redirect()->route('painel.suporte.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
