<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TutoriaisCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\TutorialCategoria;

class TutoriaisCategoriasController extends Controller
{
    public function index()
    {
        $categorias = TutorialCategoria::ordenados()->get();

        return view('painel.tutoriais.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.tutoriais.categorias.create');
    }

    public function store(TutoriaisCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            TutorialCategoria::create($input);
            return redirect()->route('painel.tutoriais.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(TutorialCategoria $categoria)
    {
        return view('painel.tutoriais.categorias.edit', compact('categoria'));
    }

    public function update(TutoriaisCategoriasRequest $request, TutorialCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.tutoriais.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(TutorialCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.tutoriais.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
