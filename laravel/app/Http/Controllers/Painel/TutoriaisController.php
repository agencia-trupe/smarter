<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TutoriaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Tutorial;
use App\Models\TutorialCategoria;

class TutoriaisController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = TutorialCategoria::ordenados()->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (TutorialCategoria::find($filtro)) {
            $registros = Tutorial::where('tutoriais_categoria_id', $filtro)->orderBy('data', 'DESC')->orderBy('id', 'DESC')->get();
        } else {
            $registros = Tutorial::orderBy('data', 'DESC')->orderBy('id', 'DESC')->get();
        }

        return view('painel.tutoriais.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.tutoriais.create', compact('categorias'));
    }

    public function store(TutoriaisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Tutorial::upload_capa();
            if (isset($input['thumb'])) $input['thumb'] = Tutorial::upload_thumb();

            Tutorial::create($input);

            return redirect()->route('painel.tutoriais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Tutorial $registro)
    {
        $categorias = $this->categorias;

        return view('painel.tutoriais.edit', compact('registro', 'categorias'));
    }

    public function update(TutoriaisRequest $request, Tutorial $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Tutorial::upload_capa();
            if (isset($input['thumb'])) $input['thumb'] = Tutorial::upload_thumb();

            $registro->update($input);

            return redirect()->route('painel.tutoriais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Tutorial $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.tutoriais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
