<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Models\Produto;

class VideosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = Video::produto($produto->id)->ordenados()->get();

        return view('painel.videos.index', compact('registros', 'produto'));
    }

    public function create(Produto $produto)
    {
        return view('painel.videos.create', compact('produto'));
    }

    public function store(VideosRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Video::upload_capa();

            Video::create($input);

            return redirect()->route('painel.produtos.videos.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, Video $registro)
    {
        return view('painel.videos.edit', compact('registro', 'produto'));
    }

    public function update(VideosRequest $request, Produto $produto, Video $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Video::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.produtos.videos.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, Video $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.videos.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
