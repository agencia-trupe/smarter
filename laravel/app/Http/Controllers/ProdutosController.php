<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Produto;
use App\Models\ProdutoLinha;
use App\Models\Produtoscapas;
use App\Models\AcessorioConsumivelEpi;
use App\Models\AcessorioConsumivelEpiCategoria;

class ProdutosController extends Controller
{
    public function index($categoria = null)
    {
        $capas      = Produtoscapas::first();
        $categorias = Produto::categoriasArray();

        if (!$categoria) {
            return view('frontend.produtos.home', compact('capas', 'categorias'));
        }

        if (!array_key_exists($categoria, $categorias)) abort('404');

        if ($categoria == 'acessorios-consumiveis-epis') {
            $acessorios = AcessorioConsumivelEpiCategoria::ordenados()->get();
        } else {
            $acessorios = null;
        }

        $linhas = ProdutoLinha::ordenados()->get();

        foreach ($linhas as $linha) {
            $linha->produtosCat = $linha->produtos->where('categoria', $categoria);
        }

        return view('frontend.produtos.index', compact('capas', 'categorias', 'categoria', 'linhas', 'acessorios'));
    }

    public function show($categoria, Produto $produto)
    {
        if ($produto->categoria !== $categoria) abort('404');

        return view('frontend.produtos.show', compact('produto', 'categoria'));
    }

    public function busca()
    {
        $termo = request('termo');

        if ($termo) {
            $produtos = Produto::ordenados()->busca($termo)->get();

            return view('frontend.produtos.busca', compact('termo', 'produtos'));
        }

        return redirect()->route('home');
    }

    public function acessorios(AcessorioConsumivelEpiCategoria $categoria)
    {
        return view('frontend.produtos.acessorios', compact('categoria'));
    }

    public function acessoriosShow(AcessorioConsumivelEpiCategoria $categoria, AcessorioConsumivelEpi $produto)
    {
        return view('frontend.produtos.acessorios-show', compact('categoria', 'produto'));
    }
}
