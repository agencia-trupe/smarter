<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\SolicitacoesGarantiasRequest;
use App\Http\Requests\SolicitacoesManuaisRequest;
use App\Http\Requests\SolicitacoesOrientacoesRequest;

use App\Models\Suporte;
use App\Models\Cabecalhos;
use App\Models\GarantiaDeProdutos;
use App\Models\SolicitacaoGarantia;
use App\Models\SolicitacaoManual;
use App\Models\SolicitacaoOrientacao;
use App\Models\ProdutoLinha;
use App\Models\Produto;
use App\Models\Contato;

class SuporteController extends Controller
{
    public function index()
    {
        $suporte = Suporte::first();
        $cabecalhoImg = \App\Models\Cabecalhos::first()->suporte;

        return view('frontend.suporte.index', compact('suporte', 'cabecalhoImg'));
    }

    public function garantiaDeProdutos()
    {
        $suporte            = Suporte::first();
        $garantiaDeProdutos = GarantiaDeProdutos::first();
        $produtos           = Produto::ordenados()->lists('titulo_'.app()->getLocale(), 'id');

        return view('frontend.suporte.garantia-de-produtos.index', compact('suporte', 'garantiaDeProdutos', 'produtos'));
    }

    public function garantiaDeProdutosMais()
    {
        $suporte            = Suporte::first();
        $garantiaDeProdutos = GarantiaDeProdutos::first();

        return view('frontend.suporte.garantia-de-produtos.mais', compact('suporte', 'garantiaDeProdutos'));
    }

    public function garantiaDeProdutosPost(SolicitacoesGarantiasRequest $request, SolicitacaoGarantia $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('arquivo')) {
            $input['arquivo'] = SolicitacaoGarantia::upload_arquivo();
        } else {
            $input['arquivo'] = '';
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.garantia', $input, function($message) use ($request, $contato)
            {
                $message->to('suporte@smarterbrasil.com.br', config('site.name'))
                        ->subject('[SOLICITAÇÃO DE GARANTIA] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }

    public function manuaisDeProdutos()
    {
        $suporte = Suporte::first();
        $linhas  = ProdutoLinha::ordenados()->get();

        return view('frontend.suporte.manuais', compact('suporte', 'linhas'));
    }

    public function manuaisDeProdutosPost(SolicitacoesManuaisRequest $request, $produtoId, SolicitacaoManual $contatoRecebido)
    {
        $produto = Produto::findOrFail($produtoId);
        $input   = $request->all();

        if ($request->hasFile('nf')) {
            $input['nf'] = SolicitacaoManual::upload_arquivo();
        }

        $contatoRecebido->create($input);

        session()->flash('downloadManual', $produto->manual);

        return back();
    }

    public function orientacoesTecnicas($linhaId = null)
    {
        $suporte = Suporte::first();
        $linhas  = ProdutoLinha::ordenados()->get();

        return view('frontend.suporte.orientacoes', compact('suporte', 'linhas'));
    }

    public function orientacoesTecnicasPost(SolicitacoesOrientacoesRequest $request, SolicitacaoOrientacao $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('arquivo')) {
            $input['arquivo'] = SolicitacaoOrientacao::upload_arquivo();
        } else {
            $input['arquivo'] = '';
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orientacao', $input, function($message) use ($request, $contato)
            {
                $message->to(['suporte@smarterbrasil.com.br', 'comercial@smarterbrasil.com.br'], config('site.name'))
                        ->subject('[SOLICITAÇÃO DE ORIENTAÇÃO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return redirect()->route('suporte.orientacoes-tecnicas')->with('success', true);
    }
}
