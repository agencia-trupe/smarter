<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Tutorial;
use App\Models\TutorialCategoria;

class TutoriaisController extends Controller
{
    public function index(TutorialCategoria $categoria)
    {
        $cabecalhoImg = \App\Models\Cabecalhos::first()->tutoriais;

        $categorias = TutorialCategoria::ordenados()->get();

        if (!$categoria->exists) {
            $tutoriais = Tutorial::orderBy('data', 'DESC')->orderBy('id', 'DESC')->paginate(5, ['*'], 'pagina');
            $primeiro  = Tutorial::orderBy('data', 'DESC')->orderBy('id', 'DESC')->first() ? Tutorial::orderBy('data', 'DESC')->orderBy('id', 'DESC')->first()->id : null;
        } else {
            $tutoriais = $categoria->tutoriais()->paginate(5, ['*'], 'pagina');
            $primeiro  = $categoria->tutoriais()->first() ? $categoria->tutoriais()->first()->id : null;
        }


        return view('frontend.tutoriais.index', compact('categorias', 'categoria', 'tutoriais', 'primeiro', 'cabecalhoImg'));
    }

    public function show(TutorialCategoria $categoria, Tutorial $tutorial)
    {
        $data_post = \Carbon\Carbon::createFromFormat('d/m/Y', $tutorial->data)->format('Y-m-d');

        $anterior = Tutorial::where('data', '<', $data_post)
                          ->orWhere(function($query) use ($data_post, $tutorial) {
                              $query->where('data', $data_post)
                                    ->where('id', '<', $tutorial->id);
                          })
                          ->orderBy('data', 'DESC')
                          ->orderBy('id', 'DESC')
                          ->first();

        $proximo = Tutorial::where('data', '>', $data_post)
                          ->orWhere(function($query) use ($data_post, $tutorial) {
                              $query->where('data', $data_post)
                                    ->where('id', '>', $tutorial->id);
                          })
                          ->orderBy('data', 'ASC')
                          ->orderBy('id', 'ASC')
                          ->first();

        return view('frontend.tutoriais.show', compact('categoria', 'tutorial', 'anterior', 'proximo'));
    }
}
