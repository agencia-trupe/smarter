<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AcessoriosConsumiveisEpisCategoriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt'    => 'required',
            'titulo_en'    => 'required',
            'capa'         => 'required|image',
            'descricao_pt' => 'required',
            'descricao_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
