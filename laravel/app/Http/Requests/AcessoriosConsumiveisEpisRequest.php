<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AcessoriosConsumiveisEpisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'acessorios_categoria_id' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'imagem' => 'required|image',
            'codigo' => 'required',
            'modelo' => 'required',
            'descricao_pt' => 'required',
            'descricao_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
