<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AcessoriosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produto_id' => 'required',
            'tipo' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'imagem' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
