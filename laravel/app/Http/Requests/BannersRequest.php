<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'background' => 'required|image',
            'imagem' => 'image',
            'icone' => 'image',
            'titulo_pt' => '',
            'titulo_en' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['background'] = 'image';
            $rules['imagem'] = 'image';
            $rules['icone'] = 'image';
        }

        return $rules;
    }
}
