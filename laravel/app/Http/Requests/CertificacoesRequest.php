<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CertificacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'imagem' => 'required|image',
            'imagem_cinza' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
            $rules['imagem_cinza'] = 'image';
        }

        return $rules;
    }
}
