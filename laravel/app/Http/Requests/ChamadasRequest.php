<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'image',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
