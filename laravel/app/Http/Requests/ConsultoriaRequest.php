<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsultoriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1_pt' => 'required',
            'texto_1_en' => 'required',
            'imagem_consultoria' => 'image',
            'texto_2_pt' => 'required',
            'texto_2_en' => 'required',
            'imagem_contato' => 'image',
        ];
    }
}
