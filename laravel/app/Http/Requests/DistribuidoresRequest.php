<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DistribuidoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'estado' => 'required',
            'cidade' => 'required',
            'empresa' => 'required',
            'endereco' => '',
            'cep' => '',
            'telefone' => '',
            'e_mail' => '',
            'site' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
