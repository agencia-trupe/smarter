<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DownloadsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produto_id' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
