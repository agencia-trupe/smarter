<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EcommercesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'site' => 'required',
            'marca' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
