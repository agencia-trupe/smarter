<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1_pt' => 'required',
            'texto_1_en' => 'required',
            'imagem' => 'image',
            'mapa' => 'image',
            'texto_2_pt' => 'required',
            'texto_2_en' => 'required',
        ];
    }
}
