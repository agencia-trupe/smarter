<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EspecificacoesTecnicasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produto_id' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'valor_pt' => 'required',
            'valor_en' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
