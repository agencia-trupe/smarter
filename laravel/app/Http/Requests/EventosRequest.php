<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_calendario_pt' => 'required',
            'titulo_calendario_en' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
