<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventoscapaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'capa' => 'required|image',
        ];
    }
}
