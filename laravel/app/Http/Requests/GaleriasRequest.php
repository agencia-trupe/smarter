<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GaleriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'capa' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
