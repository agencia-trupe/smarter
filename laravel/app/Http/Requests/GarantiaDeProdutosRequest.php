<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GarantiaDeProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_abertura_pt' => 'required',
            'texto_abertura_en' => 'required',
            'imagem_abertura' => 'image',
            'telefone' => 'required',
            'e_mail' => 'required',
            'whatsapp' => 'required',
            'texto_saiba_mais_pt' => 'required',
            'texto_saiba_mais_en' => 'required',
            'imagem_saiba_mais' => 'image',
        ];
    }
}
