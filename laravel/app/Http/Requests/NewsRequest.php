<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'novidades_pt' => 'required',
            'novidades_en' => 'required',
            'galeria_pt' => 'required',
            'galeria_en' => 'required',
        ];
    }
}
