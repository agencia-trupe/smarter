<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NoticiasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'chamada_pt' => 'required',
            'chamada_en' => 'required',
            'capa' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
