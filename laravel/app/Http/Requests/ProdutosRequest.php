<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_linha_id' => 'required',
            'categoria' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'chamada_pt' => 'required',
            'chamada_en' => 'required',
            'capa' => 'required|image',
            'tabela_pt' => 'required|image',
            'tabela_en' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['tabela_pt'] = 'image';
            $rules['tabela_en'] = 'image';
        }

        return $rules;
    }
}
