<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutoscapasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'soldagem_eletrodo' => 'image',
            'soldagem_eletrodo_ativo' => 'image',
            'soldagem_tig' => 'image',
            'soldagem_tig_ativo' => 'image',
            'soldagem_migmag' => 'image',
            'soldagem_migmag_ativo' => 'image',
            'acessorios' => 'image',
            'acessorios_ativo' => 'image',
        ];
    }
}
