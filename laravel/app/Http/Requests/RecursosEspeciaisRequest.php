<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RecursosEspeciaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produto_id' => 'required',
            'imagem' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
