<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SetoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gestao_comercial' => 'required',
            'financeiro' => 'required',
            'apoio_administrativo_e_comercial' => 'required',
            'assistencia_tecnica_e_pos_venda' => 'required',
            'suporte_tecnico_de_soldagem_e_inspecao' => 'required',
            'marketing' => 'required',
        ];
    }
}
