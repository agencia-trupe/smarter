<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SuporteRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'orientacoes_tecnicas_abertura_pt' => 'required',
            'orientacoes_tecnicas_abertura_en' => 'required',
            'orientacoes_tecnicas_capa' => 'image',
            'garantia_de_produtos_abertura_pt' => 'required',
            'garantia_de_produtos_abertura_en' => 'required',
            'garantia_de_produtos_capa' => 'image',
            'manuais_de_produtos_abertura_pt' => 'required',
            'manuais_de_produtos_abertura_en' => 'required',
            'manuais_de_produtos_capa' => 'image',
        ];
    }
}
