<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('smarter-no-mundo', 'EmpresaController@index')->name('empresa');
    Route::get('produtos/acessorios/{acessorios_slug}', 'ProdutosController@acessorios')->name('produtos.acessorios');
    Route::get('produtos/acessorios/{acessorios_slug}/{acessorio_slug}', 'ProdutosController@acessoriosShow')->name('produtos.acessorios.show');
    Route::get('produtos/{produtos_categoria?}', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/{produtos_categoria}/{produtos_slug}', 'ProdutosController@show')->name('produtos.show');
    Route::get('tutoriais/{tutoriais_categoria?}', 'TutoriaisController@index')->name('tutoriais');
    Route::get('tutoriais/{tutoriais_categoria}/{tutoriais_slug}', 'TutoriaisController@show')->name('tutoriais.show');
    Route::get('suporte', 'SuporteController@index')->name('suporte');
    Route::get('suporte/orientacoes-tecnicas', 'SuporteController@orientacoesTecnicas')->name('suporte.orientacoes-tecnicas');
    Route::post('suporte/orientacoes-tecnicas/{linha_id}/{produto_id}', 'SuporteController@orientacoesTecnicasPost')->name('suporte.orientacoes-tecnicas.post');
    Route::get('suporte/garantia-de-produtos', 'SuporteController@garantiaDeProdutos')->name('suporte.garantia-de-produtos');
    Route::post('suporte/garantia-de-produtos', 'SuporteController@garantiaDeProdutosPost')->name('suporte.garantia-de-produtos.post');
    Route::get('suporte/garantia-de-produtos/saiba-mais', 'SuporteController@garantiaDeProdutosMais')->name('suporte.garantia-de-produtos.saiba-mais');
    Route::get('suporte/manuais-de-produtos', 'SuporteController@manuaisDeProdutos')->name('suporte.manuais-de-produtos');
    Route::post('suporte/manuais-de-produtos/{produto_id}', 'SuporteController@manuaisDeProdutosPost')->name('suporte.manuais-de-produtos.post');
    Route::get('onde-comprar/{estado?}', 'OndeComprarController@index')->name('onde-comprar');
    Route::get('consultoria', 'ConsultoriaController@index')->name('consultoria');
    Route::post('consultoria', 'ConsultoriaController@post')->name('consultoria.post');
    Route::get('news', 'NewsController@index')->name('news');
    Route::get('news/novidades', 'NoticiasController@index')->name('news.noticias');
    Route::get('news/novidades/{noticias_slug}', 'NoticiasController@show')->name('news.noticias.show');
    Route::get('news/calendario-de-eventos', 'EventosController@index')->name('news.eventos');
    Route::get('news/galeria', 'GaleriaController@index')->name('news.galeria');
    Route::get('news/galeria/{galeria_slug}', 'GaleriaController@show')->name('news.galeria.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('busca', 'ProdutosController@busca')->name('busca');


    // Localização
    Route::get('idioma/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en') Session::put('locale', $idioma);
        return redirect()->route('home');
    })->name('lang');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('setores', 'SetoresController', ['only' => ['index', 'update']]);
		Route::resource('news', 'NewsController', ['only' => ['index', 'update']]);
		Route::resource('catalogo', 'CatalogoController', ['only' => ['index', 'update']]);
		Route::resource('distribuidores', 'DistribuidoresController');
		Route::resource('ecommerces', 'EcommercesController');
		Route::resource('onde-comprar', 'OndeComprarController', ['only' => ['index', 'update']]);
		Route::resource('acessorios-consumiveis-epis/categorias', 'AcessoriosConsumiveisEpisCategoriasController', ['parameters' => ['categorias' => 'categorias_acessorios']]);
		Route::resource('acessorios-consumiveis-epis', 'AcessoriosConsumiveisEpisController');
        Route::resource('acessorios-consumiveis-epis.destaques', 'AcessoriosDestaquesController', ['parameters' => ['destaques' => 'destaques_acessorios']]);
        Route::get('acessorios-consumiveis-epis/{acessorios_consumiveis_epis}/imagens/clear', [
            'as'   => 'painel.acessorios-consumiveis-epis.imagens.clear',
            'uses' => 'AcessoriosImagensController@clear'
        ]);
        Route::resource('acessorios-consumiveis-epis.imagens', 'AcessoriosImagensController', ['parameters' => ['imagens' => 'imagens_acessorios']]);
        Route::get('consultoria/recebidos/{consultoria_recebidos}/toggle', ['as' => 'painel.consultoria.recebidos.toggle', 'uses' => 'ConsultoriaRecebidosController@toggle']);
        Route::resource('consultoria/recebidos', 'ConsultoriaRecebidosController', ['parameters' => ['recebidos' => 'consultoria_recebidos']]);
		Route::resource('consultoria', 'ConsultoriaController', ['only' => ['index', 'update']]);
        Route::resource('garantia-de-produtos', 'GarantiaDeProdutosController', ['only' => ['index', 'update']]);

        Route::get('solicitacoes-de-manuais/{solicitacoes_de_manuais}/toggle', ['as' => 'painel.solicitacoes-de-manuais.toggle', 'uses' => 'SolicitacoesManuaisController@toggle']);
        Route::resource('solicitacoes-de-manuais', 'SolicitacoesManuaisController');

        Route::get('solicitacoes-de-garantias/{solicitacoes_de_garantias}/toggle', ['as' => 'painel.solicitacoes-de-garantias.toggle', 'uses' => 'SolicitacoesGarantiasController@toggle']);
        Route::resource('solicitacoes-de-garantias', 'SolicitacoesGarantiasController');

        Route::get('solicitacoes-de-orientacoes/{solicitacoes_de_orientacoes}/toggle', ['as' => 'painel.solicitacoes-de-orientacoes.toggle', 'uses' => 'SolicitacoesOrientacoesController@toggle']);
        Route::resource('solicitacoes-de-orientacoes', 'SolicitacoesOrientacoesController');

        Route::resource('suporte', 'SuporteController', ['only' => ['index', 'update']]);
        Route::resource('produtos/linhas', 'ProdutosLinhasController', ['parameters' => ['linhas' => 'linhas_produtos']]);
        Route::resource('produtos/capas', 'ProdutoscapasController', ['only' => ['index', 'update']]);
        Route::resource('produtos', 'ProdutosController');
        Route::get('produtos/{produtos}/imagens/clear', [
            'as'   => 'painel.produtos.imagens.clear',
            'uses' => 'ProdutosImagensController@clear'
        ]);
        Route::resource('produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'imagens_produtos']]);
        Route::get('produtos/{produtos}/fotos/clear', [
            'as'   => 'painel.produtos.fotos.clear',
            'uses' => 'ProdutosFotosController@clear'
        ]);
        Route::resource('produtos.fotos', 'ProdutosFotosController', ['parameters' => ['fotos' => 'fotos_produtos']]);
        Route::resource('produtos.videos', 'VideosController');
        Route::resource('produtos.downloads', 'DownloadsController');
        Route::resource('produtos.certificacoes', 'CertificacoesProdutosController', ['parameters' => ['certificacoes' => 'certificacoes_produtos']]);
        Route::resource('produtos.recursos-especiais', 'RecursosEspeciaisController');
        Route::resource('produtos.acessorios', 'AcessoriosController');
        Route::resource('produtos.destaques', 'DestaquesController');
        Route::resource('produtos.especificacoes-tecnicas', 'EspecificacoesTecnicasController');
        Route::resource('galerias', 'GaleriasController');
        Route::get('galerias/{galerias}/imagens/clear', [
            'as'   => 'painel.galerias.imagens.clear',
            'uses' => 'GaleriasImagensController@clear'
        ]);
        Route::resource('galerias.imagens', 'GaleriasImagensController', ['parameters' => ['imagens' => 'imagens_galerias']]);
        Route::resource('noticias', 'NoticiasController');
		Route::resource('eventos/capa', 'EventoscapaController', ['only' => ['index', 'update']]);
        Route::resource('eventos', 'EventosController');
        Route::resource('tutoriais/categorias', 'TutoriaisCategoriasController', ['parameters' => ['categorias' => 'categorias_tutoriais']]);
        Route::resource('tutoriais', 'TutoriaisController');
		Route::resource('escritorios', 'EscritoriosController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('cabecalhos', 'CabecalhosController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('chamadas', 'ChamadasController');
		Route::resource('certificacoes', 'CertificacoesController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');

        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');

        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
