<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Acessorio extends Model
{
    protected $table = 'acessorios';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 290,
            'height' => 115,
            'bgcolor' => '#fff',
            'path'   => 'assets/img/acessorios/'
        ]);
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }
}
