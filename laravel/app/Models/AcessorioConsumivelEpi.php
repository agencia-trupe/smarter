<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class AcessorioConsumivelEpi extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'acessorios_consumiveis_epis';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('acessorios_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\AcessorioConsumivelEpiCategoria', 'acessorios_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\AcessorioImagem', 'acessorios_id')->ordenados();
    }

    public function destaques()
    {
        return $this->hasMany('App\Models\AcessorioDestaque', 'acessorios_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'   => 500,
            'height'  => 400,
            'bgcolor' => '#fff',
            'path'    => 'assets/img/acessoriosprodutos/'
        ]);
    }
}
