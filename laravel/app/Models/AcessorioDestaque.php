<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AcessorioDestaque extends Model
{
    protected $table = 'acessorios_consumiveis_epis_destaques';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 90,
            'height' => null,
            'path'   => 'assets/img/destaques/'
        ]);
    }

    public function scopeAcessorio($query, $id)
    {
        return $query->where('acessorios_id', $id);
    }
}
