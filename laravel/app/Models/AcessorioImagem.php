<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AcessorioImagem extends Model
{
    protected $table = 'acessorios_consumiveis_epis_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeAcessorio($query, $id)
    {
        return $query->where('acessorios_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'   => 450,
                'height'  => 450,
                'path'    => 'assets/img/produtos/imagens/'
            ]
        ]);
    }
}
