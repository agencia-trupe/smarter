<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_background()
    {
        return CropImage::make('background', [
            'width'  => 1100,
            'height' => 412,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_background_mobile()
    {
        return CropImage::make('background_mobile', [
            'width'  => 768,
            'height' => 500,
            'path'   => 'assets/img/banners/mobile/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 275,
            'height' => 140,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'       => 275,
            'height'      => 140,
            'transparent' => true,
            'path'        => 'assets/img/banners/'
        ]);
    }
}
