<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cabecalhos extends Model
{
    protected $table = 'cabecalhos';

    protected $guarded = ['id'];

    public static function upload_empresa()
    {
        return CropImage::make('empresa', [
            'width'  => 1280,
            'height' => 125,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_tutoriais()
    {
        return CropImage::make('tutoriais', [
            'width'  => 1280,
            'height' => 125,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_suporte()
    {
        return CropImage::make('suporte', [
            'width'  => 1280,
            'height' => 125,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_onde_comprar()
    {
        return CropImage::make('onde_comprar', [
            'width'  => 1280,
            'height' => 125,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_consultoria()
    {
        return CropImage::make('consultoria', [
            'width'  => 1280,
            'height' => 125,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_contato()
    {
        return CropImage::make('contato', [
            'width'  => 1280,
            'height' => 125,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

}
