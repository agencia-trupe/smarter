<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Catalogo extends Model
{
    protected $table = 'catalogo';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 160,
            'height' => null,
            'path'   => 'assets/img/catalogo/'
        ]);
    }

    public static function upload_arquivo() {
        $file = request()->file('arquivo');

        $path = 'catalogos/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
