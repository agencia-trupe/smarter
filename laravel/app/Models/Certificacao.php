<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Certificacao extends Model
{
    protected $table = 'certificacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 45,
            'height' => 28,
            'path'   => 'assets/img/certificacoes/'
        ]);
    }

    public static function upload_imagem_cinza()
    {
        return CropImage::make('imagem_cinza', [
            'width'  => 45,
            'height' => 28,
            'path'   => 'assets/img/certificacoes/'
        ]);
    }
}
