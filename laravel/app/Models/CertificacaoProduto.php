<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CertificacaoProduto extends Model
{
    protected $table = 'certificacoes_produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 60,
            'height' => 37,
            'path'   => 'assets/img/certificacoes-produtos/'
        ]);
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }
}
