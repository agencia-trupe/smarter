<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Consultoria extends Model
{
    protected $table = 'consultoria';

    protected $guarded = ['id'];

    public static function upload_imagem_consultoria()
    {
        return CropImage::make('imagem_consultoria', [
            'width'  => 1400,
            'height' => 350,
            'path'   => 'assets/img/consultoria/'
        ]);
    }

    public static function upload_imagem_consultoria_mobile()
    {
        return CropImage::make('imagem_consultoria_mobile', [
            'width'  => 768,
            'height' => 500,
            'path'   => 'assets/img/consultoria/mobile/'
        ]);
    }

    public static function upload_imagem_contato()
    {
        return CropImage::make('imagem_contato', [
            'width'  => 1400,
            'height' => 350,
            'path'   => 'assets/img/consultoria/'
        ]);
    }

}
