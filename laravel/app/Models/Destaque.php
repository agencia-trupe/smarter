<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Destaque extends Model
{
    protected $table = 'destaques';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 90,
            'height' => null,
            'path'   => 'assets/img/destaques/'
        ]);
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }
}
