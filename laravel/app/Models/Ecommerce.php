<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Ecommerce extends Model
{
    protected $table = 'ecommerces';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'   => 400,
            'height'  => 400,
            'bgcolor' => '#fff',
            'path'    => 'assets/img/ecommerces/'
        ]);
    }
}
