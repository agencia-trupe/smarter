<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Galeria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'galerias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\GaleriaImagem', 'galeria_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 225,
                'height' => 155,
                'path'   => 'assets/img/galerias/capa/'
            ],
            [
                'width'  => 690,
                'height' => 380,
                'path'   => 'assets/img/galerias/banner/'
            ],
        ]);
    }
}
