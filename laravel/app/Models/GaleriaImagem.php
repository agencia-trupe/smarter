<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class GaleriaImagem extends Model
{
    protected $table = 'galerias_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeGaleria($query, $id)
    {
        return $query->where('galeria_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/galerias/imagens/thumbs/'
            ],
            [
                'width'   => 85,
                'height'  => 60,
                'path'    => 'assets/img/galerias/imagens/thumbs-pequenos/'
            ],
            [
                'width'   => 930,
                'height'  => 400,
                'galeria' => true,
                'bgcolor' => '#909090',
                'path'    => 'assets/img/galerias/imagens/'
            ]
        ]);
    }
}
