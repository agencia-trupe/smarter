<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class GarantiaDeProdutos extends Model
{
    protected $table = 'garantia_de_produtos';

    protected $guarded = ['id'];

    public static function upload_imagem_abertura()
    {
        return CropImage::make('imagem_abertura', [
            'width'  => 430,
            'height' => null,
            'path'   => 'assets/img/garantia-de-produtos/'
        ]);
    }

    public static function upload_imagem_saiba_mais()
    {
        return CropImage::make('imagem_saiba_mais', [
            'width'  => 970,
            'height' => 320,
            'path'   => 'assets/img/garantia-de-produtos/'
        ]);
    }

}
