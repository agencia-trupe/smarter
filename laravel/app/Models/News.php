<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class News extends Model
{
    protected $table = 'news';

    protected $guarded = ['id'];

}
