<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;

class Noticia extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'noticias';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 970,
                'height' => 400,
                'path'   => 'assets/img/noticias/banner-grande/'
            ],
            [
                'width'  => 690,
                'height' => 380,
                'path'   => 'assets/img/noticias/banner-pequeno/'
            ],
            [
                'width'  => 970,
                'height' => 345,
                'path'   => 'assets/img/noticias/capa/'
            ],
        ]);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}
