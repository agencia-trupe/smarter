<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class OndeComprar extends Model
{
    protected $table = 'onde_comprar';

    protected $guarded = ['id'];

    public static function upload_imagem_televendas()
    {
        return CropImage::make('imagem_televendas', [
            'width'  => 1400,
            'height' => 175,
            'path'   => 'assets/img/onde-comprar/'
        ]);
    }

    public static function upload_imagem_distribuidores()
    {
        return CropImage::make('imagem_distribuidores', [
            'width'  => 1400,
            'height' => 175,
            'path'   => 'assets/img/onde-comprar/'
        ]);
    }

}
