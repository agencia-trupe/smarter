<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeLinha($query, $linha_id)
    {
        return $query->where('produtos_linha_id', $linha_id);
    }

    public function linha()
    {
        return $this->belongsTo('App\Models\ProdutoLinha', 'produtos_linha_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public function fotos()
    {
        return $this->hasMany('App\Models\ProdutoFoto', 'produto_id')->ordenados();
    }

    public function certificacoes()
    {
        return $this->hasMany('App\Models\CertificacaoProduto', 'produto_id')->ordenados();
    }

    public function recursos()
    {
        return $this->hasMany('App\Models\Recurso', 'produto_id')->ordenados();
    }

    public function acessorios()
    {
        return $this->hasMany('App\Models\Acessorio', 'produto_id')->ordenados();
    }

    public function destaques()
    {
        return $this->hasMany('App\Models\Destaque', 'produto_id')->ordenados();
    }

    public function especificacoes()
    {
        return $this->hasMany('App\Models\Especificacao', 'produto_id')->ordenados();
    }

    public function downloads()
    {
        return $this->hasMany('App\Models\Download', 'produto_id')->ordenados();
    }

    public function videos()
    {
        return $this->hasMany('App\Models\Video', 'produto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'   => 500,
            'height'  => 400,
            'bgcolor' => '#fff',
            'path'    => 'assets/img/produtos/'
        ]);
    }

    public static function upload_tabela_pt()
    {
        return CropImage::make('tabela_pt', [
            'width'   => 445,
            'height'  => null,
            'path'    => 'assets/img/produtos/tabelas/'
        ]);
    }

    public static function upload_tabela_en()
    {
        return CropImage::make('tabela_en', [
            'width'   => 445,
            'height'  => null,
            'path'    => 'assets/img/produtos/tabelas/'
        ]);
    }

    public static function categoriasArray()
    {
        return [
            'soldagem-eletrodo'           => 'Soldagem Eletrodo',
            'soldagem-tig'                => 'Soldagem TIG',
            'soldagem-mig-mag'            => 'Soldagem MIG/MAG',
            'corte-plasma'                => 'Corte Plasma',
            'acessorios-consumiveis-epis' => 'Acessórios, Consumíveis & EPI\'s'
        ];
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo_'.app()->getLocale(), 'LIKE', "%{$termo}%");
    }

    public static function upload_arquivo() {
        $file = request()->file('manual');

        $path = 'manuais/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
