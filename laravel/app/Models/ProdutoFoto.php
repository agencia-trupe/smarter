<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoFoto extends Model
{
    protected $table = 'produtos_fotos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadFoto()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 285,
                'height'  => 170,
                'path'    => 'assets/img/produtos/fotos/thumbs/'
            ],
            [
                'width'   => null,
                'height'  => null,
                'path'    => 'assets/img/produtos/fotos/'
            ]
        ]);
    }
}
