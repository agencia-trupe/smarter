<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Produtoscapas extends Model
{
    protected $table = 'produtoscapas';

    protected $guarded = ['id'];

    public static function upload_soldagem_eletrodo()
    {
        return CropImage::make('soldagem_eletrodo', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_soldagem_eletrodo_ativo()
    {
        return CropImage::make('soldagem_eletrodo_ativo', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_soldagem_tig()
    {
        return CropImage::make('soldagem_tig', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_soldagem_tig_ativo()
    {
        return CropImage::make('soldagem_tig_ativo', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_soldagem_migmag()
    {
        return CropImage::make('soldagem_migmag', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_soldagem_migmag_ativo()
    {
        return CropImage::make('soldagem_migmag_ativo', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_corte_plasma()
    {
        return CropImage::make('corte_plasma', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_corte_plasma_ativo()
    {
        return CropImage::make('corte_plasma_ativo', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_acessorios()
    {
        return CropImage::make('acessorios', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

    public static function upload_acessorios_ativo()
    {
        return CropImage::make('acessorios_ativo', [
            'width'  => 1400,
            'height' => 260,
            'path'   => 'assets/img/produtoscapas/'
        ]);
    }

}
