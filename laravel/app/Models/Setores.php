<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Setores extends Model
{
    protected $table = 'setores';

    protected $guarded = ['id'];

}
