<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Suporte extends Model
{
    protected $table = 'suporte';

    protected $guarded = ['id'];

    public static function upload_orientacoes_tecnicas_capa()
    {
        return CropImage::make('orientacoes_tecnicas_capa', [
            'width'  => 620,
            'height' => 310,
            'path'   => 'assets/img/suporte/'
        ]);
    }

    public static function upload_garantia_de_produtos_capa()
    {
        return CropImage::make('garantia_de_produtos_capa', [
            'width'  => 620,
            'height' => 310,
            'path'   => 'assets/img/suporte/'
        ]);
    }

    public static function upload_manuais_de_produtos_capa()
    {
        return CropImage::make('manuais_de_produtos_capa', [
            'width'  => 620,
            'height' => 310,
            'path'   => 'assets/img/suporte/'
        ]);
    }

}
