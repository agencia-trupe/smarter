<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;

class Tutorial extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'tutoriais';

    protected $guarded = ['id'];

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('tutoriais_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\TutorialCategoria', 'tutoriais_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 970,
            'height' => 345,
            'path'   => 'assets/img/tutoriais/capa/'
        ]);
    }

    public static function upload_thumb()
    {
        return CropImage::make('thumb', [
            [
                'width'  => 475,
                'height' => 345,
                'path'   => 'assets/img/tutoriais/thumb-grande/'
            ],
            [
                'width'  => 275,
                'height' => 200,
                'path'   => 'assets/img/tutoriais/thumb-pequeno/'
            ]
        ]);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}
