<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class TutorialCategoria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'tutoriais_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function tutoriais()
    {
        return $this->hasMany('App\Models\Tutorial', 'tutoriais_categoria_id')->orderBy('data', 'DESC');
    }
}
