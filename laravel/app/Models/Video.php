<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Video extends Model
{
    protected $table = 'videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 285,
            'height' => 170,
            'path'   => 'assets/img/videos/'
        ]);
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }
}
