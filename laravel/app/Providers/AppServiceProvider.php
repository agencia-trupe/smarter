<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('frontend.common.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('certificacoes', \App\Models\Certificacao::ordenados()->get());
            $view->with('catalogo', \App\Models\Catalogo::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('garantiasNaoLidos', \App\Models\SolicitacaoGarantia::naoLidos()->count());
            $view->with('orientacoesNaoLidos', \App\Models\SolicitacaoOrientacao::naoLidos()->count());
            $view->with('manuaisNaoLidos', \App\Models\SolicitacaoManual::naoLidos()->count());
            $view->with('consultoriaNaoLidos', \App\Models\ConsultoriaRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
