<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('setores', 'App\Models\Setores');
		$router->model('news', 'App\Models\News');
		$router->model('catalogo', 'App\Models\Catalogo');
		$router->model('distribuidores', 'App\Models\Distribuidor');
		$router->model('ecommerces', 'App\Models\Ecommerce');
		$router->model('onde-comprar', 'App\Models\OndeComprar');
		$router->model('acessorios-consumiveis-epis', 'App\Models\AcessorioConsumivelEpi');
		$router->model('categorias_acessorios', 'App\Models\AcessorioConsumivelEpiCategoria');
        $router->model('imagens_acessorios', 'App\Models\AcessorioImagem');
        $router->model('destaques_acessorios', 'App\Models\AcessorioDestaque');
        $router->model('consultoria', 'App\Models\Consultoria');
        $router->model('capas', 'App\Models\Produtoscapas');
        $router->model('capa', 'App\Models\Eventoscapa');
        $router->model('solicitacoes-de-manuais', 'App\Models\SolicitacaoManual');
        $router->model('solicitacoes-de-orientacoes', 'App\Models\SolicitacaoOrientacao');
        $router->model('solicitacoes-de-garantias', 'App\Models\SolicitacaoGarantia');
        $router->model('garantia-de-produtos', 'App\Models\GarantiaDeProdutos');
        $router->model('suporte', 'App\Models\Suporte');
        $router->model('videos', 'App\Models\Video');
        $router->model('downloads', 'App\Models\Download');
        $router->model('certificacoes-produtos', 'App\Models\CertificacaoProduto');
        $router->model('recursos-especiais', 'App\Models\Recurso');
        $router->model('acessorios', 'App\Models\Acessorio');
        $router->model('destaques', 'App\Models\Destaque');
        $router->model('especificacoes-tecnicas', 'App\Models\Especificacao');
        $router->model('produtos', 'App\Models\Produto');
        $router->model('linhas_produtos', 'App\Models\ProdutoLinha');
        $router->model('imagens_produtos', 'App\Models\ProdutoImagem');
		$router->model('fotos_produtos', 'App\Models\ProdutoFoto');
		$router->model('galerias', 'App\Models\Galeria');
		$router->model('imagens_galerias', 'App\Models\GaleriaImagem');
		$router->model('noticias', 'App\Models\Noticia');
		$router->model('eventos', 'App\Models\Evento');
		$router->model('tutoriais', 'App\Models\Tutorial');
		$router->model('categorias_tutoriais', 'App\Models\TutorialCategoria');
		$router->model('escritorios', 'App\Models\Escritorio');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('cabecalhos', 'App\Models\Cabecalhos');
		$router->model('banners', 'App\Models\Banner');
		$router->model('chamadas', 'App\Models\Chamada');
		$router->model('certificacoes', 'App\Models\Certificacao');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('consultoria_recebidos', 'App\Models\ConsultoriaRecebido');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('produtos_slug', function($value) {
            return \App\Models\Produto::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('acessorios_slug', function($value) {
            return \App\Models\AcessorioConsumivelEpiCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('acessorio_slug', function($value) {
            return \App\Models\AcessorioConsumivelEpi::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('noticias_slug', function($value) {
            return \App\Models\Noticia::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('galeria_slug', function($value) {
            return \App\Models\Galeria::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('tutoriais_categoria', function($value) {
            return \App\Models\TutorialCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('tutoriais_slug', function($value) {
            return \App\Models\Tutorial::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
