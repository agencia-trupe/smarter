-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: smarter
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalogo`
--

DROP TABLE IF EXISTS `catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo`
--

LOCK TABLES `catalogo` WRITE;
/*!40000 ALTER TABLE `catalogo` DISABLE KEYS */;
INSERT INTO `catalogo` VALUES (1,'',NULL,NULL);
/*!40000 ALTER TABLE `catalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acessorios_consumiveis_epis`
--

DROP TABLE IF EXISTS `acessorios_consumiveis_epis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acessorios_consumiveis_epis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acessorios_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_completa_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao_completa_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acessorios_consumiveis_epis_acessorios_categoria_id_foreign` (`acessorios_categoria_id`),
  CONSTRAINT `acessorios_consumiveis_epis_acessorios_categoria_id_foreign` FOREIGN KEY (`acessorios_categoria_id`) REFERENCES `acessorios_consumiveis_epis_categorias` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acessorios_consumiveis_epis`
--

LOCK TABLES `acessorios_consumiveis_epis` WRITE;
/*!40000 ALTER TABLE `acessorios_consumiveis_epis` DISABLE KEYS */;
/*!40000 ALTER TABLE `acessorios_consumiveis_epis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acessorios_consumiveis_epis_imagens`
--

DROP TABLE IF EXISTS `acessorios_consumiveis_epis_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acessorios_consumiveis_epis_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acessorios_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acessorios_consumiveis_epis_imagens_acessorios_id_foreign` (`acessorios_id`),
  CONSTRAINT `acessorios_consumiveis_epis_imagens_acessorios_id_foreign` FOREIGN KEY (`acessorios_id`) REFERENCES `acessorios_consumiveis_epis` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acessorios_consumiveis_epis_imagens`
--

LOCK TABLES `acessorios_consumiveis_epis_imagens` WRITE;
/*!40000 ALTER TABLE `acessorios_consumiveis_epis_imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `acessorios_consumiveis_epis_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acessorios_consumiveis_epis_destaques`
--

DROP TABLE IF EXISTS `acessorios_consumiveis_epis_destaques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acessorios_consumiveis_epis_destaques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acessorios_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acessorios_consumiveis_epis_destaques_acessorios_id_foreign` (`acessorios_id`),
  CONSTRAINT `acessorios_consumiveis_epis_destaques_acessorios_id_foreign` FOREIGN KEY (`acessorios_id`) REFERENCES `acessorios_consumiveis_epis` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acessorios_consumiveis_epis_destaques`
--

LOCK TABLES `acessorios_consumiveis_epis_destaques` WRITE;
/*!40000 ALTER TABLE `acessorios_consumiveis_epis_destaques` DISABLE KEYS */;
/*!40000 ALTER TABLE `acessorios_consumiveis_epis_destaques` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-26 13:36:30
