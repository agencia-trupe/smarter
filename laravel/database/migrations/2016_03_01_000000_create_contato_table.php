<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->text('endereco_pt');
            $table->text('endereco_en');
            $table->string('telefone');
            $table->text('google_maps');
            $table->string('facebook');
            $table->string('facebook_label');
            $table->string('twitter');
            $table->string('twitter_label');
            $table->string('instagram');
            $table->string('instagram_label');
            $table->string('linkedin');
            $table->string('linkedin_label');
            $table->string('youtube');
            $table->string('youtube_label');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
