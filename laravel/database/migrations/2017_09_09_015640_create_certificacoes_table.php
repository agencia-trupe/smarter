<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificacoesTable extends Migration
{
    public function up()
    {
        Schema::create('certificacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('imagem');
            $table->string('imagem_cinza');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('certificacoes');
    }
}
