<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('background');
            $table->string('background_mobile');
            $table->string('imagem');
            $table->string('icone');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
