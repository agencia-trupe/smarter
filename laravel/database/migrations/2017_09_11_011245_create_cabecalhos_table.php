<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabecalhosTable extends Migration
{
    public function up()
    {
        Schema::create('cabecalhos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->string('tutoriais');
            $table->string('suporte');
            $table->string('onde_comprar');
            $table->string('consultoria');
            $table->string('contato');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cabecalhos');
    }
}
