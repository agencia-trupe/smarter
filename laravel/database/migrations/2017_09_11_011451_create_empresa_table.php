<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_1_pt');
            $table->text('texto_1_en');
            $table->string('imagem');
            $table->string('mapa');
            $table->text('texto_2_pt');
            $table->text('texto_2_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
