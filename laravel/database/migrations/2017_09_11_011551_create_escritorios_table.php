<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscritoriosTable extends Migration
{
    public function up()
    {
        Schema::create('escritorios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('imagem');
            $table->text('endereco');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('escritorios');
    }
}
