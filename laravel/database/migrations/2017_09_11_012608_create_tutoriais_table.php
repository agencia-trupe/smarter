<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutoriaisTable extends Migration
{
    public function up()
    {
        Schema::create('tutoriais_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('tutoriais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutoriais_categoria_id')->unsigned()->nullable();
            $table->string('slug');
            $table->string('data');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('chamada_pt');
            $table->text('chamada_en');
            $table->string('capa');
            $table->string('thumb');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->foreign('tutoriais_categoria_id')->references('id')->on('tutoriais_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tutoriais');
        Schema::drop('tutoriais_categorias');
    }
}
