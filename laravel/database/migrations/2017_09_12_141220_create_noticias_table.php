<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration
{
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('data');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('chamada_pt');
            $table->text('chamada_en');
            $table->string('capa');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('noticias');
    }
}
