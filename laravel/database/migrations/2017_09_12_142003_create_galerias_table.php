<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriasTable extends Migration
{
    public function up()
    {
        Schema::create('galerias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('capa');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->timestamps();
        });

        Schema::create('galerias_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('galeria_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->text('legenda_pt');
            $table->text('legenda_en');
            $table->timestamps();
            $table->foreign('galeria_id')->references('id')->on('galerias')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('galerias_imagens');
        Schema::drop('galerias');
    }
}
