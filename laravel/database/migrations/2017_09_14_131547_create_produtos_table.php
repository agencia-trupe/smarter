<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_linhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_linha_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('categoria');
            $table->string('capa');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('subtitulo_pt');
            $table->string('subtitulo_en');
            $table->text('chamada_pt');
            $table->text('chamada_en');
            $table->text('tabela_pt');
            $table->text('tabela_en');
            $table->string('manual');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->foreign('produtos_linha_id')->references('id')->on('produtos_linhas')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('produtos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('produtos_imagens');
        Schema::drop('produtos');
        Schema::drop('produtos_linhas');
    }
}
