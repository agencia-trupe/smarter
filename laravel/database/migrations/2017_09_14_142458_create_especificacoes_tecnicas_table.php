<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecificacoesTecnicasTable extends Migration
{
    public function up()
    {
        Schema::create('especificacoes_tecnicas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('valor_pt');
            $table->text('valor_en');
            $table->timestamps();
            $table->integer('produto_id')->unsigned();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('especificacoes_tecnicas');
    }
}
