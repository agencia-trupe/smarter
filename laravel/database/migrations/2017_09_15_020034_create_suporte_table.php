<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuporteTable extends Migration
{
    public function up()
    {
        Schema::create('suporte', function (Blueprint $table) {
            $table->increments('id');
            $table->text('orientacoes_tecnicas_abertura_pt');
            $table->text('orientacoes_tecnicas_abertura_en');
            $table->string('orientacoes_tecnicas_capa');
            $table->text('garantia_de_produtos_abertura_pt');
            $table->text('garantia_de_produtos_abertura_en');
            $table->string('garantia_de_produtos_capa');
            $table->text('manuais_de_produtos_abertura_pt');
            $table->text('manuais_de_produtos_abertura_en');
            $table->string('manuais_de_produtos_capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('suporte');
    }
}
