<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarantiaDeProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('garantia_de_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_abertura_pt');
            $table->text('texto_abertura_en');
            $table->string('imagem_abertura');
            $table->string('telefone');
            $table->string('e_mail');
            $table->string('whatsapp');
            $table->text('texto_saiba_mais_pt');
            $table->text('texto_saiba_mais_en');
            $table->string('imagem_saiba_mais');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('garantia_de_produtos');
    }
}
