<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitacoesGarantiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacoes_garantias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('empresa');
            $table->string('cpf_cnpj');
            $table->string('cidade');
            $table->string('estado');
            $table->string('telefone');
            $table->string('email');
            $table->string('produto');
            $table->string('nf');
            $table->text('problema');
            $table->string('arquivo');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitacoes_garantias');
    }
}
