<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoscapaTable extends Migration
{
    public function up()
    {
        Schema::create('eventoscapa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('eventoscapa');
    }
}
