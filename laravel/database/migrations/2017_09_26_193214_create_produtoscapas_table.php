<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoscapasTable extends Migration
{
    public function up()
    {
        Schema::create('produtoscapas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('soldagem_eletrodo');
            $table->string('soldagem_eletrodo_ativo');
            $table->string('soldagem_tig');
            $table->string('soldagem_tig_ativo');
            $table->string('soldagem_migmag');
            $table->string('soldagem_migmag_ativo');
            $table->string('corte_plasma');
            $table->string('corte_plasma_ativo');
            $table->string('acessorios');
            $table->string('acessorios_ativo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtoscapas');
    }
}
