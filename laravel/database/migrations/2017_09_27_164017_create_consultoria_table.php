<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultoriaTable extends Migration
{
    public function up()
    {
        Schema::create('consultoria', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_1_pt');
            $table->text('texto_1_en');
            $table->string('imagem_consultoria');
            $table->string('imagem_consultoria_mobile');
            $table->text('texto_2_pt');
            $table->text('texto_2_en');
            $table->string('imagem_contato');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('consultoria');
    }
}
