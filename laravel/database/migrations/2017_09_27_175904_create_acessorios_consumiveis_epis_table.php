<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcessoriosConsumiveisEpisTable extends Migration
{
    public function up()
    {
        Schema::create('acessorios_consumiveis_epis_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('capa');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('acessorios_consumiveis_epis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('acessorios_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('slug');
            $table->string('imagem');
            $table->string('codigo');
            $table->string('modelo');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->text('descricao_completa_pt');
            $table->text('descricao_completa_en');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->foreign('acessorios_categoria_id')->references('id')->on('acessorios_consumiveis_epis_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('acessorios_consumiveis_epis_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('acessorios_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('acessorios_id')->references('id')->on('acessorios_consumiveis_epis')->onDelete('cascade');
        });

        Schema::create('acessorios_consumiveis_epis_destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('acessorios_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->timestamps();
            $table->foreign('acessorios_id')->references('id')->on('acessorios_consumiveis_epis')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('acessorios_consumiveis_epis_destaques');
        Schema::drop('acessorios_consumiveis_epis_imagens');
        Schema::drop('acessorios_consumiveis_epis');
        Schema::drop('acessorios_consumiveis_epis_categorias');
    }
}
