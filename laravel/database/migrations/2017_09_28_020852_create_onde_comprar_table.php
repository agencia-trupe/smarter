<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOndeComprarTable extends Migration
{
    public function up()
    {
        Schema::create('onde_comprar', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->string('imagem_televendas');
            $table->string('imagem_distribuidores');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('onde_comprar');
    }
}
