<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommercesTable extends Migration
{
    public function up()
    {
        Schema::create('ecommerces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('site');
            $table->string('marca');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ecommerces');
    }
}
