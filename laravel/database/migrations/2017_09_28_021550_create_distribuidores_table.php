<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistribuidoresTable extends Migration
{
    public function up()
    {
        Schema::create('distribuidores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('estado');
            $table->string('cidade');
            $table->string('empresa');
            $table->string('endereco');
            $table->string('cep');
            $table->string('telefone');
            $table->string('e_mail');
            $table->string('site');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('distribuidores');
    }
}
