<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->text('novidades_pt');
            $table->text('novidades_en');
            $table->text('galeria_pt');
            $table->text('galeria_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('news');
    }
}
