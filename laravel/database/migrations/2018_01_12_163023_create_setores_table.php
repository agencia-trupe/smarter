<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetoresTable extends Migration
{
    public function up()
    {
        Schema::create('setores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gestao_comercial');
            $table->string('financeiro');
            $table->string('apoio_administrativo_e_comercial');
            $table->string('assistencia_tecnica_e_pos_venda');
            $table->string('suporte_tecnico_de_soldagem_e_inspecao');
            $table->string('marketing');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('setores');
    }
}
