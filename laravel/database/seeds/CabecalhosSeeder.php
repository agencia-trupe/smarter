<?php

use Illuminate\Database\Seeder;

class CabecalhosSeeder extends Seeder
{
    public function run()
    {
        DB::table('cabecalhos')->insert([
            'empresa' => '',
            'tutoriais' => '',
            'suporte' => '',
            'contato' => '',
        ]);
    }
}
