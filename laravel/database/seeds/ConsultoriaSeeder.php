<?php

use Illuminate\Database\Seeder;

class ConsultoriaSeeder extends Seeder
{
    public function run()
    {
        DB::table('consultoria')->insert([
            'texto_1_pt' => '',
            'texto_1_en' => '',
            'imagem_consultoria' => '',
            'texto_2_pt' => '',
            'texto_2_en' => '',
            'imagem_contato' => '',
        ]);
    }
}
