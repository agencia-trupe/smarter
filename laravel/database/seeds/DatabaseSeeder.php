<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(SetoresSeeder::class);
		$this->call(NewsSeeder::class);
		$this->call(CatalogoSeeder::class);
		$this->call(OndeComprarSeeder::class);
		$this->call(ConsultoriaSeeder::class);
		$this->call(ProdutoscapasSeeder::class);
		$this->call(EventoscapaSeeder::class);
		$this->call(GarantiaDeProdutosSeeder::class);
		$this->call(SuporteSeeder::class);
		$this->call(EmpresaSeeder::class);
		$this->call(CabecalhosSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
