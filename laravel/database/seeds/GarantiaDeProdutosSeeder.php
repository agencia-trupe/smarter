<?php

use Illuminate\Database\Seeder;

class GarantiaDeProdutosSeeder extends Seeder
{
    public function run()
    {
        DB::table('garantia_de_produtos')->insert([
            'texto_abertura_pt' => '',
            'texto_abertura_en' => '',
            'imagem_abertura' => '',
            'telefone' => '',
            'e_mail' => '',
            'whatsapp' => '',
            'texto_saiba_mais_pt' => '',
            'texto_saiba_mais_en' => '',
            'imagem_saiba_mais' => '',
        ]);
    }
}
