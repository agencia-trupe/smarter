<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    public function run()
    {
        DB::table('news')->insert([
            'novidades_pt' => '',
            'novidades_en' => '',
            'galeria_pt' => '',
            'galeria_en' => '',
        ]);
    }
}
