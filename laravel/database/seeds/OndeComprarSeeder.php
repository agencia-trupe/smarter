<?php

use Illuminate\Database\Seeder;

class OndeComprarSeeder extends Seeder
{
    public function run()
    {
        DB::table('onde_comprar')->insert([
            'texto_pt' => '',
            'texto_en' => '',
            'imagem_televendas' => '',
            'imagem_distribuidores' => '',
        ]);
    }
}
