<?php

use Illuminate\Database\Seeder;

class ProdutoscapasSeeder extends Seeder
{
    public function run()
    {
        DB::table('produtoscapas')->insert([
            'soldagem_eletrodo' => '',
            'soldagem_eletrodo_ativo' => '',
            'soldagem_tig' => '',
            'soldagem_tig_ativo' => '',
            'soldagem_migmag' => '',
            'soldagem_migmag_ativo' => '',
            'acessorios' => '',
            'acessorios_ativo' => '',
        ]);
    }
}
