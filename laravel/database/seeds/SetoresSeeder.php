<?php

use Illuminate\Database\Seeder;

class SetoresSeeder extends Seeder
{
    public function run()
    {
        DB::table('setores')->insert([
            'gestao_comercial'                       => 'thiago.gerencia@smarterbrasil.com.br',
            'financeiro'                             => 'financeiro@smarterbrasil.com.br',
            'apoio_administrativo_e_comercial'       => 'comercial@smarterbrasil.com.br, adm@smarterbrasil.com.br',
            'assistencia_tecnica_e_pos_venda'        => 'suporte@smarterbrasil.com.br',
            'suporte_tecnico_de_soldagem_e_inspecao' => 'inspesolda@smarterbrasil.com.br',
            'marketing'                              => 'mkt@smarterbrasil.com.br',
        ]);
    }
}
