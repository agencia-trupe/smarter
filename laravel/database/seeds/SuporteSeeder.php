<?php

use Illuminate\Database\Seeder;

class SuporteSeeder extends Seeder
{
    public function run()
    {
        DB::table('suporte')->insert([
            'orientacoes_tecnicas_abertura_pt' => '',
            'orientacoes_tecnicas_abertura_en' => '',
            'orientacoes_tecnicas_capa' => '',
            'garantia_de_produtos_abertura_pt' => '',
            'garantia_de_produtos_abertura_en' => '',
            'garantia_de_produtos_capa' => '',
            'manuais_de_produtos_abertura_pt' => '',
            'manuais_de_produtos_abertura_en' => '',
            'manuais_de_produtos_capa' => '',
        ]);
    }
}
