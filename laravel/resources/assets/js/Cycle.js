export default function Cycle() {
    $('.noticias-banners').cycle({
        slides: '>a',
        pagerTemplate: '<a href=#>{{slideNum}}</a>',
    });

    $('.galeria-cycle').cycle({
        slides: '.imagem',
        autoHeight: 'container',
        next: '#next',
        prev: '#prev',
        pager: '#pager'
    });

    $('.home .banners').cycle({
        slides: '>a',
        pager: '#pager',
        pagerTemplate: '<a href=#>{{slideNum}}</a>',
        pauseOnHover: true,
        timeout: 6000
    });

    $('.home .banners-mobile').cycle({
        slides: '>a',
    });

    $('.imagens-cycle').cycle({
        next: '#next',
        prev: '#prev',
        pager: '#pager',
        timeout: 0
    });

    $('.imagens-carousel').cycle({
        fx: 'carousel',
        slides: '>div',
        carouselFluid: true,
        allowWrap: false,
        timeout: 0
    });

    $('.imagens-cycle').on('cycle-next cycle-prev', function(e, opts) {
        $('.imagens-carousel').cycle('goto', opts.currSlide);
    });

    $('.imagens-carousel .cycle-slide').click(function(){
        var index = $('.imagens-carousel').data('cycle.API').getSlideIndex(this);
        $('.imagens-cycle, .imagens-carousel').cycle('goto', index);
    });

    $('.galeria-cycle').on('cycle-before', function(e, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
        $(outgoingSlideEl).find('p').css('display', 'none');
        setTimeout(function() {
            $(outgoingSlideEl).find('p').css('display', 'block');
        }, 500);
    });

    $('.chamadas .slick').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
}
