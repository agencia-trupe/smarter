export default function Distribuidores() {
    $('.distribuidores select[name=estado]').change(function() {
        var uf    = $(this).val(),
            base  = $('base').attr('href');

        if (uf) {
            window.location = base + '/onde-comprar/' + uf;
        } else {
            window.location = base + '/onde-comprar/';
        }
    });
};
