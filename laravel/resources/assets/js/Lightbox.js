export default function Lightbox() {
    $('.produtos-video').fancybox({
        padding: 0,
        type: 'iframe',
        width: 800,
        height: 450,
        aspectRatio: true
    });

    $('.produtos-foto').fancybox({
        padding: 0
    });
};
