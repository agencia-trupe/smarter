export default function MobileToggle() {
    var $handle = $('#mobile-toggle'),
        $nav    = $('#nav-mobile');

    $handle.on('click touchstart', function(event) {
        event.preventDefault();
        $nav.slideToggle();
        $handle.toggleClass('close');
    });

    $('#nav-desktop .nav-produtos').click(function(event) {
        event.preventDefault();

        $(this).toggleClass('active');
        $('.dropdown-produtos-desktop').slideToggle('fast');
    });

    $('#nav-mobile .nav-produtos').click(function(event) {
        event.preventDefault();

        $(this).toggleClass('active');
        $('.dropdown-produtos-mobile').slideToggle('fast');
    });
};
