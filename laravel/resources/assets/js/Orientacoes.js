export default function Orientacoes() {
    $('.o-linha-toggle').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('active')) return;

        $('.o-linha-toggle').removeClass('active');

        $('.maquinas-lista').slideUp();
        $('.maquinas-lista#' + $(this).data('linha')).slideDown();

        $(this).addClass('active');
    });

    $('.orientacoes-form-toggle').click(function(event) {
        event.preventDefault();
        $(this).toggleClass('active').next().slideToggle();
    });
};
