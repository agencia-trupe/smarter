export default function Tabs() {
    $('.tabs-nav > a:first-child, .tabs-conteudo > div:first-child').addClass('active');

    $('.tabs-nav a').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('active')) return;

        var hash = $(this).attr('href');

        $('.tabs-nav > a').removeClass('active');
        $(this).addClass('active');

        $('.tabs-conteudo > div.active').slideUp(function() {
            $('.tabs-conteudo').find(hash).slideDown().addClass('active');
            $(this).removeClass('active');
        });
    });
};
