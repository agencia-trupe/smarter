export default function VoltarAoTopo() {
    $('.handle-voltar-topo').click(function(event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });
};
