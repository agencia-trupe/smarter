export default function Filtro() {
    $('#filtro-select').on('change', function () {
        var id    = $(this).val(),
            cat   = $('#categoria-select').val(),
            base  = $('base').attr('href'),
            route = $(this).data('route');

        if (id) {
            if (cat) {
                window.location = `${base}/${route}?categoria=${cat}&filtro=${id}`;
            } else {
                window.location = `${base}/${route}?filtro=${id}`;
            }
        } else {
            if (cat) {
                window.location = `${base}/${route}?categoria=${cat}`;
            } else {
                window.location = `${base}/${route}`;
            }
        }
    });

    $('#categoria-select').on('change', function () {
        var id     = $(this).val(),
            filtro = $('#filtro-select').val(),
            base   = $('base').attr('href'),
            route  = $(this).data('route');

        if (id) {
            if (filtro) {
                window.location = `${base}/${route}?categoria=${id}&filtro=${filtro}`;
            } else {
                window.location = `${base}/${route}?categoria=${id}`;
            }
        } else {
            if (filtro) {
                window.location = `${base}/${route}?filtro=${filtro}`;
            } else {
                window.location = `${base}/${route}`;
            }
        }
    });
};
