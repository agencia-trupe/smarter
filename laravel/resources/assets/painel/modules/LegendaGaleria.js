export default function LegendaGaleria() {
    var enviaAlteracao = function() {
        $.ajax({
            type: "POST",
            url: $('#form-modal-foto').attr('action'),
            data: {
                legenda_pt: $('#form-modal-foto #legenda_pt').val(),
                legenda_en: $('#form-modal-foto #legenda_en').val(),
                _method: 'PATCH'
            },
            success: function(data) {
                $.growl.notice({ title: "", message: data.success });
                bootbox.hideAll();
            },
            error: function(data) {
                $.growl.error({ title: "", message: data.responseText });
            },
            dataType: 'json'
        });
    };

    $('body').on('submit', '#form-modal-foto', function(e) {
        e.preventDefault();
        enviaAlteracao();
    });

    $('body').on('click', '.imagem-edit-legenda', function(e) {
        e.preventDefault();

        var imagemRota = $(this).first().data('href'),
            imagemArquivo = $(this).first().data('path');

        $.getJSON(imagemRota, function(data) {
            bootbox.dialog({
                title: 'Editar Imagem',
                onEscape: function() {},
                message: '<div class="row">' +
                    '<div class="col-md-2">' +
                    '<img src="' + imagemArquivo + '/' + data.imagem +'" style="width:100%;height:auto;">' +
                    '</div>' +
                    '<div class="form-group col-md-10">' +
                    '<form id="form-modal-foto" action="' + imagemRota + '">' +
                    '<label for="legenda_pt">Legenda PT</label>' +
                    '<input class="form-control" name="legenda_pt" type="text" value="' + (data.legenda_pt || '') + '" id="legenda_pt"><br>' +
                    '<label for="legenda_en">Legenda EN</label>' +
                    '<input class="form-control" name="legenda_en" type="text" value="' + (data.legenda_en || '') + '" id="legenda_en">' +
                    '<input type="submit" style="visibility: hidden; position: absolute; height: 0px; width: 0px; border: none; padding: 0px;" hidefocus="true" tabindex="-1">' +
                    '</form></div></div>',
                buttons: {
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-default btn-sm'
                    },
                    main: {
                        label: '<span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Alterar',
                        className: 'btn-success btn-sm',
                        callback: function() {
                            enviaAlteracao();
                        }
                    },
                }
            }).bind('shown.bs.modal', function(){
                $(this).find("input").first().focus();
            });
        });
    });
};
