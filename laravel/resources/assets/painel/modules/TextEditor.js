const config = {
    padrao: {
        toolbar: [['Bold', 'Italic'], ['InjectImage'], ['Link', 'Unlink'], ['JustifyLeft', 'JustifyCenter', 'JustifyBlock']]
    },

    clean: {
        toolbar: [['JustifyLeft', 'JustifyCenter', 'JustifyBlock']],
        removePlugins: 'elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR
    },

    tutoriais: {
        toolbar: [['Bold', 'Italic'], ['InjectImage'], ['Link', 'Unlink'], ['JustifyLeft', 'JustifyCenter', 'JustifyBlock']]
    },

    eventos: {
        toolbar: [['Bold', 'Italic'], ['JustifyLeft', 'JustifyCenter', 'JustifyBlock']],
        enterMode: CKEDITOR.ENTER_BR
    },

    tabela: {
        colorButton_enableMore: 'true',
        toolbar: [['Table'], ['TextColor']],
    }
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        $('base').attr('href') + '/assets/ckeditor.css',
        CKEDITOR.config.contentsCss
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal('injectimage', $('base').attr('href') + '/assets/injectimage/plugin.js');
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage,colorbutton,colordialog,justify';

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
};
