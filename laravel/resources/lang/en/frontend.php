<?php

return [

    'nav' => [
        'home'         => 'Home',
        'empresa'      => 'Smarter in the Word',
        'produtos'     => 'Products',
        'tutoriais'    => 'Tutorials',
        'suporte'      => 'Suport',
        'onde-comprar' => 'Where to Find',
        'consultoria'  => 'Consulting',
        'news'         => 'News',
        'contato'      => 'Contact',

        'soldagem-eletrodo'           => 'Welding Electrode',
        'soldagem-tig'                => 'Welding TIG',
        'soldagem-mig-mag'            => 'Welding MIG/MAG',
        'corte-plasma'                => 'Plasma Cutting',
        'acessorios-consumiveis-epis' => 'Accessories, Consumables & EPI\'s',

        'catalogo' => 'Download the<br><strong>Smarter</strong><br>product catalog!'
    ],

    'header' => [
        'pesquisar'     => 'searching',
        'redes-sociais' => 'social medias',
        'idioma'        => 'language',
    ],

    'footer' => [
        'fone' => 'Phone',
    ],

    'newsletter' => [
        'titulo'         => 'Sign up for our <strong>Newsletter</strong>',
        'chamada'        => 'Stay tuned with our News',
        'cadastrar'      => 'REGISTER',
        'nome-required'  => 'fill in your name',
        'email-required' => 'enter an email address',
        'email-email'    => 'enter a valid email address',
        'email-unique'   => 'email entered is already registered',
        'sucesso'        => 'registration successfully complete!',
    ],

    'produtos' => [
        'titulo-soldagem-eletrodo'           => '<strong>welding</strong> electrode',
        'titulo-soldagem-tig'                => '<strong>welding</strong> tig',
        'titulo-soldagem-mig-mag'            => '<strong>welding</strong> mig/mag',
        'titulo-corte-plasma'                => '<strong>plasma</strong> cutting',
        'titulo-acessorios-consumiveis-epis' => '<strong>accessories,</strong>consumables<br>& epi\'s',

        'saiba-mais'              => 'know more',
        'recursos-especiais'      => 'Special Features',
        'acessorios'              => 'Accessories',
        'inclusos'                => 'items <strong>included</strong>',
        'opcionais'               => '<strong>optional</strong> items',
        'destaques'               => 'Highlights',
        'especificacoes'          => 'Specifications',
        'especificacoes-tecnicas' => 'Technical specifications',
        'produto'                 => 'Product',
        'fotos-videos'            => 'Photos & Videos',

        'resultado'               => 'Search result:',
        'nenhum'                  => 'No results found.',
        'ver-mais'                => 'View more products',
        'codigo'                  => 'Code',
        'modelo'                  => 'Model',
        'descricao'               => 'Description'
    ],

    'tutoriais' => [
        'categorias' => 'Categories',
        'acesse'     => 'Access',
        'anterior'   => 'Previous',
        'proximo'    => 'Next',
        'voltar'     => 'Back to the list of tutorials',
    ],

    'suporte' => [
        'orientacoes-tecnicas'          => 'Technical Guidelines',
        'garantia-de-produtos'          => 'Product Warranty',
        'manuais-de-produtos'           => 'Product Manuals',
        'assistencia-tecnica'           => 'Technical Assistance',
        'assistencia-texto'             => 'Please contact us to request technical assistance for your Smarter product.',
        'solicitacao-de-garantia'       => 'Warranty Request',
        'solicitacao-de-garantia-texto' => 'Fill out the information to request a warranty procedure for your Smarter product.',
        'orientacoes-tecnicas-texto'    => 'Select your Smarter product model to request information and guidance',
        'numero-nf'                     => 'NF number',
        'descricao-problema'            => 'Problem description',
        'solicitacao-enviada'           => 'Request sent successfully!',
        'manuais-de-produtos-texto'     => 'Select the template to access the complete product file.',
        'baixar-manual'                 => 'Download Manual',
        'baixar'                        => 'Download',
        'copia-nf'                      => 'Copy of invoice',
        'duvida'                        => 'Your question has not been resolved?',
        'contato'                       => 'Contact us',
        'linha'                         => 'Line',
        'maquina'                       => 'Machine',
        'solicitacao'                   => 'Enter your question or request here',
    ],

    'news' => [
        'novidades'         => 'News',
        'ver-noticias'      => 'See News',
        'mais-noticias'     => 'More News',
        'acessar'           => 'Access',
        'ler-mais'          => 'Read more',
        'proximo'           => 'Older posts',
        'anterior'          => 'Most recent posts',
        'noticia-anterior'  => 'Previous',
        'proxima-noticia'   => 'Next',
        'noticias-voltar'   => 'Back to the list of News',
        'data'              => 'Date',
        'calendario'        => 'Events calendar',
        'lista-eventos'     => 'List of events',
        'nenhum-evento'     => 'No event found in the selected month.',
        'galeria'           => 'Gallery',
        'ver-galeria'       => 'View Gallery',
        'galeria-voltar'    => 'Back to Gallery',
    ],

    'contato' => [
        'titulo'         => 'Contact us',
        'frase'          => 'Fill out the form below to contact <strong>Smarter</strong>',
        'nome'           => 'Name',
        'telefone'       => 'Phone',
        'celular'        => 'Cell Phone',
        'empresa'        => 'Company',
        'endereco'       => 'Adress',
        'cep'            => 'CEP',
        'cidade'         => 'City',
        'estado'         => 'Estate',
        'setor'          => 'Sector',
        'mensagem'       => 'Message',
        'anexar'         => 'Attach file',
        'enviar'         => 'Send',
        'smarter-brasil' => 'Smarter Brazil',
        'enviado'        => 'Message sent successfully!',
        'setores'        => [
            'Gestão Comercial'                       => 'Commercial management',
            'Financeiro'                             => 'Financial',
            'Apoio Administrativo e Comercial'       => 'Administrative and Commercial Support',
            'Assistência Técnica e Pós-Venda'        => 'Technical Assistance and After Sales',
            'Suporte Técnico de Soldagem e Inspeção' => 'Welding and Inspection Technical Support',
            'Marketing'                              => 'Marketing'
        ]
    ],

    'consultoria' => [
        'titulo' => 'Smarter iTech Consulting',
        'frase'  => 'Fill out the form and a <strong>Smarter</strong> consultant will contact you.'
    ],

    'onde-comprar' => [
        'distribuidores'           => 'Distributors',
        'ecommerce'                => 'E-commerce / Telesales',
        'acesse'                   => 'Access the site',
        'distribuidores-regionais' => 'Regional Distributors',
        'estados'                  => 'Click the Brazilian states to find the authorized <strong>Smarter iTech</strong> distributor nearest you.',
        'nenhum'                   => 'No distributors found in this state.',
        'estado'                   => 'Select a state'
    ],
];
