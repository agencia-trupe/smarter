<?php

return [

    'nav' => [
        'home'         => 'Home',
        'empresa'      => 'Smarter no Mundo',
        'produtos'     => 'Produtos',
        'tutoriais'    => 'Tutoriais',
        'suporte'      => 'Suporte',
        'onde-comprar' => 'Onde Comprar',
        'consultoria'  => 'Consultoria',
        'news'         => 'News',
        'contato'      => 'Contato',

        'soldagem-eletrodo'           => 'Soldagem Eletrodo',
        'soldagem-tig'                => 'Soldagem TIG',
        'soldagem-mig-mag'            => 'Soldagem MIG/MAG',
        'corte-plasma'                => 'Corte Plasma',
        'acessorios-consumiveis-epis' => 'Acessórios, Consumíveis & EPI\'s',

        'catalogo' => 'Faça download<br>do catálogo de<br>produtos <strong>Smarter</strong>!'
    ],

    'header' => [
        'pesquisar'     => 'pesquisar',
        'redes-sociais' => 'redes sociais',
        'idioma'        => 'idioma',
    ],

    'footer' => [
        'fone' => 'Fone',
    ],

    'newsletter' => [
        'titulo'         => 'Assine nossa <strong>Newsletter</strong>',
        'chamada'        => 'Fique por dentro das nossas novidades',
        'cadastrar'      => 'CADASTRAR',
        'nome-required'  => 'preencha seu nome',
        'email-required' => 'insira um endereço de e-mail',
        'email-email'    => 'insira um endereço de e-mail válido',
        'email-unique'   => 'o e-mail inserido já está cadastrado',
        'sucesso'        => 'cadastro efetuado com sucesso!',
    ],

    'produtos' => [
        'titulo-soldagem-eletrodo'           => '<strong>soldagem</strong> eletrodo',
        'titulo-soldagem-tig'                => '<strong>soldagem</strong> tig',
        'titulo-soldagem-mig-mag'            => '<strong>soldagem</strong> mig/mag',
        'titulo-corte-plasma'                => '<strong>corte</strong> plasma',
        'titulo-acessorios-consumiveis-epis' => '<strong>linha de</strong> acessórios,<br>consumíveis<br>e epi\'s',

        'saiba-mais'              => 'saiba mais',
        'recursos-especiais'      => 'Recursos Especiais',
        'acessorios'              => 'Acessórios',
        'inclusos'                => 'itens <strong>inclusos</strong>',
        'opcionais'               => 'itens <strong>opcionais</strong>',
        'destaques'               => 'Destaques',
        'especificacoes'          => 'Especificações',
        'especificacoes-tecnicas' => 'Especificações Técnicas',
        'produto'                 => 'Produto',
        'fotos-videos'            => 'Fotos & Vídeos',

        'resultado'               => 'Resultado da busca:',
        'nenhum'                  => 'Nenhum resultado encontrado.',
        'ver-mais'                => 'Ver mais produtos',
        'codigo'                  => 'Código',
        'modelo'                  => 'Modelo',
        'descricao'               => 'Descrição'
    ],

    'tutoriais' => [
        'categorias' => 'Categorias',
        'acesse'     => 'Acesse',
        'anterior'   => 'Anterior',
        'proximo'    => 'Próximo',
        'voltar'     => 'Voltar para a lista de tutoriais',
    ],

    'suporte' => [
        'orientacoes-tecnicas'          => 'Orientações Técnicas',
        'garantia-de-produtos'          => 'Garantia de Produtos',
        'manuais-de-produtos'           => 'Manuais de Produtos',
        'assistencia-tecnica'           => 'Assistência Técnica',
        'assistencia-texto'             => 'Entre em contato conosco para solicitar assistência técnica para seu produto Smarter.',
        'solicitacao-de-garantia'       => 'Solicitação de Garantia',
        'solicitacao-de-garantia-texto' => 'Preencha os dados para solicitar procedimento de garantia para o seu produto Smarter.',
        'orientacoes-tecnicas-texto'    => 'Selecione o modelo de seu produto Smarter para solicitar informações e orientações',
        'numero-nf'                     => 'Número da NF',
        'descricao-problema'            => 'Descrição do problema',
        'solicitacao-enviada'           => 'Solicitação enviada com sucesso!',
        'manuais-de-produtos-texto'     => 'Selecione o modelo para acessar o arquivo completo de produto.',
        'baixar-manual'                 => 'Baixar Manual',
        'baixar'                        => 'Baixar',
        'copia-nf'                      => 'Cópia da nota fiscal',
        'duvida'                        => 'Sua dúvida não foi solucionada?',
        'contato'                       => 'Entre em contato',
        'linha'                         => 'Linha',
        'maquina'                       => 'Máquina',
        'solicitacao'                   => 'Digite aqui sua dúvida ou solicitação',
    ],

    'news' => [
        'novidades'         => 'Novidades',
        'ver-noticias'      => 'Ver notícias',
        'mais-noticias'     => 'Mais notícias',
        'acessar'           => 'Acessar',
        'ler-mais'          => 'Ler mais',
        'proximo'           => 'Postagens mais antigas',
        'anterior'          => 'Postagens mais recentes',
        'noticia-anterior'  => 'Anterior',
        'proxima-noticia'   => 'Próxima',
        'noticias-voltar'   => 'Voltar para a lista de novidades',
        'data'              => 'Data',
        'calendario'        => 'Calendário de Eventos',
        'lista-eventos'     => 'Lista de eventos',
        'nenhum-evento'     => 'Nenhum evento encontrado no mês selecionado.',
        'galeria'           => 'Galeria',
        'ver-galeria'       => 'Ver galeria',
        'galeria-voltar'    => 'Voltar para a Galeria',
    ],

    'contato' => [
        'titulo'         => 'Fale conosco',
        'frase'          => 'Preencha o formulário abaixo para entrar em contato com a <strong>Smarter</strong>',
        'nome'           => 'Nome',
        'telefone'       => 'Telefone',
        'celular'        => 'Celular',
        'empresa'        => 'Empresa',
        'endereco'       => 'Endereço',
        'cep'            => 'CEP',
        'cidade'         => 'Cidade',
        'estado'         => 'Estado',
        'setor'          => 'Setor',
        'mensagem'       => 'Mensagem',
        'anexar'         => 'Anexar arquivo',
        'enviar'         => 'Enviar',
        'smarter-brasil' => 'Smarter Brasil',
        'enviado'        => 'Mensagem enviada com sucesso!',
        'setores'        => [
            'Gestão Comercial'                       => 'Gestão Comercial',
            'Financeiro'                             => 'Financeiro',
            'Apoio Administrativo e Comercial'       => 'Apoio Administrativo e Comercial',
            'Assistência Técnica e Pós-Venda'        => 'Assistência Técnica e Pós-Venda',
            'Suporte Técnico de Soldagem e Inspeção' => 'Suporte Técnico de Soldagem e Inspeção',
            'Marketing'                              => 'Marketing'
        ]
    ],

    'consultoria' => [
        'titulo' => 'Consultoria Smarter iTech',
        'frase'  => 'Preencha o formulário e um consultor <strong>Smarter</strong> entrará em contato.'
    ],

    'onde-comprar' => [
        'distribuidores'           => 'Distribuidores',
        'ecommerce'                => 'E-commerce / Televendas',
        'acesse'                   => 'Acesse o site',
        'distribuidores-regionais' => 'Distribuidores Regionais',
        'estados'                  => 'Clique nos estados brasileiros para encontrar o distribuidor autorizado <strong>Smarter iTech</strong> mais próximo de você.',
        'nenhum'                   => 'Nenhum distribuidor encontrado neste estado.',
        'estado'                   => 'Selecione um estado'
    ],
];
