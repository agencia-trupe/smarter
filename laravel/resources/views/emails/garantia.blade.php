<!DOCTYPE html>
<html>
<head>
    <title>[SOLICITAÇÃO DE GARANTIA] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
@if($empresa)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
@endif
@if($cpf_cnpj)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CPF / CNPJ:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cpf_cnpj }}</span><br>
@endif
@if($cidade)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cidade:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cidade }}</span><br>
@endif
@if($estado)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Estado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $estado }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Produto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $produto }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Número da NF:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nf }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Descrição do problema:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $problema }}</span><br>
@if($arquivo)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Arquivo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'><a href="{{ url('arquivos/'.$arquivo) }}">{{ $arquivo }}</a></span><br>
@endif
</body>
</html>
