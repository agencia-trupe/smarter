<!DOCTYPE html>
<html>
<head>
    <title>[SOLICITAÇÃO DE ORIENTAÇÃO] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
@if($cidade_estado)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cidade / Estado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cidade_estado }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Produto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $produto }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Dúvida ou solicitação:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $solicitacao }}</span><br>
@if($arquivo)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Arquivo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'><a href="{{ url('arquivos/'.$arquivo) }}">{{ $arquivo }}</a></span><br>
@endif
</body>
</html>
