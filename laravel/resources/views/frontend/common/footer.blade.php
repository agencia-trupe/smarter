    <footer @if(Route::currentRouteName() == 'produtos') class="claro" @endif>
        <div class="center">
            <div class="informacoes">
                <img src="{{ asset('assets/img/layout/footer-marca.png') }}" alt="">

                <h4>SMARTER BRASIL COMERCIAL DE MÁQUINAS LTDA</h4>
                <p>
                    {!! $contato->{'endereco_'.app()->getLocale()} !!}
                    <br>
                    <strong>{{ trans('frontend.footer.fone') }}: {{ $contato->telefone }}</strong><br>
                    <strong>E-mail: <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></strong>
                </p>

                <div class="certificacoes">
                    @if(Route::currentRouteName() == 'produtos')
                    <img src="{{ asset('assets/img/layout/certificados-cinza.png') }}" alt="">
                    @else
                    <img src="{{ asset('assets/img/layout/certificados-branco.png') }}" alt="">
                    @endif
                </div>
            </div>

            <div class="links">
                <a href="#">{{ trans('frontend.nav.home') }}</a>
                <a href="{{ route('empresa') }}">{{ trans('frontend.nav.empresa') }}</a>
                <a href="{{ route('produtos') }}">{{ trans('frontend.nav.produtos') }}</a>
                <a href="{{ route('tutoriais') }}">{{ trans('frontend.nav.tutoriais') }}</a>
                <a href="{{ route('suporte') }}">{{ trans('frontend.nav.suporte') }}</a>
                <a href="{{ route('onde-comprar') }}">{{ trans('frontend.nav.onde-comprar') }}</a>
                <a href="{{ route('consultoria') }}">{{ trans('frontend.nav.consultoria') }}</a>
                <a href="{{ route('news') }}">{{ trans('frontend.nav.news') }}</a>
                <a href="{{ route('contato') }}">{{ trans('frontend.nav.contato') }}</a>
            </div>

            <div class="social">
                @foreach(['facebook', 'instagram', 'twitter', 'youtube', 'linkedin'] as $s)
                @if($contato->{$s})
                <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">
                    {{ $contato->{$s.'_label'} }}
                </a>
                @endif
                @endforeach

                @if($catalogo->arquivo)
                <div class="catalogo">
                    <a href="{{ url('catalogos/'.$catalogo->arquivo) }}" target="_blank">
                        <img src="{{ asset('assets/img/catalogo/'.$catalogo->imagem) }}" alt="">
                    </a>
                </div>
                @endif
            </div>
        </div>

        <a href="#" class="handle-voltar-topo">
            VOLTAR AO TOPO
        </a>
    </footer>
