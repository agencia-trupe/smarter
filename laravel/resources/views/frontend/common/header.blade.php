<header>
    <div class="center">
        <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>

        <div class="top-bar">
            <form action="{{ route('busca') }}" method="GET" class="pesquisar">
                <span>{{ trans('frontend.header.pesquisar') }}</span>
                <input type="text" name="termo" value="{{ Request::get('termo') }}" required>
                <input type="submit">
            </form>

            <div class="sociais">
                <span>{{ trans('frontend.header.redes-sociais') }}</span>
                @foreach(['facebook', 'instagram', 'twitter', 'youtube', 'linkedin'] as $s)
                @if($contato->{$s})
                <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank"></a>
                @endif
                @endforeach
            </div>

            <div class="idioma">
                <span>{{ trans('frontend.header.idioma') }}</span>
                <a href="{{ route('lang', 'pt') }}" class="pt" title="Versão em Português">pt</a>
                <a href="{{ route('lang', 'en') }}" class="en" title="English Version">en</a>
            </div>
        </div>

        {{--
        @if($catalogo->arquivo)
        <a href="{{ url('catalogos/'.$catalogo->arquivo) }}" class="catalogo-desktop" target="_blank">
            <div class="imagem">
                <img src="{{ asset('assets/img/layout/catalogo.jpg') }}" alt="">
            </div>
            <span>{!! trans('frontend.nav.catalogo') !!}</span>
        </a>
        @endif
        --}}

        <nav id="nav-desktop">
            <a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>
                {{ trans('frontend.nav.empresa') }}
            </a>
            <a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif">
                {{ trans('frontend.nav.produtos') }}
            </a>
            <a href="{{ route('tutoriais') }}" @if(Tools::isActive('tutoriais*')) class="active" @endif>
                {{ trans('frontend.nav.tutoriais') }}
            </a>
            <a href="{{ route('suporte') }}" @if(Tools::isActive('suporte*')) class="active" @endif>
                {{ trans('frontend.nav.suporte') }}
            </a>
            <a href="{{ route('onde-comprar') }}" @if(Tools::isActive('onde-comprar')) class="active" @endif>
                {{ trans('frontend.nav.onde-comprar') }}
            </a>
            <a href="{{ route('consultoria') }}" @if(Tools::isActive('consultoria')) class="active" @endif>
                {{ trans('frontend.nav.consultoria') }}
            </a>
            <a href="{{ route('news') }}" @if(Tools::isActive('news*')) class="active" @endif>
                {{ trans('frontend.nav.news') }}
            </a>
            <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>
                {{ trans('frontend.nav.contato') }}
            </a>
        </nav>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </div>

    {{--
    <div class="dropdown-produtos-desktop">
        <div class="center">
            <a href="{{ route('produtos', 'soldagem-eletrodo') }}">{{ trans('frontend.nav.soldagem-eletrodo') }}</a>
            <a href="{{ route('produtos', 'soldagem-tig') }}">{{ trans('frontend.nav.soldagem-tig') }}</a>
            <a href="{{ route('produtos', 'soldagem-mig-mag') }}">{{ trans('frontend.nav.soldagem-mig-mag') }}</a>
            <a href="{{ route('produtos', 'acessorios-consumiveis-epis') }}">{{ trans('frontend.nav.acessorios-consumiveis-epis') }}</a>
        </div>
    </div>
    --}}

    <nav id="nav-mobile">
        <a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>
            {{ trans('frontend.nav.empresa') }}
        </a>
        <a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif">
            {{ trans('frontend.nav.produtos') }}
        </a>
        {{--
        <div class="dropdown-produtos-mobile">
            <a href="{{ route('produtos', 'soldagem-eletrodo') }}">{{ trans('frontend.nav.soldagem-eletrodo') }}</a>
            <a href="{{ route('produtos', 'soldagem-tig') }}">{{ trans('frontend.nav.soldagem-tig') }}</a>
            <a href="{{ route('produtos', 'soldagem-mig-mag') }}">{{ trans('frontend.nav.soldagem-mig-mag') }}</a>
            <a href="{{ route('produtos', 'acessorios-consumiveis-epis') }}">{{ trans('frontend.nav.acessorios-consumiveis-epis') }}</a>
        </div>
        --}}
        <a href="{{ route('tutoriais') }}" @if(Tools::isActive('tutoriais*')) class="active" @endif>
            {{ trans('frontend.nav.tutoriais') }}
        </a>
        <a href="{{ route('suporte') }}" @if(Tools::isActive('suporte*')) class="active" @endif>
            {{ trans('frontend.nav.suporte') }}
        </a>
        <a href="{{ route('onde-comprar') }}" @if(Tools::isActive('onde-comprar')) class="active" @endif>
            {{ trans('frontend.nav.onde-comprar') }}
        </a>
        <a href="{{ route('consultoria') }}" @if(Tools::isActive('consultoria')) class="active" @endif>
            {{ trans('frontend.nav.consultoria') }}
        </a>
        <a href="{{ route('news') }}" @if(Tools::isActive('news*')) class="active" @endif>
            {{ trans('frontend.nav.news') }}
        </a>
        <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>
            {{ trans('frontend.nav.contato') }}
        </a>

        <div class="sociais">
            <span>{{ trans('frontend.header.redes-sociais') }}</span>
            @foreach(['facebook', 'instagram', 'twitter', 'youtube', 'linkedin'] as $s)
            <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank"></a>
            @endforeach
        </div>

        <div class="idioma">
            <span>{{ trans('frontend.header.idioma') }}</span>
            <a href="{{ route('lang', 'pt') }}" class="pt" title="Versão em Português">pt</a>
            <a href="{{ route('lang', 'en') }}" class="en" title="English Version">en</a>
        </div>

        @if($catalogo->arquivo)
        <a href="{{ url('catalogos/'.$catalogo->arquivo) }}" class="catalogo-mobile" target="_blank">
            <div class="imagem">
                <img src="{{ asset('assets/img/layout/catalogo.jpg') }}" alt="">
            </div>
            <span>{!! trans('frontend.nav.catalogo') !!}</span>
        </a>
        @endif
    </nav>
</header>
