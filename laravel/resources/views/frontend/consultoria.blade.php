@extends('frontend.common.template')

@section('content')

    <div class="cabecalho-img">
        <div class="center">
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalhoImg) }}" alt="">
        </div>
    </div>

    <div class="breadcrumb-simple">
        <div class="center">
            <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.consultoria') }}</span>
        </div>
    </div>

    <div class="consultoria">
        <div class="texto">
            <div class="center">
                <h1>{{ trans('frontend.consultoria.titulo') }}</h1>
                {!! $consultoria->{'texto_1_'.app()->getLocale()}!!}
            </div>
        </div>

        <img class="imagem-consultoria" src="{{ asset('assets/img/consultoria/'.$consultoria->imagem_consultoria) }}" alt="">
        <img class="imagem-consultoria mobile" src="{{ asset('assets/img/consultoria/mobile/'.$consultoria->imagem_consultoria_mobile) }}" alt="">

        <div class="texto">
            <div class="center">
                {!! $consultoria->{'texto_2_'.app()->getLocale()}!!}
            </div>
        </div>

        <div class="imagem" style="background-image:url({{ asset('assets/img/consultoria/'.$consultoria->imagem_contato) }})">
            <div class="center">
                <span>{{ trans('frontend.nav.contato') }}</span>
            </div>
        </div>

        <div class="center">
            @if(session('success'))
            <div class="contato-enviado">
                <h3>{{ trans('frontend.contato.enviado') }}</h3>
            </div>
            @else
            <div class="texto">
                <p>{!! trans('frontend.consultoria.frase') !!}</p>
            </div>
            <form action="{{ route('consultoria.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erros">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif

                <div class="row">
                    <label>{{ trans('frontend.contato.nome') }}</label>
                    {!! Form::text('nome', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.empresa') }}</label>
                    {!! Form::text('empresa', null) !!}
                </div>
                <div class="row">
                    <label>CPF / CNPJ</label>
                    {!! Form::text('cpf_cnpj', null) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.cidade') }}</label>
                    {!! Form::text('cidade', null) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.estado') }}</label>
                    {!! Form::text('estado', null) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.telefone') }}</label>
                    {!! Form::text('telefone', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>E-mail</label>
                    {!! Form::email('email', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.anexar') }}</label>
                    <div class="anexo-wrapper">
                        {!! Form::file('arquivo', null) !!}
                    </div>
                </div>
                <div class="submit-wrapper">
                    <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                </div>
            </form>
            @endif
        </div>
    </div>

@endsection
