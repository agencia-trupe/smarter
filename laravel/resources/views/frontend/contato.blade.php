@extends('frontend.common.template')

@section('content')

    <div class="cabecalho-img">
        <div class="center">
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalhoImg) }}" alt="">
        </div>
    </div>

    <div class="contato">
        <div class="center">
            <h1>{{ trans('frontend.contato.titulo') }}</h1>
            @unless(session('success'))
            <p>{!! trans('frontend.contato.frase') !!}</p>
            @endunless

            @if(session('success'))
            <div class="contato-enviado">
                <h3>{{ trans('frontend.contato.enviado') }}</h3>
            </div>
            @else
            <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erros">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif

                <div class="row">
                    <label>
                        {{ trans('frontend.contato.nome') }}<span>*</span>
                        {!! Form::text('nome', null, ['required' => true]) !!}
                    </label>
                </div>
                <div class="row">
                    <label class="col-tel">
                        {{ trans('frontend.contato.telefone') }}<span>*</span>
                        {!! Form::text('telefone', null, ['required' => true]) !!}
                    </label>
                    <label class="col-tel">
                        {{ trans('frontend.contato.celular') }}<span>*</span>
                        {!! Form::text('celular', null, ['required' => true]) !!}
                    </label>
                </div>
                <div class="row">
                    <label>
                        E-mail<span>*</span>
                        {!! Form::email('email', null, ['required' => true]) !!}
                    </label>
                </div>
                <div class="row">
                    <label>
                        {{ trans('frontend.contato.empresa') }}
                        {!! Form::text('empresa', null) !!}
                    </label>
                </div>
                <div class="row">
                    <label>
                        {{ trans('frontend.contato.endereco') }}
                        {!! Form::text('endereco', null) !!}
                    </label>
                </div>
                <div class="row">
                    <label class="col-cep">
                        {{ trans('frontend.contato.cep') }}
                        {!! Form::text('cep', null) !!}
                    </label>
                    <label class="col-cidade">
                        {{ trans('frontend.contato.cidade') }}
                        {!! Form::text('cidade', null) !!}
                    </label>
                    <label class="col-estado">
                        {{ trans('frontend.contato.estado') }}
                        {!! Form::text('estado', null) !!}
                    </label>
                </div>
                <div class="row">
                    <label>
                        {{ trans('frontend.contato.setor') }}<span>*</span>
                        {!! Form::select('setor', trans('frontend.contato.setores'), null, ['placeholder' => 'Selecione', 'required' => true]) !!}
                    </label>
                </div>
                <div class="row">
                    <label>
                        {{ trans('frontend.contato.mensagem') }}<span>*</span>
                        {!! Form::textarea('mensagem', null, ['required' => true]) !!}
                    </label>
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.anexar') }}</label>
                    <div class="anexo-wrapper">
                        {!! Form::file('arquivo', null) !!}
                    </div>
                </div>

                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            </form>
            @endif

            <div class="informacoes">
                <div class="texto">
                    <h3>{{ trans('frontend.contato.smarter-brasil') }}</h3>
                    <a href="www.smarterbrasil.com.br">www.smarterbrasil.com.br</a>
                    <p>
                        <strong>{{ trans('frontend.contato.endereco') }}:</strong>
                        {!! $contato->{'endereco_'.app()->getLocale()} !!}
                    </p>
                    <p>
                        <strong>E-mail:</strong>
                        <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                    </p>
                    <p>
                        <strong>{{ trans('frontend.contato.telefone') }}:</strong>
                        {{ $contato->telefone }}
                    </p>
                </div>

                <div class="mapa">{!! $contato->google_maps !!}</div>
            </div>
        </div>
    </div>

@endsection
