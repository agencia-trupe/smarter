@extends('frontend.common.template')

@section('content')

    <div class="cabecalho-img">
        <div class="center">
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalhoImg) }}" alt="">
        </div>
    </div>

    <div class="breadcrumb-simple">
        <div class="center">
            <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.empresa') }}</span>
        </div>
    </div>

    <div class="empresa">
        <div class="texto-1">
            <div class="center">
                <div class="texto">
                    <h2>{{ trans('frontend.contato.smarter-brasil') }}</h2>
                    {!! $empresa->{'texto_1_'.app()->getLocale()} !!}
                </div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">
                </div>
            </div>
        </div>

        <div class="mapa">
            <div class="center">
                <img src="{{ asset('assets/img/empresa/'.$empresa->mapa) }}" alt="">
                <span>{{ trans('frontend.nav.empresa') }}</span>
            </div>
        </div>

        <div class="texto-2">
            <div class="center">
                {!! $empresa->{'texto_2_'.app()->getLocale()} !!}
            </div>
        </div>

        <div class="escritorios">
            <div class="center">
                @foreach($escritorios as $escritorio)
                <div class="escritorio @if($escritorio->titulo == 'Smarter Brasil') brasil @endif">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/escritorios/'.$escritorio->imagem) }}" alt="">
                        <span>{{ $escritorio->titulo }}</span>
                    </div>
                    <div class="endereco">{!! $escritorio->endereco !!}</div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
