@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" style="background-image:url('{{ asset('assets/img/banners/'.$banner->background) }}')">
                <div class="center">
                    <div class="texto">{!! $banner->{'texto_'.app()->getLocale()} !!}</div>
                </div>
            </a>
            @endforeach

            <div class="pager center">
                <div id="pager"></div>
            </div>
        </div>

        <div class="banners-mobile">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" style="background-image:url('{{ asset('assets/img/banners/mobile/'.$banner->background_mobile) }}')">
                <div class="center">
                    <div class="texto">{!! $banner->{'texto_'.app()->getLocale()} !!}</div>
                </div>
            </a>
            @endforeach
        </div>

        <div class="chamadas-wrapper">
            <div class="center">
                <div class="chamadas">
                    <div class="slick">
                        @foreach($chamadas as $chamada)
                        <a href="{{ $chamada->link }}">
                            <div class="hover-fx"></div>
                            <img src="{{ asset('assets/img/chamadas/'.$chamada->imagem) }}" alt="">
                            <div class="texto">
                                <h2>{{ $chamada->{'titulo_'.app()->getLocale()} }}</h2>
                                <p>{{ $chamada->{'subtitulo_'.app()->getLocale()} }}</p>
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>

                <form action="" class="newsletter" id="form-newsletter">
                    <h2>{!! trans('frontend.newsletter.titulo') !!}</h2>
                    <p>{{ trans('frontend.newsletter.chamada') }}</p>
                    <input type="text" name="newsletter_nome" id="newsletter_nome" placeholder="{{ trans('frontend.contato.nome') }}">
                    <input type="email" name="newsletter_email" id="newsletter_email" placeholder="E-mail">
                    <input type="submit" value="{{ trans('frontend.newsletter.cadastrar') }}">
                </form>
            </div>
        </div>
    </div>

@endsection
