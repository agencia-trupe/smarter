@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('news') }}">
                    {{ trans('frontend.nav.news') }}
                </a>
                /
                <strong>{{ trans('frontend.news.calendario') }}</strong>
            </span>
        </div>
    </div>

    <div class="eventos">
        <div class="center">
            <h1>{{ trans('frontend.news.calendario') }}</h1>

            <div class="navegacao">
                <a href="{{ $navegacao['anterior'] }}"><<</a>
                <span>{{ $navegacao['atual'] }}</span>
                <a href="{{ $navegacao['proximo'] }}">>></a>
            </div>

            @if(!count($listaEventos))
            <div class="nenhum">
                {{ trans('frontend.news.nenhum-evento') }}
            </div>
            @else
            <div class="calendario">
                {!! $calendario !!}
            </div>

            <div class="lista">
                <h3>{{ trans('frontend.news.lista-eventos') }}</h3>
                @foreach($listaEventos as $evento)
                <div class="evento">
                    <h2>{{ $evento->{'titulo_'.app()->getLocale()} }}</h2>
                    <p>
                        <strong>{{ trans('frontend.news.data') }}:</strong> {{ $evento->data }}<br>
                        {!! $evento->{'texto_'.app()->getLocale()} !!}
                    </p>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>

@endsection
