@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('news') }}">
                    {{ trans('frontend.nav.news') }}
                </a>
                /
                <strong>{{ trans('frontend.news.galeria') }}</strong>
            </span>
        </div>
    </div>

    <div class="galeria-index">
        <div class="center">
            <h1>{{ trans('frontend.news.galeria') }}</h1>

            <div class="thumbs">
                @foreach($galerias as $galeria)
                <a href="{{ route('news.galeria.show', $galeria->slug) }}">
                    <img src="{{ asset('assets/img/galerias/capa/'.$galeria->capa) }}" alt="">
                    <span>{{ $galeria->{'titulo_'.app()->getLocale()} }}</span>
                </a>
                @endforeach
            </div>

            {{ with(new App\Pagination\GaleriasPresenter($galerias))->render() }}
        </div>
    </div>


@endsection
