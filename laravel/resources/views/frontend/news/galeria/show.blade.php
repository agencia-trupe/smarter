@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('news') }}">
                    {{ trans('frontend.nav.news') }}
                </a>
                /
                <strong>{{ trans('frontend.news.galeria') }}</strong>
            </span>
        </div>
    </div>

    <div class="galeria-show">
        <div class="center">
            @if($galeria->imagens->count())
            <div class="galeria-cycle">
                @foreach($galeria->imagens as $imagem)
                <div class="imagem" data-cycle-pager-template="<a href='#'><img src='{{ asset('assets/img/galerias/imagens/thumbs-pequenos/'.$imagem->imagem) }}'></a>">
                    <img src="{{ asset('assets/img/galerias/imagens/'.$imagem->imagem) }}" alt="">
                    @if($imagem->{'legenda_'.app()->getLocale()})
                    <p>{{ $imagem->{'legenda_'.app()->getLocale()} }}</p>
                    @endif
                </div>
                @endforeach

                <div class="cycle-controls">
                    <div class="cycle-prev" id="prev"></div>
                    <div class="cycle-next" id="next"></div>
                </div>
            </div>

            <div class="cycle-pager" id="pager"></div>
            @endif

            <div class="texto">
                <h1>{{ $galeria->{'titulo_'.app()->getLocale()} }}</h1>
                {!! $galeria->{'texto_'.app()->getLocale()} !!}
            </div>

            <a href="{{ route('news.galeria') }}">{{ trans('frontend.news.galeria-voltar') }}</a>
        </div>
    </div>


@endsection
