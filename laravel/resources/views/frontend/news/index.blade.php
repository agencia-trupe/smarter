@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <strong>News</strong>
            </span>
        </div>
    </div>

    <div class="news">
        <div class="center">
            <div class="novidades">
                <div class="texto">
                    <h2>{{ trans('frontend.news.novidades') }}</h2>
                    <p>{{ $news->{'novidades_'.app()->getLocale()} }}</p>
                    <a href="{{ route('news.noticias') }}">{{ trans('frontend.news.ver-noticias') }}</a>
                </div>
                <div class="noticias-banners">
                    @foreach($novidades as $noticia)
                    <a href="{{ route('news.noticias.show', $noticia->slug) }}" style="background-image:url('{{ asset('assets/img/noticias/banner-pequeno/'.$noticia->capa) }}')">
                        <span>{{ $noticia->{'titulo_'.app()->getLocale()} }}</span>
                    </a>
                    @endforeach
                    <div class="cycle-pager"></div>
                </div>
            </div>

            <a href="{{ route('news.eventos') }}" class="eventos" style="background-image:url({{ asset('assets/img/eventoscapa/'.$eventosCapa->capa) }})">
                <h2>{{ trans('frontend.news.calendario') }}</h2>
                <span>{{ trans('frontend.news.acessar') }}</span>
            </a>

            <form action="" class="newsletter" id="form-newsletter">
                <h2>{!! trans('frontend.newsletter.titulo') !!}</h2>
                <p>{{ trans('frontend.newsletter.chamada') }}</p>
                <input type="text" name="newsletter_nome" id="newsletter_nome" placeholder="{{ trans('frontend.contato.nome') }}">
                <input type="email" name="newsletter_email" id="newsletter_email" placeholder="E-mail">
                <input type="submit" value="{{ trans('frontend.newsletter.cadastrar') }}">
            </form>

            <div class="galeria">
                <div class="texto">
                    <h2>{{ trans('frontend.news.galeria') }}</h2>
                    <p>{{ $news->{'galeria_'.app()->getLocale()} }}</p>
                    <a href="{{ route('news.galeria') }}">{{ trans('frontend.news.ver-galeria') }}</a>
                </div>
                <div class="noticias-banners">
                    @foreach($galerias as $galeria)
                    <a href="{{ route('news.galeria.show', $galeria->slug) }}" style="background-image:url('{{ asset('assets/img/galerias/banner/'.$galeria->capa) }}')">
                        <span>{{ $galeria->{'titulo_'.app()->getLocale()} }}</span>
                    </a>
                    @endforeach
                    <div class="cycle-pager"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
