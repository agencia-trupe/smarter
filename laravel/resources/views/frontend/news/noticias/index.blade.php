@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('news') }}">
                    {{ trans('frontend.nav.news') }}
                </a>
                /
                <strong>{{ trans('frontend.news.novidades') }}</strong>
            </span>
        </div>
    </div>

    <div class="noticias-index">
        <div class="center">
            @unless(Request::get('pagina') && Request::get('pagina') != 1)
            <div class="noticias-banners">
                @foreach($banners as $banner)
                <a href="{{ route('news.noticias.show', $banner->slug) }}">
                    <img src="{{ asset('assets/img/noticias/banner-grande/'.$banner->capa) }}" alt="">
                    <span>{{ $banner->{'titulo_'.app()->getLocale()} }}</span>
                </a>
                @endforeach
                <div class="cycle-pager"></div>
            </div>
            @endunless

            <div class="mais-noticias">
                <h3>{{ trans('frontend.news.mais-noticias') }}</h3>

                @foreach($noticias as $noticia)
                <a href="{{ route('news.noticias.show', $noticia->slug) }}">
                    <span class="data">{{ $noticia->data }}</span>
                    <h2>{{ $noticia->{'titulo_'.app()->getLocale()} }}</h2>
                    <p>{!! $noticia->{'chamada_'.app()->getLocale()} !!}</p>
                    <span class="mais">{{ trans('frontend.news.ler-mais') }}</span>
                </a>
                @endforeach
            </div>

            {{ with(new App\Pagination\NoticiasPresenter($noticias))->render() }}
        </div>
    </div>

@endsection
