@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('news') }}">
                    {{ trans('frontend.nav.news') }}
                </a>
                /
                <strong>{{ $noticia->{'titulo_'.app()->getLocale()} }}</strong>
            </span>
        </div>
    </div>

    <div class="noticias-show">
        <div class="center">
            <img src="{{ asset('assets/img/noticias/capa/'.$noticia->capa) }}" alt="">
            <span class="data">{{ $noticia->data }}</span>
            <h2>{{ $noticia->{'titulo_'.app()->getLocale()} }}</h2>
            <p class="chamada">{!! $noticia->{'chamada_'.app()->getLocale()} !!}</p>
            <div class="texto">
                {!! $noticia->{'texto_'.app()->getLocale()} !!}
            </div>

            <div class="noticias-show-nav">
                @if($anterior)
                <a href="{{ route('news.noticias.show', $anterior->slug) }}" class="anterior">&laquo; {{ trans('frontend.news.noticia-anterior') }}</a>
                @endif
                <a href="{{ route('news.noticias') }}" class="voltar">{{ trans('frontend.news.noticias-voltar') }}</a>
                @if($proximo)
                <a href="{{ route('news.noticias.show', $proximo->slug) }}" class="proximo">{{ trans('frontend.news.proxima-noticia') }} &raquo;</a>
                @endif
            </div>
        </div>
    </div>

@endsection
