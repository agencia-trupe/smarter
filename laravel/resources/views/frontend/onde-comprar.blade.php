@extends('frontend.common.template')

@section('content')

    <div class="cabecalho-img">
        <div class="center">
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalhoImg) }}" alt="">
        </div>
    </div>

    <div class="breadcrumb-simple">
        <div class="center">
            <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.onde-comprar') }}</span>
        </div>
    </div>

    <div class="onde-comprar">
        <div class="texto">
            <div class="center">
                <h1>{{ trans('frontend.onde-comprar.distribuidores') }}</h1>
                {!! $ondeComprar->{'texto_'.app()->getLocale()}!!}
            </div>
        </div>

        <div class="imagem" style="background-image:url({{ asset('assets/img/onde-comprar/'.$ondeComprar->imagem_televendas) }})">
            <div class="center">
                <span>{{ trans('frontend.onde-comprar.ecommerce') }}</span>
            </div>
        </div>

        <div class="ecommerces">
            <div class="center">
                @foreach($ecommerces as $ecommerce)
                <a href="{{ $ecommerce->site }}" target="_blank">
                    <img src="{{ asset('assets/img/ecommerces/'.$ecommerce->marca) }}" alt="{{ $ecommerce->nome }}">
                    <span>{{ trans('frontend.onde-comprar.acesse') }}</span>
                </a>
                @endforeach
            </div>
        </div>

        <div class="imagem" style="background-image:url({{ asset('assets/img/onde-comprar/'.$ondeComprar->imagem_distribuidores) }})">
            <div class="center">
                <span>{{ trans('frontend.onde-comprar.distribuidores-regionais') }}</span>
            </div>
        </div>

        <div class="distribuidores" id="mapa">
            <div class="center">
                <div class="mapa-wrapper" @if(!$distribuidores) style="float:left" @endif>
                    <div class="mapa">
                        @foreach(Tools::listaEstados() as $uf => $nome)
                        <a href="{{ route('onde-comprar', $uf) }}#mapa" class="{{ $uf }} @if($estado && $uf == $estado) active @endif">{{ $nome }}</a>
                        @endforeach
                    </div>
                    <p>{!! trans('frontend.onde-comprar.estados') !!}</p>
                </div>

                {!! Form::select('estado', Tools::listaEstados(), strtoupper($estado), ['placeholder' => trans('frontend.onde-comprar.estado')]) !!}

                @if($distribuidores)
                <div class="distribuidores-lista">
                    <h2>{{ Tools::listaEstados()[strtoupper($estado)] }}</h2>
                    @if(!count($distribuidores))
                    <p class="nenhum">{{ trans('frontend.onde-comprar.nenhum') }}</p>
                    @else
                    @foreach($distribuidores as $d)
                    <div class="distribuidor">
                        <img src="{{ asset('assets/img/distribuidores/'.$d->imagem) }}" alt="">
                        <p>
                            <span>
                                <strong>{{ $d->cidade }} - {{ strtoupper($estado) }}</strong>
                            </span>
                            <span>
                                <strong>{{ trans('frontend.contato.empresa') }}:</strong> {{ $d->empresa }}
                            </span>
                            @if($d->endereco)
                            <span>
                                <strong>{{ trans('frontend.contato.endereco') }}:</strong> {{ $d->endereco }}
                            </span>
                            @endif
                            @if($d->cep)
                            <span>
                                <strong>CEP:</strong> {{ $d->cep }}
                            </span>
                            @endif
                            @if($d->telefone)
                            <span>
                                <strong>{{ trans('frontend.contato.telefone') }}:</strong> {{ $d->telefone }}
                            </span>
                            @endif
                            @if($d->e_mail)
                            <span>
                                <strong>E-mail:</strong> <a href="mailto:{{ $d->e_mail }}">{{ $d->e_mail }}</a>
                            </span>
                            @endif
                            @if($d->site)
                            <span>
                                <strong>Site:</strong> <a href="{{ $d->site }}" target="_blank">{{ $d->site }}</a>
                            </span>
                            @endif
                        </p>
                    </div>
                    @endforeach
                    @endif
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
