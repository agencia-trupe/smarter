@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                {{ trans('frontend.nav.produtos') }}
                /
                <a href="{{ route('produtos', 'acessorios-consumiveis-epis') }}">
                    {{ trans('frontend.nav.acessorios-consumiveis-epis') }}
                </a>
                /
                <strong>{{ $produto->{'titulo_'.app()->getLocale()} }}</strong>
            </span>
        </div>
    </div>

    <div class="produtos-show">
        <div class="center">
            <div class="right">
                <h3>
                    {{ $produto->{'descricao_'.app()->getLocale()} }}<br>
                    <span style="margin-top:5px;font-weight:700;color:#000">{{ $produto->modelo }}</span>
                </h3>
                <h1>{{ $produto->codigo }}</h1>
                {!! $produto->{'descricao_completa_'.app()->getLocale()} !!}
            </div>

            <div class="left">
                @if(count($produto->imagens))
                <div class="imagens imagens-acessorios">
                    <div class="imagens-cycle-wrapper">
                        <div class="imagens-cycle">
                            @foreach($produto->imagens as $imagem)
                            <img src="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}">
                            @endforeach
                        </div>
                        <div id="prev"></div>
                        <div id="next"></div>
                    </div>
                    <div class="imagens-carousel">
                        @foreach($produto->imagens as $imagem)
                        <div>
                            <div class="container">
                                <img src='{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}'>
                                <div class="fx"></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>

        @if(count($produto->destaques))
        <div class="destaques">
            <div class="center">
                <h2>{{ trans('frontend.produtos.destaques') }}</h2>
                <div class="lista">
                    @foreach($produto->destaques->chunk(2) as $chunk)
                    <div class="row">
                        @foreach($chunk as $destaque)
                        <div>
                            <img src="{{ asset('assets/img/destaques/'.$destaque->imagem) }}" alt="">
                            <div class="texto">
                                <h3>{{ $destaque->{'titulo_'.app()->getLocale()} }}</h3>
                                <p>{{ $destaque->{'texto_'.app()->getLocale()} }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

        @if($produto->{'texto_'.app()->getLocale()})
        <div class="texto">
            <div class="center">
                {!! $produto->{'texto_'.app()->getLocale()} !!}
            </div>
        </div>
        @endif
    </div>

@endsection
