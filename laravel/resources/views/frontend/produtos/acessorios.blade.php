@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                {{ trans('frontend.nav.produtos') }}
                /
                <a href="{{ route('produtos', 'acessorios-consumiveis-epis') }}">
                    {{ trans('frontend.nav.acessorios-consumiveis-epis') }}
                </a>
                /
                <strong>{{ $categoria->{'titulo_'.app()->getLocale()} }}</strong>
            </span>
        </div>
    </div>

    <div class="produtos-acessorios-show">
        <div class="center">
            <img src="{{ asset('assets/img/acessorioscapa/'.$categoria->capa) }}" alt="">
            <h1>{{ $categoria->{'titulo_'.app()->getLocale()} }}</h1>
            <div class="texto">
                {!! $categoria->{'descricao_'.app()->getLocale()} !!}
            </div>
            <table>
                <thead>
                    <th>{{ trans('frontend.produtos.produto') }}</th>
                    <th>{{ trans('frontend.produtos.codigo') }}</th>
                    <th>{{ trans('frontend.produtos.modelo') }}</th>
                    <th>{{ trans('frontend.produtos.descricao') }}</th>
                </thead>
                @foreach($categoria->acessorios_consumiveis_epis as $p)
                <tr>
                    <td>
                        @if($p->descricao_completa_pt && $p->descricao_completa_en)
                        <a href="{{ route('produtos.acessorios.show', [$categoria->slug, $p->slug]) }}">
                            <img src="{{ asset('assets/img/acessoriosprodutos/'.$p->imagem) }}" alt="{{ $p->{'titulo_'.app()->getLocale()} }}" title="{{ $p->{'titulo_'.app()->getLocale()} }}">
                        </a>
                        @else
                        <img src="{{ asset('assets/img/acessoriosprodutos/'.$p->imagem) }}" alt="{{ $p->{'titulo_'.app()->getLocale()} }}" title="{{ $p->{'titulo_'.app()->getLocale()} }}">
                        @endif
                    </td>
                    <td>{{ $p->codigo }}</td>
                    <td>{{ $p->modelo }}</td>
                    <td>{{ $p->{'descricao_'.app()->getLocale()} }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
