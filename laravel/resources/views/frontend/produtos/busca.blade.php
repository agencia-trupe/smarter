@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                {{ trans('frontend.nav.produtos') }}
                /
                {{ trans('frontend.produtos.resultado') }} {{ $termo }}
            </span>
        </div>
    </div>

    <div class="produtos-index">
        <div class="center">
            @if(count($produtos))
            <div class="thumbs">
                @foreach($produtos as $produto)
                <a href="{{ route('produtos.show', [$produto->categoria, $produto->slug]) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    <span class="subtitulo">{{ $produto->{'subtitulo_'.app()->getLocale()} }}</span>
                    <span class="titulo">{{ $produto->{'titulo_'.app()->getLocale()} }}</span>
                    <span class="saiba">{{ trans('frontend.produtos.saiba-mais') }}</span>
                </a>
                @endforeach
            </div>
            @else
            <div class="nenhum">
                {{ trans('frontend.produtos.nenhum') }}
            </div>
            @endif
        </div>
    </div>

@endsection
