@extends('frontend.common.template')

@section('content')

    <?php
        $fieldCapa = [
            'soldagem-eletrodo'           => 'soldagem_eletrodo',
            'soldagem-tig'                => 'soldagem_tig',
            'soldagem-mig-mag'            => 'soldagem_migmag',
            'corte-plasma'                => 'corte_plasma',
            'acessorios-consumiveis-epis' => 'acessorios'
        ];
    ?>

    @foreach($categorias as $c => $t)
    <a href="{{ route('produtos', $c) }}#{{ $c }}" class="produtos-banner-categoria {{ $c }}" style="background-image:url({{ asset('assets/img/produtoscapas/'.$capas->{$fieldCapa[$c]}) }})">
        <div class="center">
            <div class="titulo-wrapper">
                <div>
                    <span>{!! trans('frontend.produtos.titulo-'.$c) !!}</span>
                </div>
            </div>
        </div>
    </a>
    <style>
        .produtos-banner-categoria.{{ $c }}:hover {
            background-image: url({{ asset('assets/img/produtoscapas/'.$capas->{$fieldCapa[$c].'_ativo'}) }}) !important;
        }
    </style>
    @endforeach

@endsection
