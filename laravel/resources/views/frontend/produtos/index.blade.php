@extends('frontend.common.template')

@section('content')

    <?php
        $fieldCapa = [
            'soldagem-eletrodo'           => 'soldagem_eletrodo',
            'soldagem-tig'                => 'soldagem_tig',
            'soldagem-mig-mag'            => 'soldagem_migmag',
            'corte-plasma'                => 'corte_plasma',
            'acessorios-consumiveis-epis' => 'acessorios'
        ];
    ?>

    @foreach($categorias as $c => $t)
        @if($categoria == 'acessorios-consumiveis-epis' && $categoria == $c)
        <div class="produtos-banner-categoria {{ $categoria }}" id="{{ $c }}" style="background-image:url({{ asset('assets/img/produtoscapas/'.$capas->{$fieldCapa[$c].'_ativo'}) }})">
            <div class="center">
                <div class="titulo-wrapper">
                    <div>
                        <span>{!! trans('frontend.produtos.titulo-'.$categoria) !!}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="breadcrumb-simple">
            <div class="center">
                <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.produtos') }} &rsaquo; {{ trans('frontend.nav.'.$categoria) }}</span>
            </div>
        </div>

        <div class="produtos-index produtos-acessorios-index">
            <div class="center">
                @foreach($acessorios as $cat)
                    @if(count($cat->acessorios_consumiveis_epis))
                    <div class="acessorios-row">
                        <div class="linha-div">{{ $cat->{'titulo_'.app()->getLocale()} }}</div>
                        <div class="acessorios-produtos">
                            @foreach($cat->acessorios_consumiveis_epis()->take(3)->get() as $p)
                            <div>
                                <img src="{{ asset('assets/img/acessoriosprodutos/'.$p->imagem) }}" alt="">
                                <span>{{ $p->{'titulo_'.app()->getLocale()} }}</span>
                            </div>
                            @endforeach
                        </div>
                        <a href="{{ route('produtos.acessorios', $cat->slug) }}">{{ trans('frontend.produtos.ver-mais') }}</a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
        @elseif($categoria == $c)
        <div class="produtos-banner-categoria {{ $categoria }}" id="{{ $c }}" style="background-image:url({{ asset('assets/img/produtoscapas/'.$capas->{$fieldCapa[$c].'_ativo'}) }})">
            <div class="center">
                <div class="titulo-wrapper">
                    <div>
                        <span>{!! trans('frontend.produtos.titulo-'.$categoria) !!}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="breadcrumb-simple">
            <div class="center">
                <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.produtos') }} &rsaquo; {{ trans('frontend.nav.'.$categoria) }}</span>
            </div>
        </div>

        <div class="produtos-index">
            <div class="center">
                @foreach($linhas as $linha)
                @unless(!$linha->produtosCat->count())
                <div class="linha-div">{{ $linha->{'titulo_'.app()->getLocale()} }}</div>
                <div class="thumbs">
                    @foreach($linha->produtosCat as $produto)
                    <a href="{{ route('produtos.show', [$categoria, $produto->slug]) }}">
                        <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                        <span class="subtitulo">{{ $produto->{'subtitulo_'.app()->getLocale()} }}</span>
                        <span class="titulo">{{ $produto->{'titulo_'.app()->getLocale()} }}</span>
                        <span class="saiba">{{ trans('frontend.produtos.saiba-mais') }}</span>
                    </a>
                    @endforeach
                </div>
                @endunless
                @endforeach
            </div>
        </div>
        @else
        <a href="{{ route('produtos', $c) }}#{{ $c }}" class="produtos-banner-categoria {{ $c }}" style="background-image:url({{ asset('assets/img/produtoscapas/'.$capas->{$fieldCapa[$c]}) }})">
            <div class="center">
                <div class="titulo-wrapper">
                    <div>
                        <span>{!! trans('frontend.produtos.titulo-'.$c) !!}</span>
                    </div>
                </div>
            </div>
        </a>
        <style>
            .produtos-banner-categoria.{{ $c }}:hover {
                background-image: url({{ asset('assets/img/produtoscapas/'.$capas->{$fieldCapa[$c].'_ativo'}) }}) !important;
            }
        </style>
        @endif
    @endforeach

@endsection
