@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                {{ trans('frontend.nav.produtos') }}
                /
                <a href="{{ route('produtos', $produto->categoria) }}">
                    {{ trans('frontend.nav.'.$produto->categoria) }}
                </a>
                /
                <strong>{{ $produto->{'titulo_'.app()->getLocale()} }}</strong>
            </span>
        </div>
    </div>

    <div class="produtos-show">
        <div class="center">
            <div class="right">
                <h3>{{ $produto->{'subtitulo_'.app()->getLocale()} }}</h3>
                <h1>{{ $produto->{'titulo_'.app()->getLocale()} }}</h1>
                {!! $produto->{'chamada_'.app()->getLocale()} !!}

                @if(count($produto->recursos))
                <div class="recursos">
                    <h2>{{ trans('frontend.produtos.recursos-especiais') }}</h2>
                    <div>
                        @foreach($produto->recursos as $recurso)
                        <img src="{{ asset('assets/img/recursos-especiais/'.$recurso->imagem) }}" alt="">
                        @endforeach
                    </div>
                </div>
                @endif

                <div class="tabela">
                    <img src="{{ asset('assets/img/produtos/tabelas/'.$produto->{'tabela_'.app()->getLocale()}) }}" style="display:block;max-width:100%" alt="">
                </div>
            </div>

            <div class="left">
                @if(count($produto->imagens))
                <div class="imagens">
                    <div class="imagens-cycle-wrapper">
                        <div class="imagens-cycle">
                            @foreach($produto->imagens as $imagem)
                            <img src="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}">
                            @endforeach
                        </div>
                        <div id="prev"></div>
                        <div id="next"></div>
                    </div>
                    <div class="imagens-carousel">
                        @foreach($produto->imagens as $imagem)
                        <div>
                            <div class="container">
                                <img src='{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}'>
                                <div class="fx"></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                <div class="certificacoes">
                    <img src="{{ asset('assets/img/layout/certificados-produtos.png') }}" alt="">
                </div>
            </div>

            <div style="clear:both"></div>

            @if(count($produto->acessorios))
            <div class="acessorios">
                @if(count($produto->acessorios->where('tipo', 'Incluso')))
                    <div class="tipo">
                        <h2>
                            <span>{{ trans('frontend.produtos.acessorios') }}</span>
                            <span><span>{!! trans('frontend.produtos.inclusos') !!}</span></span>
                        </h2>
                        <div class="desktop">
                        @foreach($produto->acessorios->where('tipo', 'Incluso')->chunk(3) as $chunk)
                            <div class="row">
                            @foreach($chunk as $acessorio)
                                <div class="acessorio">
                                    <img src="{{ asset('assets/img/acessorios/'.$acessorio->imagem) }}" alt="">
                                    <span>{{ $acessorio->{'titulo_'.app()->getLocale()} }}</span>
                                </div>
                            @endforeach
                            </div>
                        @endforeach
                        </div>
                        <div class="mobile">
                        @foreach($produto->acessorios->where('tipo', 'Incluso')->chunk(2) as $chunk)
                            <div class="row">
                            @foreach($chunk as $acessorio)
                                <div class="acessorio">
                                    <img src="{{ asset('assets/img/acessorios/'.$acessorio->imagem) }}" alt="">
                                    <span>{{ $acessorio->{'titulo_'.app()->getLocale()} }}</span>
                                </div>
                            @endforeach
                            </div>
                        @endforeach
                        </div>
                    </div>
                @endif
                @if(count($produto->acessorios->where('tipo', 'Opcional')))
                    <div class="tipo">
                        <h2>
                            <span>{{ trans('frontend.produtos.acessorios') }}</span>
                            <span><span>{!! trans('frontend.produtos.opcionais') !!}</span></span>
                        </h2>
                        <div class="desktop">
                        @foreach($produto->acessorios->where('tipo', 'Opcional')->chunk(3) as $chunk)
                            <div class="row">
                            @foreach($chunk as $acessorio)
                                <div class="acessorio">
                                    <img src="{{ asset('assets/img/acessorios/'.$acessorio->imagem) }}" alt="">
                                    <span>{{ $acessorio->{'titulo_'.app()->getLocale()} }}</span>
                                </div>
                            @endforeach
                            </div>
                        @endforeach
                        </div>
                        <div class="mobile">
                        @foreach($produto->acessorios->where('tipo', 'Opcional')->chunk(2) as $chunk)
                            <div class="row">
                            @foreach($chunk as $acessorio)
                                <div class="acessorio">
                                    <img src="{{ asset('assets/img/acessorios/'.$acessorio->imagem) }}" alt="">
                                    <span>{{ $acessorio->{'titulo_'.app()->getLocale()} }}</span>
                                </div>
                            @endforeach
                            </div>
                        @endforeach
                        </div>
                    </div>
                @endif
            </div>
            @endif
        </div>

        @if(count($produto->destaques))
        <div class="destaques">
            <div class="center">
                <h2>{{ trans('frontend.produtos.destaques') }}</h2>
                <div class="lista">
                    @foreach($produto->destaques->chunk(2) as $chunk)
                    <div class="row">
                        @foreach($chunk as $destaque)
                        <div>
                            <img src="{{ asset('assets/img/destaques/'.$destaque->imagem) }}" alt="">
                            <div class="texto">
                                <h3>{{ $destaque->{'titulo_'.app()->getLocale()} }}</h3>
                                <p>{{ $destaque->{'texto_'.app()->getLocale()} }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

        @if($produto->{'texto_'.app()->getLocale()})
        <div class="texto">
            <div class="center">
                {!! $produto->{'texto_'.app()->getLocale()} !!}
            </div>
        </div>
        @endif

        @if(count($produto->especificacoes) || count($produto->downloads) || count($produto->fotos) || count($produto->videos))
        <div class="especificacoes">
            <div class="center">
                <h2>{{ trans('frontend.produtos.especificacoes') }}</h2>

                <nav class="tabs-nav">
                    @if(count($produto->especificacoes))
                    <a href="#especificacoes-tecnicas">
                        <span>{{ trans('frontend.produtos.especificacoes-tecnicas') }}</span>
                    </a>
                    @endif
                    @if(count($produto->downloads))
                    <a href="#downloads">
                        <span>Downloads</span>
                    </a>
                    @endif
                    @if(count($produto->fotos) || count($produto->videos))
                    <a href="#fotos-videos">
                        <span>{{ trans('frontend.produtos.fotos-videos') }}</span>
                    </a>
                    @endif
                </nav>

                <div class="tabs-conteudo">
                    @if(count($produto->especificacoes))
                    <div id="especificacoes-tecnicas">
                        <table>
                            <thead>
                                <th>{{ trans('frontend.produtos.produto') }}</th>
                                <th>{{ $produto->{'titulo_'.app()->getLocale()} }}</th>
                            </thead>
                            @foreach($produto->especificacoes as $e)
                            <tr>
                                <td>{{ $e->{'titulo_'.app()->getLocale()} }}</td>
                                <td>{!! $e->{'valor_'.app()->getLocale()} !!}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @endif

                    @if(count($produto->downloads))
                    <div id="downloads">
                        @foreach($produto->downloads as $d)
                        <div>
                            <p>
                                {{ $d->{'titulo_'.app()->getLocale()} }}
                                <strong>{{ $produto->{'titulo_'.app()->getLocale()} }}</strong>
                                -
                                <a href="{{ url('downloads/'.$d->arquivo) }}" target="_blank">DOWNLOAD</a>
                            </p>
                        </div>
                        @endforeach
                    </div>
                    @endif

                    @if(count($produto->fotos) || count($produto->videos))
                    <div id="fotos-videos">
                        @foreach($produto->fotos as $f)
                        <a href="{{ asset('assets/img/produtos/fotos/'.$f->imagem) }}" class="produtos-foto">
                            <img src="{{ asset('assets/img/produtos/fotos/thumbs/'.$f->imagem) }}" alt="">
                        </a>
                        @endforeach
                        @foreach($produto->videos as $v)
                        <a href="{{ $v->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$v->video_codigo : 'https://player.vimeo.com/video/'.$v->video_codigo }}" class="produtos-video">
                            <img src="{{ asset('assets/img/videos/'.$v->capa) }}" alt="">
                            <span class="video-icone"></span>
                        </a>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endif
    </div>

@endsection
