@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('suporte') }}">
                {{ trans('frontend.nav.suporte') }}
                </a>
                /
                <strong>{{ trans('frontend.suporte.garantia-de-produtos') }}</strong>
            </span>
        </div>
    </div>

    <div class="garantia-de-produtos">
        <div class="center">
            <div class="texto">
                <h1>{{ trans('frontend.suporte.garantia-de-produtos') }}</h1>
                {!! $garantiaDeProdutos->{'texto_abertura_'.app()->getLocale()} !!}
                <a href="{{ route('suporte.garantia-de-produtos.saiba-mais') }}">
                    {{ ucfirst(trans('frontend.produtos.saiba-mais')) }}
                </a>
            </div>
            <img src="{{ asset('assets/img/garantia-de-produtos/'.$garantiaDeProdutos->imagem_abertura) }}" alt="">

            <div class="assistencia">
                <h2>{{ trans('frontend.suporte.assistencia-tecnica') }}</h2>
                <p>{!! trans('frontend.suporte.assistencia-texto') !!}</p>
            </div>

            <div class="contatos">
                <div class="telefone">
                    <span>{{ trans('frontend.contato.telefone') }}</span>
                    <span>{{ $garantiaDeProdutos->telefone }}</span>
                </div>
                <div class="email">
                    <span>E-mail</span>
                    <span><a href="mailto:{{ $garantiaDeProdutos->e_mail }}">{{ $garantiaDeProdutos->e_mail }}</a></span>
                </div>
                <div class="whatsapp">
                    <span>WhatsApp</span>
                    <span>{{ $garantiaDeProdutos->whatsapp }}</span>
                </div>
                <style>
                    @media only screen and (min-width:768px) {
                        .garantia-de-produtos .contatos .whatsapp span:last-child {
                            font-size: 34px;
                            left: 174px;
                            top: 80px;
                        }
                    }
                </style>
            </div>

            <div class="solicitacao">
                <h2>{{ trans('frontend.suporte.solicitacao-de-garantia') }}</h2>
                <p>{{ trans('frontend.suporte.solicitacao-de-garantia-texto') }}</p>
            </div>

            @if(session('success'))
            <div class="solicitacao-enviada">
                <h3>{{ trans('frontend.suporte.solicitacao-enviada') }}</h3>
            </div>
            @else
            <form action="{{ route('suporte.garantia-de-produtos.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erros">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif

                <div class="row">
                    <label>{{ trans('frontend.contato.nome') }}</label>
                    {!! Form::text('nome', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.empresa') }}</label>
                    {!! Form::text('empresa', null) !!}
                </div>
                <div class="row">
                    <label>CPF / CNPJ</label>
                    {!! Form::text('cpf_cnpj', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.cidade') }}</label>
                    {!! Form::text('cidade', null) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.estado') }}</label>
                    {!! Form::text('estado', null) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.telefone') }}</label>
                    {!! Form::text('telefone', null) !!}
                </div>
                <div class="row">
                    <label>E-mail</label>
                    {!! Form::email('email', null) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.produtos.produto') }}</label>
                    {!! Form::select('produto', $produtos, null, ['placeholder' => '---', 'required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.suporte.numero-nf') }}</label>
                    {!! Form::text('nf', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.suporte.descricao-problema') }}</label>
                    {!! Form::textarea('problema', null, ['required' => true]) !!}
                </div>
                <div class="row">
                    <label>{{ trans('frontend.contato.anexar') }}</label>
                    <div class="anexo-wrapper">
                        {!! Form::file('arquivo', null) !!}
                    </div>
                </div>
                <div class="submit-wrapper">
                    <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                </div>
            </form>
            @endif
        </div>
    </div>

    <div class="suporte-thumbs-2">
        <div class="center">
            <a href="{{ route('suporte.orientacoes-tecnicas') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->orientacoes_tecnicas_capa) }}">
                <h3>{{ trans('frontend.suporte.orientacoes-tecnicas') }}</h3>
                <p>{{ $suporte->{'orientacoes_tecnicas_abertura_'.app()->getLocale()} }}</p>
            </a>
            <a href="{{ route('suporte.manuais-de-produtos') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->manuais_de_produtos_capa) }}">
                <h3>{{ trans('frontend.suporte.manuais-de-produtos') }}</h3>
                <p>{{ $suporte->{'manuais_de_produtos_abertura_'.app()->getLocale()} }}</p>
            </a>
        </div>
    </div>

@endsection
