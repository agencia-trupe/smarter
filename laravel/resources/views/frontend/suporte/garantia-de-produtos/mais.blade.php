@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('suporte') }}">
                {{ trans('frontend.nav.suporte') }}
                </a>
                /
                <a href="{{ route('suporte.garantia-de-produtos') }}">
                {{ trans('frontend.suporte.garantia-de-produtos') }}
                </a>
                /
                <strong>{{ ucfirst(trans('frontend.produtos.saiba-mais')) }}</strong>
            </span>
        </div>
    </div>

    <div class="garantia-de-produtos-mais">
        <div class="center">
            <img src="{{ asset('assets/img/garantia-de-produtos/'.$garantiaDeProdutos->imagem_saiba_mais) }}" alt="">
            <div class="texto">
                <h1>{{ trans('frontend.suporte.garantia-de-produtos') }}</h1>
                {!! $garantiaDeProdutos->{'texto_saiba_mais_'.app()->getLocale()} !!}
            </div>
        </div>
    </div>

    <div class="suporte-thumbs-3">
        <div class="center">
            <a href="{{ route('suporte.orientacoes-tecnicas') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->orientacoes_tecnicas_capa) }}">
                <span>{{ trans('frontend.suporte.orientacoes-tecnicas') }}</span>
            </a>
            <a href="{{ route('suporte.garantia-de-produtos') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->garantia_de_produtos_capa) }}">
                <span>{{ trans('frontend.suporte.garantia-de-produtos') }}</span>
            </a>
            <a href="{{ route('suporte.manuais-de-produtos') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->manuais_de_produtos_capa) }}">
                <span>{{ trans('frontend.suporte.manuais-de-produtos') }}</span>
            </a>
        </div>
    </div>

@endsection
