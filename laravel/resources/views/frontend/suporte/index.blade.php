@extends('frontend.common.template')

@section('content')

    <div class="cabecalho-img">
        <div class="center">
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalhoImg) }}" alt="">
        </div>
    </div>

    <div class="breadcrumb-simple">
        <div class="center">
            <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.suporte') }}</span>
        </div>
    </div>

    <div class="suporte">
        <div class="center">
            <a href="{{ route('suporte.orientacoes-tecnicas') }}">
                <div class="texto">
                    <h3>{{ trans('frontend.suporte.orientacoes-tecnicas') }}</h3>
                    <p>{{ $suporte->{'orientacoes_tecnicas_abertura_'.app()->getLocale()} }}</p>
                    <span>{{ trans('frontend.tutoriais.acesse') }}</span>
                </div>
                <div class="imagem" style="background-image:url({{ asset('assets/img/suporte/'.$suporte->orientacoes_tecnicas_capa) }})"></div>
            </a>
            <div class="divider"></div>
            <a href="{{ route('suporte.garantia-de-produtos') }}">
                <div class="texto">
                    <h3>{{ trans('frontend.suporte.garantia-de-produtos') }}</h3>
                    <p>{{ $suporte->{'garantia_de_produtos_abertura_'.app()->getLocale()} }}</p>
                    <span>{{ trans('frontend.tutoriais.acesse') }}</span>
                </div>
                <div class="imagem" style="background-image:url({{ asset('assets/img/suporte/'.$suporte->garantia_de_produtos_capa) }})"></div>
            </a>
            <div class="divider"></div>
            <a href="{{ route('suporte.manuais-de-produtos') }}">
                <div class="texto">
                    <h3>{{ trans('frontend.suporte.manuais-de-produtos') }}</h3>
                    <p>{{ $suporte->{'manuais_de_produtos_abertura_'.app()->getLocale()} }}</p>
                    <span>{{ trans('frontend.tutoriais.acesse') }}</span>
                </div>
                <div class="imagem" style="background-image:url({{ asset('assets/img/suporte/'.$suporte->manuais_de_produtos_capa) }})"></div>
            </a>
        </div>
    </div>

@endsection
