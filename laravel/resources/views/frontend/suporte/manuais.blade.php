@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('suporte') }}">
                {{ trans('frontend.nav.suporte') }}
                </a>
                /
                <strong>{{ trans('frontend.suporte.manuais-de-produtos') }}</strong>
            </span>
        </div>
    </div>

    <div class="manuais">
        <div class="center">
            <h1>{{ trans('frontend.suporte.manuais-de-produtos') }}</h1>
            <p>{{ trans('frontend.suporte.manuais-de-produtos-texto') }}</p>

            @foreach($linhas as $linha)
                @unless(!$linha->produtosManuais->count())
                <div class="linha-div">{{ $linha->{'titulo_'.app()->getLocale()} }}</div>
                <div class="manuais-lista">
                    @foreach($linha->produtosManuais as $produto)
                    <div class="manual" id="{{ $produto->id }}">
                        <h3>{{ $produto->{'titulo_'.app()->getLocale()} }}</h3>
                        <a href="#" class="manual-toggle">{{ trans('frontend.suporte.baixar-manual') }}</a>
                        <form action="{{ route('suporte.manuais-de-produtos.post', $produto->id) }}" method="POST" enctype="multipart/form-data" style="display:{{ session('form') == $produto->id && $errors->any() ? 'block' : 'none' }}">
                            {!! csrf_field() !!}
                            @if(session('form') == $produto->id && $errors->any())
                                <div class="erros">
                                    @foreach($errors->all() as $error)
                                    {!! $error !!}<br>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::hidden('produto', $produto->{'titulo_'.app()->getLocale()}) !!}
                            <div class="row">
                                <label>{{ trans('frontend.contato.nome') }}</label>
                                @if(session('form') == $produto->id)
                                {!! Form::text('nome', null, ['required' => true]) !!}
                                @else
                                <input type="text" name="nome" required>
                                @endif
                            </div>
                            <div class="row">
                                <label>CPF / CNPJ</label>
                                @if(session('form') == $produto->id)
                                {!! Form::text('cpf_cnpj', null, ['required' => true]) !!}
                                @else
                                <input type="text" name="cpf_cnpj" required>
                                @endif
                            </div>
                            <div class="row">
                                <label>{{ trans('frontend.contato.telefone') }}</label>
                                @if(session('form') == $produto->id)
                                {!! Form::text('telefone', null, ['required' => true]) !!}
                                @else
                                <input type="text" name="telefone" required>
                                @endif
                            </div>
                            <div class="row">
                                <label>E-mail</label>
                                @if(session('form') == $produto->id)
                                {!! Form::email('email', null, ['required' => true]) !!}
                                @else
                                <input type="email" name="email" required>
                                @endif
                            </div>
                            <div class="row">
                                <label>{{ trans('frontend.suporte.copia-nf') }}</label>
                                <div class="anexo-wrapper">
                                    {!! Form::file('nf', null) !!}
                                </div>
                            </div>
                            <div class="submit-wrapper">
                                <input type="submit" value="{{ trans('frontend.suporte.baixar') }}">
                            </div>
                        </form>
                    </div>
                    @endforeach
                </div>
                @endunless
            @endforeach
        </div>
    </div>

    <div class="suporte-thumbs-2">
        <div class="center">
            <a href="{{ route('suporte.orientacoes-tecnicas') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->orientacoes_tecnicas_capa) }}">
                <h3>{{ trans('frontend.suporte.orientacoes-tecnicas') }}</h3>
                <p>{{ $suporte->{'orientacoes_tecnicas_abertura_'.app()->getLocale()} }}</p>
            </a>
            <a href="{{ route('suporte.garantia-de-produtos') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->garantia_de_produtos_capa) }}">
                <h3>{{ trans('frontend.suporte.garantia-de-produtos') }}</h3>
                <p>{{ $suporte->{'garantia_de_produtos_abertura_'.app()->getLocale()} }}</p>
            </a>
        </div>
    </div>

@endsection
