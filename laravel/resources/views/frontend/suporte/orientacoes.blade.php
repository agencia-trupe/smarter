@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('suporte') }}">
                {{ trans('frontend.nav.suporte') }}
                </a>
                /
                <strong>{{ trans('frontend.suporte.orientacoes-tecnicas') }}</strong>
            </span>
        </div>
    </div>

    <div class="orientacoes">
        <div class="center">
            <h1>{{ trans('frontend.suporte.orientacoes-tecnicas') }}</h1>
            <p>{{ trans('frontend.suporte.orientacoes-tecnicas-texto') }}</p>

            @if(session('success'))
            <div class="contato-enviado">
                <h3>{{ trans('frontend.contato.enviado') }}</h3>
            </div>
            @endif

            <div class="linhas">
                <h2>{{ trans('frontend.suporte.linha') }}</h2>
                @foreach($linhas as $l)
                    @unless(!$l->produtos->count())
                    @if(session('linha'))
                    <a href="#" data-linha="{{ $l->id }}" class="o-linha-toggle @if(session('linha') == $l->id) active @endif">
                    @else
                    <a href="#" data-linha="{{ $l->id }}" class="o-linha-toggle @if($linhas->first()->id == $l->id) active @endif">
                    @endif
                        {{ $l->{'titulo_'.app()->getLocale()} }}
                    </a>
                    @endunless
                @endforeach
            </div>
            <div class="maquinas">
                <h2>{{ trans('frontend.suporte.maquina') }}</h2>
                @foreach($linhas as $l)
                    @unless(!$l->produtos->count())
                    @if(session('linha'))
                    <div class="maquinas-lista" id="{{ $l->id }}" style="display:{{ session('linha') == $l->id ? 'block' : 'none' }}">
                    @else
                    <div class="maquinas-lista" id="{{ $l->id }}" style="display:{{ $linhas->first()->id == $l->id ? 'block' : 'none' }}">
                    @endif
                        @foreach($l->produtos as $produto)
                        <a href="#" class="orientacoes-form-toggle @if(session('produto') == $produto->id) active @endif">
                            {{ $produto->{'titulo_'.app()->getLocale()} }}
                        </a>
                        <form action="{{ route('suporte.orientacoes-tecnicas.post', [$l->id, $produto->id]) }}" method="POST" enctype="multipart/form-data" style="display:{{ session('produto') == $produto->id ? 'block': 'none' }}">
                            {!! csrf_field() !!}
                            @if(session('produto') == $produto->id && $errors->any())
                                <div class="erros">
                                    @foreach($errors->all() as $error)
                                    {!! $error !!}<br>
                                    @endforeach
                                </div>
                            @endif
                            {!! Form::hidden('produto', $produto->{'titulo_'.app()->getLocale()}) !!}
                            <div class="row">
                                <label>{{ trans('frontend.contato.nome') }}</label>
                                @if(session('produto') == $produto->id)
                                {!! Form::text('nome', null, ['required' => true]) !!}
                                @else
                                <input type="text" name="nome" required>
                                @endif
                            </div>
                            <div class="row">
                                <label>E-mail</label>
                                @if(session('produto') == $produto->id)
                                {!! Form::email('email', null, ['required' => true]) !!}
                                @else
                                <input type="email" name="email" required>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label>{{ trans('frontend.contato.telefone') }}</label>
                                    @if(session('produto') == $produto->id)
                                    {!! Form::text('telefone', null, ['required' => true]) !!}
                                    @else
                                    <input type="text" name="telefone" required>
                                    @endif
                                </div>
                                <div class="col">
                                    <label>Cidade / Estado</label>
                                    @if(session('produto') == $produto->id)
                                    {!! Form::text('cidade_estado', null, ['required' => true]) !!}
                                    @else
                                    <input type="text" name="cidade_estado" required>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <textarea name="solicitacao" placeholder="{{ trans('frontend.suporte.solicitacao') }}" required>@if(session('produto') == $produto->id){{ old('solicitacao') }}@endif</textarea>
                            </div>
                            <div class="row">
                                {!! Form::file('arquivo', null) !!}
                            </div>
                            <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                        </form>
                        @endforeach
                    </div>
                    @endunless
                @endforeach
            </div>

            <p class="contato">
                {{ trans('frontend.suporte.duvida') }}
                <a href="{{ route('contato') }}">{{ trans('frontend.suporte.contato') }}</a>
            </p>
        </div>
    </div>

    <div class="suporte-thumbs-2">
        <div class="center">
            <a href="{{ route('suporte.garantia-de-produtos') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->garantia_de_produtos_capa) }}">
                <h3>{{ trans('frontend.suporte.garantia-de-produtos') }}</h3>
                <p>{{ $suporte->{'garantia_de_produtos_abertura_'.app()->getLocale()} }}</p>
            </a>
            <a href="{{ route('suporte.manuais-de-produtos') }}">
                <img src="{{ asset('assets/img/suporte/'.$suporte->manuais_de_produtos_capa) }}">
                <h3>{{ trans('frontend.suporte.manuais-de-produtos') }}</h3>
                <p>{{ $suporte->{'manuais_de_produtos_abertura_'.app()->getLocale()} }}</p>
            </a>
        </div>
    </div>

@endsection
