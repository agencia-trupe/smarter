@extends('frontend.common.template')

@section('content')

    <div class="cabecalho-img">
        <div class="center">
            <img src="{{ asset('assets/img/cabecalhos/'.$cabecalhoImg) }}" alt="">
        </div>
    </div>

    <div class="breadcrumb-simple">
        <div class="center">
            <span><a href="{{ route('home') }}">Home</a> &rsaquo; {{ trans('frontend.nav.tutoriais') }}</span>
        </div>
    </div>

    <div class="tutoriais-index">
        <div class="center">
            <div class="categorias">
                <h4>{{ trans('frontend.tutoriais.categorias') }}</h4>
                @foreach($categorias as $cat)
                <a href="{{ route('tutoriais', $cat->slug) }}" @if($categoria->exists && $categoria->id == $cat->id) class="active" @endif>
                    - {{ $cat->{'titulo_'.app()->getLocale()} }} ({{ $cat->tutoriais->count() }})
                </a>
                @endforeach
            </div>

            <div class="tutoriais-links">
                @foreach($tutoriais as $tutorial)
                @if($primeiro == $tutorial->id)
                <a href="{{ route('tutoriais.show', [$tutorial->categoria->slug, $tutorial->slug]) }}" class="tutorial-primeiro">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/tutoriais/thumb-grande/'.$tutorial->thumb) }}" alt="">
                    </div>
                    <div class="texto">
                        <span class="data">{{ $tutorial->data }}</span>
                        <h2>{{ $tutorial->{'titulo_'.app()->getLocale()} }}</h2>
                        <p>{!! strip_tags($tutorial->{'chamada_'.app()->getLocale()}, '<br>') !!}</p>
                        <span class="acesse">{{ trans('frontend.tutoriais.acesse') }}</span>
                    </div>
                </a>
                @else
                <a href="{{ route('tutoriais.show', [$tutorial->categoria->slug, $tutorial->slug]) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/tutoriais/thumb-pequeno/'.$tutorial->thumb) }}" alt="">
                    </div>
                    <div class="texto">
                        <span class="data">{{ $tutorial->data }}</span>
                        <h2>{{ $tutorial->{'titulo_'.app()->getLocale()} }}</h2>
                        <p>{!! strip_tags($tutorial->{'chamada_'.app()->getLocale()}, '<br>') !!}</p>
                    </div>
                </a>
                @endif
                @endforeach

                {{ with(new App\Pagination\TutoriaisPresenter($tutoriais))->render() }}
            </div>

        </div>
    </div>

@endsection
