@extends('frontend.common.template')

@section('content')

    <div class="breadcrumb">
        <div class="center">
            <span>
                <a href="{{ route('home') }}">Home</a>
                /
                <a href="{{ route('tutoriais') }}">
                    {{ trans('frontend.nav.tutoriais') }}
                </a>
                /
                <strong>{{ $tutorial->{'titulo_'.app()->getLocale()} }}</strong>
            </span>
        </div>
    </div>

    <div class="tutoriais-show">
        <div class="center">
            <img src="{{ asset('assets/img/tutoriais/capa/'.$tutorial->capa) }}" alt="">
            <span class="data">{{ $tutorial->data }}</span>
            <h2>{{ $tutorial->{'titulo_'.app()->getLocale()} }}</h2>
            <p class="chamada">{!! $tutorial->{'chamada_'.app()->getLocale()} !!}</p>
            <div class="texto">
                {!! $tutorial->{'texto_'.app()->getLocale()} !!}
            </div>

            <div class="tutoriais-show-nav">
                @if($anterior)
                <a href="{{ route('tutoriais.show', [$anterior->categoria->slug, $anterior->slug]) }}" class="anterior">&laquo; {{ trans('frontend.tutoriais.anterior') }}</a>
                @endif
                <a href="{{ route('tutoriais') }}" class="voltar">{{ trans('frontend.tutoriais.voltar') }}</a>
                @if($proximo)
                <a href="{{ route('tutoriais.show', [$proximo->categoria->slug, $proximo->slug]) }}" class="proximo">{{ trans('frontend.tutoriais.proximo') }} &raquo;</a>
                @endif
            </div>
        </div>
    </div>

@endsection
