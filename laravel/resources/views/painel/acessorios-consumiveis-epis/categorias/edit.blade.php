@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Acessórios Consumíveis EPIs /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.acessorios-consumiveis-epis.categorias.update', $categoria->id],
        'method' => 'patch', 'files' => true])
    !!}

    @include('painel.acessorios-consumiveis-epis.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
