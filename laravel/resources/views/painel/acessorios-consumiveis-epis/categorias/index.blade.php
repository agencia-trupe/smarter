@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.acessorios-consumiveis-epis.index') }}" title="Voltar para Acessórios Consumíveis EPIs" class="btn btn-sm btn-default">
        &larr; Voltar para Acessórios Consumíveis EPIs    </a>

    <legend>
        <h2>
            <small>Acessórios Consumíveis EPIs /</small> Categorias

            <a href="{{ route('painel.acessorios-consumiveis-epis.categorias.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Categoria</a>
        </h2>
    </legend>

    @if(!count($categorias))
    <div class="alert alert-warning" role="alert">Nenhuma categoria cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="acessorios_consumiveis_epis_categorias">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título PT</th>
                <th>Título EN</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($categorias as $categoria)
            <tr class="tr-row" id="{{ $categoria->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $categoria->titulo_pt }}</td>
                <td>{{ $categoria->titulo_en }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.acessorios-consumiveis-epis.categorias.destroy', $categoria->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.acessorios-consumiveis-epis.categorias.edit', $categoria->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
