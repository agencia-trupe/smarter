@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Acessórios Consumíveis EPIs /</small> Adicionar Acessório Consumível EPI</h2>
    </legend>

    {!! Form::open(['route' => 'painel.acessorios-consumiveis-epis.store', 'files' => true]) !!}

        @include('painel.acessorios-consumiveis-epis.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
