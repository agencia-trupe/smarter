@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Acessórios Consumíveis EPIs /</small> Editar Acessório Consumível EPI</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.acessorios-consumiveis-epis.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.acessorios-consumiveis-epis.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
