@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Acessórios Consumíveis EPIs / Destaques / {{ $acessorio->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.acessorios-consumiveis-epis.destaques.store', $acessorio->id], 'files' => true]) !!}

        @include('painel.acessorios-destaques.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
