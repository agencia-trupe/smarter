@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Acessórios Consumíveis EPIs / Destaques / {{ $acessorio->titulo_pt }} /</small> Editar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.acessorios-consumiveis-epis.destaques.update', $acessorio->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.acessorios-destaques.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
