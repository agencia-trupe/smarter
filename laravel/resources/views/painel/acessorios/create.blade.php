@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Acessórios / {{ $produto->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.acessorios.store', $produto->id], 'files' => true]) !!}

        @include('painel.acessorios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
