@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Acessórios / {{ $produto->titulo_pt }} /</small> Editar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.acessorios.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.acessorios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
