@include('painel.common.flash')

{!! Form::hidden('produto_id', $produto->id) !!}

<div class="form-group">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::select('tipo', ['Incluso' => 'Incluso', 'Opcional' => 'Opcional'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (290 x 115px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/acessorios/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.acessorios.index', $produto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
