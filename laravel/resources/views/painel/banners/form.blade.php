@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('background', 'Background (1100 x 412px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->background) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('background', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('background_mobile', 'Background Mobile (768 x 500px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/mobile/'.$registro->background_mobile) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('background_mobile', ['class' => 'form-control']) !!}
</div>

{{--<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem (275 x 140px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('icone', 'Ícone (275 x 140px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/'.$registro->icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('icone', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>--}}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
