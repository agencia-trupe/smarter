@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('empresa', 'Empresa (1280 x 125px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->empresa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('empresa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('tutoriais', 'Tutoriais (1280 x 125px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->tutoriais) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('tutoriais', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('suporte', 'Suporte (1280 x 125px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->suporte) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('suporte', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('onde_comprar', 'Onde Comprar (1280 x 125px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->onde_comprar) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('onde_comprar', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('consultoria', 'Consultoria (1280 x 125px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->consultoria) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('consultoria', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('contato', 'Contato (1280 x 125px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->contato) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('contato', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
