@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Catálogo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.catalogo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.catalogo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
