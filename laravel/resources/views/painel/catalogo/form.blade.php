@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <p>
        <a href="{{ url('catalogos/'.$registro->arquivo) }}" target="_blank" style="display:block;">{{ $registro->arquivo }}</a>
    </p>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (largura de 160px)') !!}
@if($registro->imagem)
    <img src="{{ url('assets/img/catalogo/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
