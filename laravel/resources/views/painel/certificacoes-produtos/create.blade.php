@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Certificações / {{ $produto->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.certificacoes.store', $produto->id], 'files' => true]) !!}

        @include('painel.certificacoes-produtos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
