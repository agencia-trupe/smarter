@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Certificações / {{ $produto->titulo_pt }} /</small> Editar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.certificacoes.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificacoes-produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
