@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Certificações /</small> Adicionar Certificação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.certificacoes.store', 'files' => true]) !!}

        @include('painel.certificacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
