@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (45 x 28px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/certificacoes/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_cinza', 'Imagem Cinza (45 x 28px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/certificacoes/'.$registro->imagem_cinza) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_cinza', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.certificacoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
