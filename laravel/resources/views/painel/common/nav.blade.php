<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.banners*', Route::currentRouteName()) || str_is('painel.chamadas*', Route::currentRouteName()) || str_is('painel.cabecalhos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.cabecalhos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.cabecalhos.index') }}">Cabeçalhos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.empresa*', Route::currentRouteName()) || str_is('painel.escritorios*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Empresa
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>
            <li @if(str_is('painel.escritorios*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.escritorios.index') }}">Escritórios</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.produtos*', Route::currentRouteName()) || str_is('painel.acessorios-consumiveis-epis*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Produtos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
        	<li @if(str_is('painel.produtos*', Route::currentRouteName()) && !str_is('painel.produtos.capas*', Route::currentRouteName())) class="active" @endif>
        		<a href="{{ route('painel.produtos.index') }}">Produtos</a>
        	</li>
            <li @if(str_is('painel.acessorios-consumiveis-epis*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.acessorios-consumiveis-epis.index') }}">Acessórios Consumíveis EPIs</a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.produtos.capas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.produtos.capas.index') }}">Capas</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.tutoriais*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.tutoriais.index') }}">Tutoriais</a>
	</li>
    <li class="dropdown @if(str_is('painel.suporte*', Route::currentRouteName()) || str_is('painel.garantia-de-produtos*', Route::currentRouteName()) || str_is('painel.solicitacoes-de-garantias*', Route::currentRouteName()) || str_is('painel.solicitacoes-de-orientacoes*', Route::currentRouteName()) || str_is('painel.solicitacoes-de-manuais*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Suporte
            @if($garantiasNaoLidos + $orientacoesNaoLidos + $manuaisNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $garantiasNaoLidos + $orientacoesNaoLidos + $manuaisNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.suporte*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.suporte.index') }}">Suporte</a>
            </li>
            <li @if(str_is('painel.garantia-de-produtos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.garantia-de-produtos.index') }}">Garantia de Produtos</a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.solicitacoes-de-garantias*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.solicitacoes-de-garantias.index') }}">
                    Solicitações de Garantias
                    @if($garantiasNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $garantiasNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(str_is('painel.solicitacoes-de-orientacoes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.solicitacoes-de-orientacoes.index') }}">
                    Solicitações de Orientações
                    @if($orientacoesNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $orientacoesNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(str_is('painel.solicitacoes-de-manuais*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.solicitacoes-de-manuais.index') }}">
                    Solicitações de Manuais
                    @if($manuaisNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $manuaisNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.onde-comprar*', Route::currentRouteName()) || str_is('painel.ecommerces*', Route::currentRouteName()) || str_is('painel.distribuidores*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Onde Comprar
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.onde-comprar*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.onde-comprar.index') }}">Onde Comprar</a>
            </li>
            <li @if(str_is('painel.ecommerces*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.ecommerces.index') }}">ECommerces</a>
            </li>
            <li @if(str_is('painel.distribuidores*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.distribuidores.index') }}">Distribuidores</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.consultoria*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Consultoria
            @if($consultoriaNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $consultoriaNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.consultoria*', Route::currentRouteName()) && !str_is('painel.consultoria.recebidos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.consultoria.index') }}">Consultoria</a>
            </li>
            <li @if(str_is('painel.consultoria.recebidos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.consultoria.recebidos.index') }}">
                    Contatos Recebidos
                    @if($consultoriaNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $consultoriaNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.news*', Route::currentRouteName()) || str_is('painel.noticias*', Route::currentRouteName()) || str_is('painel.eventos*', Route::currentRouteName()) || str_is('painel.galerias*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            News
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.news*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.news.index') }}">News</a>
            </li>
            <li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.noticias.index') }}">Notícias</a>
            </li>
            <li @if(str_is('painel.eventos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.eventos.index') }}">Eventos</a>
            </li>
            <li @if(str_is('painel.galerias*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.galerias.index') }}">Galerias</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName()) || str_is('painel.newsletter*', Route::currentRouteName()) || str_is('painel.setores*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.contato.index', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(str_is('painel.contato.recebidos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.setores*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.setores.index') }}">Setores</a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.catalogo*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.catalogo.index') }}">Catálogo</a>
    </li>
</ul>
