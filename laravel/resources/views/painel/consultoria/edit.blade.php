@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Consultoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.consultoria.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.consultoria.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
