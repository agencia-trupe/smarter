@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1_pt', 'Texto 1 PT') !!}
            {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1_en', 'Texto 1 EN') !!}
            {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_consultoria', 'Imagem Consultoria (1400 x 350px)') !!}
    <img src="{{ url('assets/img/consultoria/'.$registro->imagem_consultoria) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_consultoria', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_consultoria_mobile', 'Imagem Consultoria Mobile (768 x 500px)') !!}
    <img src="{{ url('assets/img/consultoria/mobile/'.$registro->imagem_consultoria_mobile) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_consultoria_mobile', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_contato', 'Imagem Contato (1400 x 350px)') !!}
    <img src="{{ url('assets/img/consultoria/'.$registro->imagem_contato) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_contato', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
