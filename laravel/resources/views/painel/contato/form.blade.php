@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('endereco_pt', 'Endereço [PT]') !!}
            {!! Form::textarea('endereco_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('endereco_en', 'Endereço [EN]') !!}
            {!! Form::textarea('endereco_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<hr>

@foreach(['facebook', 'twitter', 'instagram', 'linkedin', 'youtube'] as $s)
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label($s, ucfirst($s)) !!}
            {!! Form::text($s, null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label($s.'_label', ucfirst($s).' Label') !!}
            {!! Form::text($s.'_label', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
@endforeach

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
