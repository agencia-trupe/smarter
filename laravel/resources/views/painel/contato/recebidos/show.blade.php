@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Telefone</label>
                <div class="well">{{ $contato->telefone }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Celular</label>
                <div class="well">{{ $contato->celular }}</div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->email }}
        </div>
    </div>

@if($contato->empresa)
    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $contato->empresa }}</div>
    </div>
@endif

@if($contato->cep)
    <div class="form-group">
        <label>CEP</label>
        <div class="well">{{ $contato->cep }}</div>
    </div>
@endif

@if($contato->cidade)
    <div class="form-group">
        <label>Cidade</label>
        <div class="well">{{ $contato->cidade }}</div>
    </div>
@endif

@if($contato->estado)
    <div class="form-group">
        <label>Estado</label>
        <div class="well">{{ $contato->estado }}</div>
    </div>
@endif

@if($contato->setor)
    <div class="form-group">
        <label>Setor</label>
        <div class="well">{{ $contato->setor }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $contato->mensagem }}</div>
    </div>

@if($contato->arquivo)
    <div class="form-group">
        <label>Arquivo</label>
        <div class="well">
            <a href="{{ url('arquivos/'.$contato->arquivo) }}" target="_blank">{{ $contato->arquivo }}</a>
        </div>
    </div>
@endif

    <a href="{{ route('painel.contato.recebidos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
