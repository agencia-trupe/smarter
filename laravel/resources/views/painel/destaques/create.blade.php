@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Destaques / {{ $produto->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.destaques.store', $produto->id], 'files' => true]) !!}

        @include('painel.destaques.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
