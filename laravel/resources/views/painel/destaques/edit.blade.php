@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Destaques / {{ $produto->titulo_pt }} /</small> Editar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.destaques.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.destaques.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
