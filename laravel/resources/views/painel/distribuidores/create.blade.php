@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Distribuidores /</small> Adicionar Distribuidor</h2>
    </legend>

    {!! Form::open(['route' => 'painel.distribuidores.store', 'files' => true]) !!}

        @include('painel.distribuidores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
