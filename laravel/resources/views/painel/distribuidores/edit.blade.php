@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Distribuidores /</small> Editar Distribuidor</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.distribuidores.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.distribuidores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
