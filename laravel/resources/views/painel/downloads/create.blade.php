@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Downloads / {{ $produto->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.downloads.store', $produto->id], 'files' => true]) !!}

        @include('painel.downloads.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
