@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Downloads / {{ $produto->titulo_pt }} /</small> Editar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.downloads.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.downloads.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
