@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>ECommerces /</small> Adicionar ECommerce</h2>
    </legend>

    {!! Form::open(['route' => 'painel.ecommerces.store', 'files' => true]) !!}

        @include('painel.ecommerces.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
