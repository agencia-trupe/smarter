@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>ECommerces /</small> Editar ECommerce</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.ecommerces.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.ecommerces.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
