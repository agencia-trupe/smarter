@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('site', 'Site') !!}
    {!! Form::text('site', null, ['class' => 'form-control', 'placeholder' => 'deve começar com http://']) !!}
</div>

<div class="well form-group">
    {!! Form::label('marca', 'Marca') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/ecommerces/'.$registro->marca) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('marca', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.ecommerces.index') }}" class="btn btn-default btn-voltar">Voltar</a>
