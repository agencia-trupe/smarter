@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1_pt', 'Texto 1 PT') !!}
            {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_1_en', 'Texto 1 EN') !!}
            {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (375px de largura)') !!}
    <img src="{{ url('assets/img/empresa/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('mapa', 'Mapa (680px de largura)') !!}
    <img src="{{ url('assets/img/empresa/'.$registro->mapa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('mapa', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
