@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Escritórios /</small> Adicionar Escritório</h2>
    </legend>

    {!! Form::open(['route' => 'painel.escritorios.store', 'files' => true]) !!}

        @include('painel.escritorios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
