@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Escritórios /</small> Editar Escritório</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.escritorios.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.escritorios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
