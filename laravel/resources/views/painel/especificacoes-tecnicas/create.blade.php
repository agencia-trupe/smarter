@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Produtos / Especificações Técnicas / {{ $produto->titulo_pt }} /</small> Adicionar
        </h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.especificacoes-tecnicas.store', $produto->id], 'files' => true]) !!}

        @include('painel.especificacoes-tecnicas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
