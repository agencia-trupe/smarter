@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Produtos / Especificações Técnicas / {{ $produto->titulo_pt }} /</small> Editar
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.especificacoes-tecnicas.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.especificacoes-tecnicas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
