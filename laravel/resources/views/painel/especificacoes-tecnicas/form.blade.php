@include('painel.common.flash')

{!! Form::hidden('produto_id', $produto->id) !!}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valor_pt', 'Valor PT') !!}
            {!! Form::textarea('valor_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valor_en', 'Valor EN') !!}
            {!! Form::textarea('valor_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.especificacoes-tecnicas.index', $produto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
