@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Eventos /</small> Editar Capa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.eventos.capa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.eventoscapa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
