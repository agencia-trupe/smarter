@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa (650 x 300px)') !!}
    <img src="{{ url('assets/img/eventoscapa/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.eventos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
