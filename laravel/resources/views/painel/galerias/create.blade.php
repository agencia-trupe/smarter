@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Galerias /</small> Adicionar Galeria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.galerias.store', 'files' => true]) !!}

        @include('painel.galerias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
