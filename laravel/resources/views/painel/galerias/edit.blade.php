@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Galerias /</small> Editar Galeria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.galerias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.galerias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
