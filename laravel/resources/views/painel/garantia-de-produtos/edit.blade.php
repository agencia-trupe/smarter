@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Garantia de Produtos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.garantia-de-produtos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.garantia-de-produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
