@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_abertura_pt', 'Texto Abertura PT') !!}
            {!! Form::textarea('texto_abertura_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_abertura_en', 'Texto Abertura EN') !!}
            {!! Form::textarea('texto_abertura_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_abertura', 'Imagem Abertura (430px de largura)') !!}
    <img src="{{ url('assets/img/garantia-de-produtos/'.$registro->imagem_abertura) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_abertura', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('e_mail', 'E-mail') !!}
    {!! Form::text('e_mail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('whatsapp', 'WhatsApp') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_saiba_mais_pt', 'Texto Saiba Mais PT') !!}
            {!! Form::textarea('texto_saiba_mais_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'tutoriais']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_saiba_mais_en', 'Texto Saiba Mais EN') !!}
            {!! Form::textarea('texto_saiba_mais_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'tutoriais']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_saiba_mais', 'Imagem Saiba Mais (970 x 320px)') !!}
    <img src="{{ url('assets/img/garantia-de-produtos/'.$registro->imagem_saiba_mais) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_saiba_mais', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
