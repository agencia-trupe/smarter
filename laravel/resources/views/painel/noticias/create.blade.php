@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Notícias /</small> Adicionar Notícia</h2>
    </legend>

    {!! Form::open(['route' => 'painel.noticias.store', 'files' => true]) !!}

        @include('painel.noticias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
