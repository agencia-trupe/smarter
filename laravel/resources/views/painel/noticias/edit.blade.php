@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Notícias /</small> Editar Notícia</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.noticias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.noticias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
