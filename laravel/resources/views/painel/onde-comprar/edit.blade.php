@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Onde Comprar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.onde-comprar.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.onde-comprar.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
