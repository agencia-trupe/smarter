@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_televendas', 'Imagem Televendas (1400 x 175px)') !!}
    <img src="{{ url('assets/img/onde-comprar/'.$registro->imagem_televendas) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_televendas', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_distribuidores', 'Imagem Distribuidores (1400 x 175px)') !!}
    <img src="{{ url('assets/img/onde-comprar/'.$registro->imagem_distribuidores) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_distribuidores', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
