@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categoria', 'Categoria') !!}
    {!! Form::select('categoria', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('produtos_linha_id', 'Linha') !!}
    {!! Form::select('produtos_linha_id', $linhas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('subtitulo_pt', 'Subtítulo PT') !!}
            {!! Form::text('subtitulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('subtitulo_en', 'Subtítulo EN') !!}
            {!! Form::text('subtitulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_pt', 'Chamada PT') !!}
            {!! Form::textarea('chamada_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_en', 'Chamada EN') !!}
            {!! Form::textarea('chamada_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa (500 x 400px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('tabela_pt', 'Tabela PT (445px de largura)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/tabelas/'.$registro->tabela_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('tabela_pt', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('tabela_en', 'Tabela EN (445px de largura)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/tabelas/'.$registro->tabela_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('tabela_en', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('manual', 'Manual') !!}
    @if(isset($registro) && $registro->manual)
    <p>
        <a href="{{ url('manuais/'.$registro->manual) }}" target="_blank" style="display:block;">{{ $registro->manual }}</a>
    </p>
    @endif
    {!! Form::file('manual', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
