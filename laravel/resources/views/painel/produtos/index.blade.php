@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Produtos /</small> {{ $categorias[$categoria] }}
            <a href="{{ route('painel.produtos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
        </h2>
    </legend>

    <div class="form-group">
        {!! Form::select('categoria', $categorias, Request::get('categoria'), ['class' => 'form-control', 'id' => 'categoria-select', 'data-route' => 'painel/produtos']) !!}
    </div>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $linhas, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Linhas', 'data-route' => 'painel/produtos']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.produtos.linhas.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Linhas</small></a>
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma linha para ordenar os registros.
            </p>
        </div>
        @endif
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Linha</th>@endif
                <th>Título PT</th>
                <th>Título EN</th>
                <th>Detalhes</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                @if(!$filtro)
                <td>
                    @if($registro->linha)
                    {{ $registro->linha->titulo_pt }}
                    @else
                    <span class="label label-warning">sem linha</span>
                    @endif
                </td>
                @endif
                <td>{{ $registro->titulo_pt }}</td>
                <td>{{ $registro->titulo_en }}</td>
                <td style="font-size:0;max-width:200px">
                    <a style="margin:1px" href="{{ route('painel.produtos.imagens.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Imagens
                    </a>
                    {{--
                    <a style="margin:1px" href="{{ route('painel.produtos.certificacoes.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Certificações
                    </a>
                    --}}
                    <a style="margin:1px" href="{{ route('painel.produtos.recursos-especiais.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Recursos Especiais
                    </a>
                    <a style="margin:1px" href="{{ route('painel.produtos.acessorios.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Acessórios
                    </a>
                    <a style="margin:1px" href="{{ route('painel.produtos.destaques.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Destaques
                    </a>
                    <a style="margin:1px" href="{{ route('painel.produtos.especificacoes-tecnicas.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Especificações Técnicas
                    </a>
                    <a style="margin:1px" href="{{ route('painel.produtos.downloads.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Downloads
                    </a>
                    <a style="margin:1px" href="{{ route('painel.produtos.fotos.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Fotos
                    </a>
                    <a style="margin:1px" href="{{ route('painel.produtos.videos.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Vídeos
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.produtos.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
