@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Linha</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.linhas.store']) !!}

        @include('painel.produtos.linhas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
