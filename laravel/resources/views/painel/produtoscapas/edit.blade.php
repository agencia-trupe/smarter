@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Capas</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.capas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtoscapas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
