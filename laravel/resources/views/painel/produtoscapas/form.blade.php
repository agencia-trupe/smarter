@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('soldagem_eletrodo', 'Soldagem Eletrodo (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->soldagem_eletrodo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('soldagem_eletrodo', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('soldagem_eletrodo_ativo', 'Soldagem Eletrodo Ativo (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->soldagem_eletrodo_ativo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('soldagem_eletrodo_ativo', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('soldagem_tig', 'Soldagem TIG (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->soldagem_tig) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('soldagem_tig', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('soldagem_tig_ativo', 'Soldagem TIG Ativo (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->soldagem_tig_ativo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('soldagem_tig_ativo', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('soldagem_migmag', 'Soldagem MIG/MAG (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->soldagem_migmag) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('soldagem_migmag', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('soldagem_migmag_ativo', 'Soldagem MIG/MAG Ativo (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->soldagem_migmag_ativo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('soldagem_migmag_ativo', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('corte_plasma', 'Corte Plasma (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->corte_plasma) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('corte_plasma', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('corte_plasma_ativo', 'Corte Plasma Ativo (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->corte_plasma_ativo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('corte_plasma_ativo', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('acessorios', 'Acessórios (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->acessorios) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('acessorios', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('acessorios_ativo', 'Acessórios Ativo (1400 x 260px)') !!}
            <img src="{{ url('assets/img/produtoscapas/'.$registro->acessorios_ativo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('acessorios_ativo', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

