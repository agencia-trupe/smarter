@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Recursos Especiais / {{ $produto->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.recursos-especiais.store', $produto->id], 'files' => true]) !!}

        @include('painel.recursos-especiais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
