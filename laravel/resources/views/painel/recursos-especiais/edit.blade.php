@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Recursos Especiais / {{ $produto->titulo_pt }} /</small> Editar</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.recursos-especiais.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.recursos-especiais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
