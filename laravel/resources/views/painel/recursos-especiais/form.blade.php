@include('painel.common.flash')

{!! Form::hidden('produto_id', $produto->id) !!}

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (60px de largura)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/recursos-especiais/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.recursos-especiais.index', $produto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
