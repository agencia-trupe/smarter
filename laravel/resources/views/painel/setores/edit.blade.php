@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Setores</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.setores.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.setores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
