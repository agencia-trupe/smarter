@include('painel.common.flash')

<p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
    <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
    Para mais de um e-mail separe por vírgula.
</p>

<div class="form-group">
    {!! Form::label('gestao_comercial', 'Gestão Comercial') !!}
    {!! Form::text('gestao_comercial', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('financeiro', 'Financeiro') !!}
    {!! Form::text('financeiro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('apoio_administrativo_e_comercial', 'Apoio Administrativo e Comercial') !!}
    {!! Form::text('apoio_administrativo_e_comercial', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('assistencia_tecnica_e_pos_venda', 'Assistência Técnica e Pós-Venda') !!}
    {!! Form::text('assistencia_tecnica_e_pos_venda', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('suporte_tecnico_de_soldagem_e_inspecao', 'Suporte Técnico de Soldagem e Inspeção') !!}
    {!! Form::text('suporte_tecnico_de_soldagem_e_inspecao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('marketing', 'Marketing') !!}
    {!! Form::text('marketing', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
