@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Solicitações de Orientações</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>E-mail</label>
                <div class="well">
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $contato->email }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Telefone</label>
                <div class="well">{{ $contato->telefone }}</div>
            </div>
        </div>
    </div>

@if($contato->cidade_estado)
    <div class="form-group">
        <label>Cidade / Estado</label>
        <div class="well">{{ $contato->cidade_estado }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Produto</label>
        <div class="well">{{ $contato->produto }}</div>
    </div>

    <div class="form-group">
        <label>Dúvida ou solicitação</label>
        <div class="well">{{ $contato->solicitacao }}</div>
    </div>

@if($contato->arquivo)
    <div class="form-group">
        <label>Arquivo</label>
        <div class="well">
            <a href="{{ url('arquivos/'.$contato->arquivo) }}" target="_blank">{{ $contato->arquivo }}</a>
        </div>
    </div>
@endif

    <a href="{{ route('painel.solicitacoes-de-orientacoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
