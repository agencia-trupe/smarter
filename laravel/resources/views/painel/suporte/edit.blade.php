@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Suporte</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.suporte.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.suporte.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
