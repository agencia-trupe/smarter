@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('orientacoes_tecnicas_abertura_pt', 'Orientações Técnicas Abertura PT') !!}
            {!! Form::textarea('orientacoes_tecnicas_abertura_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('orientacoes_tecnicas_abertura_en', 'Orientações Técnicas Abertura EN') !!}
            {!! Form::textarea('orientacoes_tecnicas_abertura_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('orientacoes_tecnicas_capa', 'Orientações Técnicas Capa (620 x 310px)') !!}
    <img src="{{ url('assets/img/suporte/'.$registro->orientacoes_tecnicas_capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('orientacoes_tecnicas_capa', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('garantia_de_produtos_abertura_pt', 'Garantia de Produtos Abertura PT') !!}
            {!! Form::textarea('garantia_de_produtos_abertura_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('garantia_de_produtos_abertura_en', 'Garantia de Produtos Abertura EN') !!}
            {!! Form::textarea('garantia_de_produtos_abertura_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>

    </div>
</div>

<div class="well form-group">
    {!! Form::label('garantia_de_produtos_capa', 'Garantia de Produtos Capa (620 x 310px)') !!}
    <img src="{{ url('assets/img/suporte/'.$registro->garantia_de_produtos_capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('garantia_de_produtos_capa', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('manuais_de_produtos_abertura_pt', 'Manuais de Produtos Abertura PT') !!}
            {!! Form::textarea('manuais_de_produtos_abertura_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('manuais_de_produtos_abertura_en', 'Manuais de Produtos Abertura EN') !!}
            {!! Form::textarea('manuais_de_produtos_abertura_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('manuais_de_produtos_capa', 'Manuais de Produtos Capa (620 x 310px)') !!}
    <img src="{{ url('assets/img/suporte/'.$registro->manuais_de_produtos_capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('manuais_de_produtos_capa', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
