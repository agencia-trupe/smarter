@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tutoriais /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.tutoriais.categorias.store']) !!}

        @include('painel.tutoriais.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
