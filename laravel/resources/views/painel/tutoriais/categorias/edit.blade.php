@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tutoriais /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.tutoriais.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.tutoriais.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
