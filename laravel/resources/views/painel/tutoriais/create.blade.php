@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tutoriais /</small> Adicionar Tutorial</h2>
    </legend>

    {!! Form::open(['route' => 'painel.tutoriais.store', 'files' => true]) !!}

        @include('painel.tutoriais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
