@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tutoriais /</small> Editar Tutorial</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tutoriais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tutoriais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
