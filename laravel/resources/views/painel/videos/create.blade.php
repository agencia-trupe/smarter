@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Vídeos / {{ $produto->titulo_pt }} /</small> Adicionar</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.videos.store', $produto->id], 'files' => true]) !!}

        @include('painel.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
