@include('painel.common.flash')

{!! Form::hidden('produto_id', $produto->id) !!}

<div class="well form-group">
    {!! Form::label('capa', 'Capa (285 x 170px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/videos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_tipo', 'Vídeo - Tipo') !!}
            {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_codigo', 'Vídeo - Código') !!}
            {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.videos.index', $produto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
